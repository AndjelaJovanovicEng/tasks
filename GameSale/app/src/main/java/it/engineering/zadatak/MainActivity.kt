package it.engineering.zadatak

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity() {

    val gameSellList: ArrayList<DataModel> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val jsonURL = "https://www.cheapshark.com/api/1.0/deals?storeID=1&upperPrice=15"

        getJSON().execute(jsonURL)

        val searchGames = findViewById<EditText>(R.id.searchGames)

        searchGames.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                filterGames(p0.toString())
            }

        })

    }

    private fun filterGames(textInput:String){
        val filteredGames = ArrayList<DataModel>()
        for(game in gameSellList){
            if(game.title.toLowerCase().contains(textInput.toLowerCase().trim())){
                filteredGames.add(game)
            }
        }

        val recyclerGameSell = findViewById<RecyclerView>(R.id.gameSell)
        val adapter = RecyclerViewAdapter(filteredGames)
        recyclerGameSell.adapter = adapter
        recyclerGameSell.layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)
        recyclerGameSell.setPadding(20,20,20,20)
    }

    inner  class getJSON: AsyncTask<String, String, String>(){

        override fun doInBackground(vararg params: String?): String {
            var json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                        reader -> reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }

            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            parseJSON(result)

            val recyclerGameSell = findViewById<RecyclerView>(R.id.gameSell)
            val adapter = RecyclerViewAdapter(gameSellList)
            recyclerGameSell.adapter = adapter
            recyclerGameSell.layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)
            recyclerGameSell.setPadding(20,20,20,20)

        }

        private fun parseJSON(jsonToParse:String?){
            val jsonArray = JSONArray(jsonToParse)

            println(jsonArray)

            jsonArray.let {
                (0 until it.length()).forEach{
                    val jsonObj = jsonArray.optJSONObject(it)

                    val title = jsonObj.getString("title")
                    val gameId = jsonObj.getString("gameID")
                    val salePrice = jsonObj.getString("salePrice")
                    val normalPrice = jsonObj.getString("normalPrice")
                    val rating = jsonObj.getString("dealRating")
                    val imgURL = jsonObj.getString("thumb")
                    gameSellList.add(DataModel(title, gameId, salePrice, normalPrice, rating, imgURL))

                }

            }

            println(gameSellList)

        }

    }



}