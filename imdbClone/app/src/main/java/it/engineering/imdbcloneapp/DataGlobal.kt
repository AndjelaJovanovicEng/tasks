package it.engineering.imdbcloneapp

import android.graphics.Bitmap
import android.graphics.Movie
import it.engineering.imdbcloneapp.entity.ActorEntity
import it.engineering.imdbcloneapp.entity.MovieEntity
import it.engineering.imdbcloneapp.entity.UpcomingMovieEntity
import it.engineering.imdbcloneapp.entity.UserEntity

object DataGlobal {

    var users : ArrayList<UserEntity> = arrayListOf(
            UserEntity("Test", "test@gmail.com", "test123"),
            UserEntity("Andjela", "andjela.jo998@gmail.com", "andj123")
    )

    var signedIn = false
    var redirectFromSignOut = false
    var enterFromProfileFragment = false
    val dataUtilityClass = Utility()

    val baseImageUrl = "https://image.tmdb.org/t/p/original"

    val movieList: ArrayList<MovieEntity> = arrayListOf()
    val upcomingMovieList:ArrayList<MovieEntity> = arrayListOf()
    val watchSoonMovieList: ArrayList<MovieEntity> = arrayListOf()

    val imageArray: ArrayList<Bitmap> = arrayListOf()
    var imageCounter = 0


    val titleArray: ArrayList<String> = arrayListOf()
    var titleCounter = 0

    val actorList: ArrayList<ActorEntity> = arrayListOf()

    val randomHour = (1..3).random()
    val randomMinute = (10..50).random()

    var enterFromComingSoon = false
    var enterFromWatchSoon = false

    val toolbarTitle1 = "Coming soon"
    val toolbarTitle2 = "Watch soon at home"


}