package it.engineering.imdbcloneapp.entity

data class UpcomingMovieEntity(
        var title: String,
        var voteAverage: Double,
        var backDropPath: String,
        var posterPath: String

) {
}