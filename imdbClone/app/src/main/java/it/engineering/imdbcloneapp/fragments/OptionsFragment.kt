package it.engineering.imdbcloneapp.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import it.engineering.imdbcloneapp.DataGlobal
import it.engineering.imdbcloneapp.MainActivity
import it.engineering.imdbcloneapp.R
import org.w3c.dom.Text

class OptionsFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_options, container, false)

        view.findViewById<TextView>(R.id.watchPreferences).setOnClickListener {
            DataGlobal.dataUtilityClass.setCurrentFragment(WatchPreferencesFragment(), activity as AppCompatActivity)
        }

        view.findViewById<TextView>(R.id.notifications).setOnClickListener {
            DataGlobal.dataUtilityClass.setCurrentFragment(NotificationsFragment(), activity  as AppCompatActivity)
        }

        view.findViewById<TextView>(R.id.display).setOnClickListener {
            DataGlobal.dataUtilityClass.setCurrentFragment(DisplayFragment(), activity  as AppCompatActivity)
        }

        view.findViewById<TextView>(R.id.storage).setOnClickListener {
            DataGlobal.dataUtilityClass.setCurrentFragment(StorageFragment(), activity  as AppCompatActivity)
        }

        view.findViewById<TextView>(R.id.about).setOnClickListener {
            DataGlobal.dataUtilityClass.setCurrentFragment(AboutFragment(), activity  as AppCompatActivity)
        }

        if(DataGlobal.signedIn){
            view.findViewById<TextView>(R.id.signOutBtn).visibility = View.VISIBLE
            view.findViewById<TextView>(R.id.signOutBtn).setOnClickListener {
                DataGlobal.signedIn = false
                DataGlobal.redirectFromSignOut = true
                startActivity(Intent(activity as AppCompatActivity, MainActivity::class.java))
                //DataGlobal.dataUtilityClass.setCurrentFragment(HomeFragment(), activity  as AppCompatActivity)
            }
        }



        return view
    }


}