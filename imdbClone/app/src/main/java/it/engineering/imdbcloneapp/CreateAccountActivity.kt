package it.engineering.imdbcloneapp

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import it.engineering.imdbcloneapp.entity.UserEntity

class CreateAccountActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_account)

        val inputName = findViewById<EditText>(R.id.registerName)
        val inputEmail = findViewById<EditText>(R.id.registerEmail)
        val inputRegisterPasswd = findViewById<EditText>(R.id.registerPasswd)
        val inputRegisterPasswd1 = findViewById<EditText>(R.id.registerPasswd1)

        val registerBtn = findViewById<Button>(R.id.createAccBtn)
        val loginBtn = findViewById<Button>(R.id.loginBtn)

        val errBox = findViewById<LinearLayout>(R.id.errorBoxSignIn)

        val showPasswdRegister = findViewById<CheckBox>(R.id.showPasswdRegister)

        registerBtn.setOnClickListener {
            if((inputName.text.toString() == "") || (inputEmail.text.toString() == "") || (inputRegisterPasswd.text.toString() == "")){
                errBox.visibility = View.VISIBLE
            } else {
                if((inputRegisterPasswd.text.toString().length < 8)){
                    errBox.visibility = View.VISIBLE
                } else {
                    DataGlobal.users.add(UserEntity(inputName.text.toString(), inputEmail.text.toString(), inputRegisterPasswd.text.toString()))
                    DataGlobal.signedIn = true
                    startActivity(Intent(this, MainActivity::class.java))
                }

            }
        }

        loginBtn.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }

        inputRegisterPasswd.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                inputRegisterPasswd1.setText(inputRegisterPasswd.getText().toString())
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int,
                                           after: Int) {
            }

            override fun afterTextChanged(s: Editable) {}
        })

        showPasswdRegister.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked == true){
                inputRegisterPasswd1.visibility = View.VISIBLE
            } else {
                inputRegisterPasswd1.visibility = View.GONE
            }
        }

        findViewById<TextView>(R.id.registerConditions).setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://m.imdb.com/conditions")))
        }

        findViewById<TextView>(R.id.registerPrivacy).setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.imdb.com/privacy")))
        }

    }
}