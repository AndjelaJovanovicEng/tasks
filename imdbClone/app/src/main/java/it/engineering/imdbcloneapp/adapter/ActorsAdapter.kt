package it.engineering.imdbcloneapp.adapter

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import it.engineering.imdbcloneapp.DataGlobal
import it.engineering.imdbcloneapp.R
import it.engineering.imdbcloneapp.entity.ActorEntity
import java.net.URL

class ActorsAdapter(private val actorsData: ArrayList<ActorEntity>) : RecyclerView.Adapter<CustomActorViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomActorViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val recycleViewRow = layoutInflater.inflate(R.layout.born_today_cell, parent, false)
        return CustomActorViewHolder(recycleViewRow)

    }

    override fun onBindViewHolder(holder: CustomActorViewHolder, position: Int) {

        holder.view.findViewById<TextView>(R.id.actorName).text = actorsData[position].name
        holder.view.findViewById<TextView>(R.id.actorAge).text = actorsData[position].age

        holder.view.findViewById<ImageView>(R.id.actorPoster).setImageResource(actorsData[position].image)


    }

    override fun getItemCount(): Int {
        return actorsData.count()
    }



}



class CustomActorViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}
