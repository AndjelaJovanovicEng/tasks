package it.engineering.imdbcloneapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import it.engineering.imdbcloneapp.DataGlobal
import it.engineering.imdbcloneapp.R
import it.engineering.imdbcloneapp.adapter.MoviesAdapter
import org.w3c.dom.Text


class FanFavouritesFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_fan_favourites, container, false)


        DataGlobal.dataUtilityClass.setVerticalAdapter(MoviesAdapter(DataGlobal.movieList) as RecyclerView.Adapter<RecyclerView.ViewHolder>, view.findViewById(R.id.fanFavRecyclerView), activity as AppCompatActivity)

        view.findViewById<TextView>(R.id.listResult).text = DataGlobal.movieList.size.toString() + " Results"

        view.findViewById<ImageView>(R.id.startHomeFragment).setOnClickListener {
            DataGlobal.dataUtilityClass.setCurrentFragment(HomeFragment(), activity as AppCompatActivity)
        }

        if(DataGlobal.enterFromComingSoon == true){
            view.findViewById<TextView>(R.id.toolBarTitle).text = DataGlobal.toolbarTitle1
            DataGlobal.dataUtilityClass.setVerticalAdapter(MoviesAdapter(DataGlobal.upcomingMovieList) as RecyclerView.Adapter<RecyclerView.ViewHolder>, view.findViewById(R.id.fanFavRecyclerView), activity as AppCompatActivity)
        }

        if(DataGlobal.enterFromWatchSoon == true){
            view.findViewById<TextView>(R.id.toolBarTitle).text = DataGlobal.toolbarTitle2
            DataGlobal.dataUtilityClass.setVerticalAdapter(MoviesAdapter(DataGlobal.watchSoonMovieList) as RecyclerView.Adapter<RecyclerView.ViewHolder>, view.findViewById(R.id.fanFavRecyclerView), activity as AppCompatActivity)
        }


        return view
    }


}