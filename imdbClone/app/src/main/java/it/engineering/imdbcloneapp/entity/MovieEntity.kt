package it.engineering.imdbcloneapp.entity

data class MovieEntity(
        var title: String,
        var originalTitle: String,
        var overview: String,
        var voteAverage: Double,
        var voteCount: Int,
        var popularity: Double,
        //var releaceDate: String,
        var backDropPath: String,
        var posterPath: String
        //var genreId: ArrayList<Int>
) {
}