package it.engineering.imdbcloneapp.adapter

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import it.engineering.imdbcloneapp.DataGlobal
import it.engineering.imdbcloneapp.DataGlobal.randomHour
import it.engineering.imdbcloneapp.DataGlobal.randomMinute
import it.engineering.imdbcloneapp.R
import it.engineering.imdbcloneapp.entity.MovieEntity
import it.engineering.imdbcloneapp.entity.UpcomingMovieEntity
import java.net.URL

class UpcomingMoviesAdapter (private val moviesData: ArrayList<MovieEntity>) : RecyclerView.Adapter<UpcomingMoviesCustomViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UpcomingMoviesCustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val recycleViewRow = layoutInflater.inflate(R.layout.upcoming_cell, parent, false)
        return UpcomingMoviesCustomViewHolder(recycleViewRow)

    }

    override fun onBindViewHolder(holder: UpcomingMoviesCustomViewHolder, position: Int) {

        holder.view.findViewById<TextView>(R.id.upcomingMovieRating).text = moviesData[position].voteAverage.toString()
        holder.view.findViewById<TextView>(R.id.upcomingMovieTitle).text = moviesData[position].title

        holder.view.findViewById<TextView>(R.id.upcomingMovieDuration).text = "$randomHour:$randomMinute"

        DownloadImage(holder.view.findViewById(R.id.upcomingMoviePoster)).execute(moviesData[position].posterPath)

    }

    override fun getItemCount(): Int {
        return moviesData.count()
    }

    inner class DownloadImage(val imageHolder: ImageView): AsyncTask<String, Void, Bitmap?>(){

        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val imageUrl = params[0]

            try {
                val imageDownloaded = URL(imageUrl).openStream()
                image = BitmapFactory.decodeStream(imageDownloaded)
            } catch (e:Exception){
                e.printStackTrace()
            }

            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            imageHolder.setImageBitmap(result)

        }

    }

}



class UpcomingMoviesCustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}