package it.engineering.imdbcloneapp

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Handler
import android.os.Looper
import android.provider.ContactsContract
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.engineering.imdbcloneapp.entity.ActorEntity
import it.engineering.imdbcloneapp.entity.MovieEntity
import it.engineering.imdbcloneapp.entity.UpcomingMovieEntity
import org.json.JSONObject
import java.net.URL

class Utility {

     fun setCurrentFragment(fragment: Fragment, activity: AppCompatActivity) {
         activity.supportFragmentManager.beginTransaction().apply {
            replace(R.id.fr_wrapper, fragment)
            commit()
        }
    }

     fun readJsonData(result: String?) {
        val jsonObject = JSONObject(result)
        val jsonArray = jsonObject.getJSONArray("results")

        jsonArray.let {
            (0 until it.length()).forEach{
                val jsonObj = jsonArray.optJSONObject(it)

                val title = jsonObj.getString("title")
                val originalTitle = jsonObj.getString("original_title")
                val overview = jsonObj.getString("overview")
                val voteAverage = jsonObj.getDouble("vote_average")
                val voteCount = jsonObj.getInt("vote_count")
                val popularity = jsonObj.getDouble("popularity")
                //var releaceDate = jsonObj.getString("release_date")
                val backDropPath = jsonObj.getString("backdrop_path")
                val posterPath = jsonObj.getString("poster_path")

                DataGlobal.movieList.add(MovieEntity(title, originalTitle, overview, voteAverage, voteCount, popularity, DataGlobal.baseImageUrl+backDropPath, DataGlobal.baseImageUrl+posterPath))

                DataGlobal.titleArray.add(title)
            }

        }

        println(DataGlobal.movieList)

    }

    fun readUpcomingJsonData(result: String?) {
        val jsonObject = JSONObject(result)
        val jsonArray = jsonObject.getJSONArray("results")

        jsonArray.let {
            (0 until it.length()).forEach{
                val jsonObj = jsonArray.optJSONObject(it)

                val title = jsonObj.getString("title")
                val originalTitle = jsonObj.getString("original_title")
                val overview = jsonObj.getString("overview")
                val voteAverage = jsonObj.getDouble("vote_average")
                val voteCount = jsonObj.getInt("vote_count")
                val popularity = jsonObj.getDouble("popularity")
                //var releaceDate = jsonObj.getString("release_date")
                val backDropPath = jsonObj.getString("backdrop_path")
                val posterPath = jsonObj.getString("poster_path")

                DataGlobal.upcomingMovieList.add(MovieEntity(title, originalTitle, overview, voteAverage, voteCount, popularity, DataGlobal.baseImageUrl+backDropPath, DataGlobal.baseImageUrl+posterPath))

            }

        }

        println(DataGlobal.upcomingMovieList)

    }

    fun readWatchSoonJsonData(result: String?) {
        val jsonObject = JSONObject(result)
        val jsonArray = jsonObject.getJSONArray("results")

        jsonArray.let {
            (0 until it.length()).forEach{
                val jsonObj = jsonArray.optJSONObject(it)

                val title = jsonObj.getString("title")
                val originalTitle = jsonObj.getString("original_title")
                val overview = jsonObj.getString("overview")
                val voteAverage = jsonObj.getDouble("vote_average")
                val voteCount = jsonObj.getInt("vote_count")
                val popularity = jsonObj.getDouble("popularity")
                //var releaceDate = jsonObj.getString("release_date")
                val backDropPath = jsonObj.getString("backdrop_path")
                val posterPath = jsonObj.getString("poster_path")

                DataGlobal.watchSoonMovieList.add(MovieEntity(title, originalTitle, overview, voteAverage, voteCount, popularity, DataGlobal.baseImageUrl+backDropPath, DataGlobal.baseImageUrl+posterPath))

            }

        }

        println(DataGlobal.watchSoonMovieList)

    }


     fun createDataSource(){
        DataGlobal.actorList.add(ActorEntity("Chris Pratt", "42", R.drawable.chris))
        DataGlobal.actorList.add(ActorEntity("Juliette Lewis", "48", R.drawable.juliete))
        DataGlobal.actorList.add(ActorEntity("Lana Wachowski", "56", R.drawable.lana))
        DataGlobal.actorList.add(ActorEntity("Natalie Alyn Lind", "42", R.drawable.natalie))
        DataGlobal.actorList.add(ActorEntity("Maggie Siff", "42", R.drawable.maggie))
        DataGlobal.actorList.add(ActorEntity("Benjamin Walker", "32", R.drawable.benjamin))
        DataGlobal.actorList.add(ActorEntity("Carrie Preston", "54", R.drawable.carrie))

    }

     fun setAdapter(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>, recyclerView: RecyclerView, activity: AppCompatActivity){
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.setPadding(5,5,5,5)

    }

    fun setVerticalAdapter(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>, recyclerView: RecyclerView, activity: AppCompatActivity){
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerView.setPadding(5,5,5,5)

    }

     fun showText(textView1: TextView){
        Handler(Looper.getMainLooper()).postDelayed({
            textView1.text = changeText()
            showText(textView1)
        }, 3500)
    }

     fun changeText():String{
        if(DataGlobal.titleCounter == DataGlobal.titleArray.size){
            DataGlobal.titleCounter = 0
        }

        var title = DataGlobal.titleArray[DataGlobal.titleCounter]
        DataGlobal.titleCounter++
        return title
    }

    fun showImage(imageHolder: ImageView){

        Handler(Looper.getMainLooper()).postDelayed({

            imageHolder.setImageBitmap(changeImage())
            showImage(imageHolder)
        }, 3500)
    }

    fun changeImage():Bitmap {



        if(DataGlobal.imageCounter == DataGlobal.imageArray.size){
           DataGlobal.imageCounter = 0
        }

        var image = DataGlobal.imageArray[DataGlobal.imageCounter]

        DataGlobal.imageCounter++
        return image
    }
}