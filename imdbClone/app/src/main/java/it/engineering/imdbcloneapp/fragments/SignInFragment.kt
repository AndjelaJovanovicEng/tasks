package it.engineering.imdbcloneapp.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import it.engineering.imdbcloneapp.DataGlobal
import it.engineering.imdbcloneapp.LoginActivity
import it.engineering.imdbcloneapp.MainActivity
import it.engineering.imdbcloneapp.R
import org.w3c.dom.Text


class SignInFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?


    ): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_sign_in, container, false)
        val startSignInAct = view.findViewById<LinearLayout>(R.id.imdbSignIn)

        val btnToHome = view.findViewById<ImageView>(R.id.btnToHome)

        val textViewBack = view.findViewById<TextView>(R.id.textViewBack)


        if(DataGlobal.enterFromProfileFragment == true){
            view.findViewById<TextView>(R.id.mainTitle).text = "Unlock all that IMDb has to offer"
            view.findViewById<TextView>(R.id.textViewBack).visibility = View.INVISIBLE
            view.findViewById<ImageView>(R.id.signInLogoImg).visibility = View.INVISIBLE
        }

        startSignInAct.setOnClickListener {
            activity?.let{
                val intent = Intent (it, LoginActivity::class.java)
                it.startActivity(intent)
            }
        }

        btnToHome.setOnClickListener {
           DataGlobal.dataUtilityClass.setCurrentFragment(HomeFragment(), activity as AppCompatActivity)
        }

        textViewBack.setOnClickListener {
            DataGlobal.dataUtilityClass.setCurrentFragment(HomeFragment(), activity as AppCompatActivity)
        }

        view.findViewById<LinearLayout>(R.id.goToRegisterForm).setOnClickListener {
            startActivity(Intent(activity, LoginActivity::class.java))
        }

        view.findViewById<TextView>(R.id.goToTermsAndConditions).setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://m.imdb.com/conditions")))
        }

        return view
    }

}