package it.engineering.imdbcloneapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import it.engineering.imdbcloneapp.R


class DisplayFragment : Fragment() {

    private val themeModes = arrayOf("Dark", "Light")

    private val videoPreviewOptions = arrayOf("WiFi only", "WiFi and cellular", "No auto-play")

    private val deviceRegion = arrayOf("Device Region (Serbia)", "United States (English)")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_display, container, false)

        val themeSpinner = view.findViewById<Spinner>(R.id.themeSpiner)
        val videoSpinner = view.findViewById<Spinner>(R.id.videoSpinner)
        val regionSpinner = view.findViewById<Spinner>(R.id.regionSpinner)


        if (themeSpinner != null){
            var arrayAdapter = ArrayAdapter(this.requireContext(), android.R.layout.simple_spinner_item, themeModes)
            themeSpinner.adapter = arrayAdapter
        }

        if (videoSpinner != null){
            var arrayAdapter = ArrayAdapter(this.requireContext(), android.R.layout.simple_spinner_item, videoPreviewOptions)
            videoSpinner.adapter = arrayAdapter
        }

        if (regionSpinner != null){
            var arrayAdapter = ArrayAdapter(this.requireContext(), android.R.layout.simple_spinner_item, deviceRegion)
            regionSpinner.adapter = arrayAdapter
        }





        return view
    }

}