package it.engineering.imdbcloneapp.fragments

import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.ContactsContract
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ScrollView
import androidx.appcompat.app.AppCompatActivity
import it.engineering.imdbcloneapp.DataGlobal
import it.engineering.imdbcloneapp.MainActivity
import it.engineering.imdbcloneapp.R

class YouFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_you, container, false)

        val linearLayout1 = view.findViewById<LinearLayout>(R.id.userNotSignedIn)
        val linearLayout2 = view.findViewById<LinearLayout>(R.id.userSignedIn)

        if(DataGlobal.signedIn == true){
            linearLayout1.visibility = View.GONE
            linearLayout2.visibility = View.VISIBLE
        }

        val openForms = view.findViewById<Button>(R.id.openForms)

        openForms.setOnClickListener {
            DataGlobal.enterFromProfileFragment = true
            DataGlobal.dataUtilityClass.setCurrentFragment(SignInFragment(), activity as AppCompatActivity)
        }

        view.findViewById<ImageView>(R.id.optionsIcon).setOnClickListener {
            DataGlobal.dataUtilityClass.setCurrentFragment(OptionsFragment(), activity as AppCompatActivity)
        }



        return view
    }

}