package it.engineering.imdbcloneapp.fragments

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.ContactsContract
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.engineering.imdbcloneapp.DataGlobal
import it.engineering.imdbcloneapp.DataGlobal.imageArray
import it.engineering.imdbcloneapp.DataGlobal.imageCounter
import it.engineering.imdbcloneapp.DataGlobal.movieList
import it.engineering.imdbcloneapp.DataGlobal.titleArray
import it.engineering.imdbcloneapp.DataGlobal.titleCounter
import it.engineering.imdbcloneapp.R
import it.engineering.imdbcloneapp.adapter.ActorsAdapter
import it.engineering.imdbcloneapp.adapter.CustomViewHolder
import it.engineering.imdbcloneapp.adapter.FanFavouritesAdapter
import it.engineering.imdbcloneapp.adapter.UpcomingMoviesAdapter
import java.net.URL

class HomeFragment : Fragment() {

    private var stopLoop = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_home, container, false)

        val openSignInForms = view.findViewById<Button>(R.id.openSignInForm)
        val browseMoviesBtn = view.findViewById<TextView>(R.id.browseMovies)


       if(DataGlobal.signedIn == true){
           openSignInForms.visibility = View.GONE
           browseMoviesBtn.visibility = View.VISIBLE
           view.findViewById<TextView>(R.id.watchListLabel).text = "Your Watchlist is Empty"
       }

        view.findViewById<Button>(R.id.openSignInForm).setOnClickListener {
            DataGlobal.enterFromProfileFragment = true
            DataGlobal.dataUtilityClass.setCurrentFragment(SignInFragment(), activity as AppCompatActivity)
        }


        for(i in 0..2){
            DownloadImage().execute(movieList[i].posterPath)
            DownloadImage().execute(movieList[i].backDropPath)
        }

        DataGlobal.dataUtilityClass.showImage(view.findViewById(R.id.imagePoster))
        DataGlobal.dataUtilityClass.showImage(view.findViewById(R.id.imageHolder))

        DataGlobal.dataUtilityClass.showText(view.findViewById(R.id.titleHolder))

        DataGlobal.dataUtilityClass.setAdapter(FanFavouritesAdapter(movieList) as RecyclerView.Adapter<RecyclerView.ViewHolder>, view.findViewById(R.id.favouritesRecyclerView), activity as AppCompatActivity)
        DataGlobal.dataUtilityClass.setAdapter(FanFavouritesAdapter(movieList) as RecyclerView.Adapter<RecyclerView.ViewHolder>, view.findViewById(R.id.streamingRecyclerView), activity as AppCompatActivity)
        DataGlobal.dataUtilityClass.setAdapter(ActorsAdapter(DataGlobal.actorList) as RecyclerView.Adapter<RecyclerView.ViewHolder>, view.findViewById(R.id.bornTodayRecyclerView), activity as AppCompatActivity)
        DataGlobal.dataUtilityClass.setAdapter(UpcomingMoviesAdapter(DataGlobal.upcomingMovieList) as RecyclerView.Adapter<RecyclerView.ViewHolder>, view.findViewById(R.id.upcomingMoviesRecyclerView), activity as AppCompatActivity)
        DataGlobal.dataUtilityClass.setAdapter(UpcomingMoviesAdapter(DataGlobal.watchSoonMovieList) as RecyclerView.Adapter<RecyclerView.ViewHolder>, view.findViewById(R.id.watchSoonAtHome), activity as AppCompatActivity)


        view.findViewById<TextView>(R.id.fanFavouritesSeeMore).setOnClickListener {
            DataGlobal.dataUtilityClass.setCurrentFragment(FanFavouritesFragment(), activity as AppCompatActivity)
        }

        view.findViewById<TextView>(R.id.comingSoonBtn).setOnClickListener {
            DataGlobal.enterFromComingSoon = true
            DataGlobal.dataUtilityClass.setCurrentFragment(FanFavouritesFragment(), activity as AppCompatActivity)
        }

        view.findViewById<TextView>(R.id.watchSoonHomeBtn).setOnClickListener {
            DataGlobal.enterFromWatchSoon = true
            DataGlobal.dataUtilityClass.setCurrentFragment(FanFavouritesFragment(), activity as AppCompatActivity)
        }

        return view
    }


    inner class DownloadImage(): AsyncTask<String, Void, Bitmap?>(){

        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val imageUrl = params[0]

            try {
                val imageDownloaded = URL(imageUrl).openStream()
                image = BitmapFactory.decodeStream(imageDownloaded)

            } catch (e:Exception){
                e.printStackTrace()
            }

            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            if(result != null){
                imageArray.add(result)

            }

        }


    }

}