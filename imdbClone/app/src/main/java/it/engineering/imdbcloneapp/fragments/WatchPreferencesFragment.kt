package it.engineering.imdbcloneapp.fragments

import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AlertDialog
import it.engineering.imdbcloneapp.R


class WatchPreferencesFragment : Fragment() {

    private val spinnerElements = arrayOf("amazon.com", "amazon.com.uk", "amazon.de", "amazon.in",
        "amazon.it", "amazon.fr", "amazon.co.jp", "amazon.ca", "amazon.es")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_watch_preferences, container, false)

        val spinner = view.findViewById<Spinner>(R.id.locationSpinner)

        if (spinner != null){
            var arrayAdapter = ArrayAdapter(this.requireContext(), android.R.layout.simple_spinner_item, spinnerElements)
            spinner.adapter = arrayAdapter
        }


        return view
    }

}