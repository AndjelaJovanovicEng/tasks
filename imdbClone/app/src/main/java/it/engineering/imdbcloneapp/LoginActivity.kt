package it.engineering.imdbcloneapp

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity


class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        val inputEmail = findViewById<EditText>(R.id.inputEmail)
        val inputPasswd = findViewById<EditText>(R.id.inputPassword)
        val inputPasswd1 = findViewById<EditText>(R.id.inputPassword1)
        val btnLogin = findViewById<Button>(R.id.signInBtn)
        val registerBtn = findViewById<Button>(R.id.registerBtn)

        val showPasswd = findViewById<CheckBox>(R.id.showPasswd)

        val errBox = findViewById<LinearLayout>(R.id.errorBox)

        btnLogin.setOnClickListener {
            val userEmail = inputEmail.text.toString()
            val userPasswd = inputPasswd.text.toString()

            if((userEmail == "") || (userPasswd == "")){
                errBox.visibility = View.VISIBLE
            } else {
                for(user in DataGlobal.users){
                    if((userEmail == user.email) && (userPasswd == user.password)){
                        DataGlobal.signedIn = true
                        startActivity(Intent(this, MainActivity::class.java))

                    }
                    break
                }
            }

        }

        registerBtn.setOnClickListener {
            startActivity(Intent(this, CreateAccountActivity::class.java))
        }

        showPasswd.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked == true){
                inputPasswd1.visibility = View.VISIBLE
            } else {
                inputPasswd1.visibility = View.GONE
            }
        }

        inputPasswd.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                inputPasswd1.setText(inputPasswd.getText().toString())
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int,
                                           after: Int) {
            }

            override fun afterTextChanged(s: Editable) {}
        })

        findViewById<TextView>(R.id.conditionsOfUse).setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://m.imdb.com/conditions")))
        }

        findViewById<TextView>(R.id.privacyNotice).setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.imdb.com/privacy")))
        }
    }




}