package it.engineering.imdbcloneapp.adapter

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import it.engineering.imdbcloneapp.DataGlobal
import it.engineering.imdbcloneapp.DataGlobal.imageArray
import it.engineering.imdbcloneapp.R
import it.engineering.imdbcloneapp.entity.MovieEntity
import java.net.URL

class FanFavouritesAdapter(private val moviesData: ArrayList<MovieEntity>) : RecyclerView.Adapter<CustomViewHolder>() {

    private var stringDateArr : List<String> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val recycleViewRow = layoutInflater.inflate(R.layout.favourites_cell, parent, false)
        return CustomViewHolder(recycleViewRow)

    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {

        holder.view.findViewById<TextView>(R.id.rating).text = moviesData[position].voteAverage.toString()
        holder.view.findViewById<TextView>(R.id.movieTitle).text = moviesData[position].title

//        val dateString = moviesData[position].releaceDate
//        stringDateArr = dateString.split(" ")

        holder.view.findViewById<TextView>(R.id.releaceDate).text = "2021"
        holder.view.findViewById<TextView>(R.id.voteCount).text = moviesData[position].voteCount.toString()

        DownloadImage(holder.view.findViewById(R.id.moviePoster)).execute(DataGlobal.baseImageUrl+moviesData[position].posterPath)

    }

    override fun getItemCount(): Int {
        return moviesData.count()
    }

    inner class DownloadImage(val imageHolder: ImageView): AsyncTask<String, Void, Bitmap?>(){

        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val imageUrl = params[0]

            try {
                val imageDownloaded = URL(imageUrl).openStream()
                image = BitmapFactory.decodeStream(imageDownloaded)
            } catch (e:Exception){
                e.printStackTrace()
            }

            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            imageHolder.setImageBitmap(result)

        }

    }


}



class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}
