package it.engineering.imdbcloneapp.entity

data class UserEntity(
    var name: String,
    var email: String,
    var password: String
) {
}