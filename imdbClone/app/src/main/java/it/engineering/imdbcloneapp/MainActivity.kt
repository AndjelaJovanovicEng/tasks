package it.engineering.imdbcloneapp

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import com.google.android.material.bottomnavigation.BottomNavigationView
import it.engineering.imdbcloneapp.DataGlobal.actorList
import it.engineering.imdbcloneapp.entity.ActorEntity
import it.engineering.imdbcloneapp.fragments.*
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val homeFragment = HomeFragment()
        val searchFragment = SearchFragment()
        val videoFragment = VideoFragment()
        val youFragment = YouFragment()

        val signInFragment = SignInFragment()

        if(!DataGlobal.signedIn){
            DataGlobal.dataUtilityClass.setCurrentFragment(signInFragment, this)
        } else {
            DataGlobal.dataUtilityClass.setCurrentFragment(homeFragment, this)
        }

        if(DataGlobal.redirectFromSignOut == true){
            DataGlobal.dataUtilityClass.setCurrentFragment(homeFragment, this)
        }


        val bottomNavigation = findViewById<BottomNavigationView>(R.id.bottomNavigation)
        bottomNavigation.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.ic_home -> DataGlobal.dataUtilityClass.setCurrentFragment(homeFragment, this)
                R.id.ic_search -> DataGlobal.dataUtilityClass.setCurrentFragment(searchFragment, this)
                R.id.ic_video -> DataGlobal.dataUtilityClass.setCurrentFragment(videoFragment, this)
                R.id.ic_you -> DataGlobal.dataUtilityClass.setCurrentFragment(youFragment, this)
            }

            true
        }

        DownloadJson().execute("https://api.themoviedb.org/3/movie/popular?api_key=04e56d7e12450e219f388fff8ebd05e6")

        DownloadJsonUpcomingMovies().execute("https://api.themoviedb.org/3/movie/popular?api_key=04e56d7e12450e219f388fff8ebd05e6&language=en-US&sort_by=popularity.desc&page=2")

        DownloadJsonWatchSoonMovies().execute("https://api.themoviedb.org/3/movie/popular?api_key=04e56d7e12450e219f388fff8ebd05e6&language=en-US&sort_by=popularity.desc&page=3")

        DataGlobal.dataUtilityClass.createDataSource()

    }

    inner  class DownloadJson: AsyncTask<String, String, String>(){
        override fun doInBackground(vararg params: String?): String {
            var json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                        reader -> reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }

            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            DataGlobal.dataUtilityClass.readJsonData(result)

        }

    }

    inner  class DownloadJsonUpcomingMovies: AsyncTask<String, String, String>(){
        override fun doInBackground(vararg params: String?): String {
            var json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                        reader -> reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }

            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            DataGlobal.dataUtilityClass.readUpcomingJsonData(result)


        }

    }

    inner  class DownloadJsonWatchSoonMovies: AsyncTask<String, String, String>(){
        override fun doInBackground(vararg params: String?): String {
            var json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                        reader -> reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }

            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            DataGlobal.dataUtilityClass.readWatchSoonJsonData(result)


        }

    }








}