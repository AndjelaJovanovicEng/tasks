package it.engineering.imdbcloneapp.adapter

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import it.engineering.imdbcloneapp.DataGlobal
import it.engineering.imdbcloneapp.R
import it.engineering.imdbcloneapp.entity.MovieEntity
import java.net.URL

class MoviesAdapter(private val moviesData: ArrayList<MovieEntity>) : RecyclerView.Adapter<CustomMoviesViewHolder>() {

    //private var stringDateArr : List<String> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomMoviesViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val recycleViewRow = layoutInflater.inflate(R.layout.fan_favourites_cell, parent, false)
        return CustomMoviesViewHolder(recycleViewRow)

    }

    override fun onBindViewHolder(holder: CustomMoviesViewHolder, position: Int) {

        holder.view.findViewById<TextView>(R.id.fanFavMovieRating).text = moviesData[position].voteAverage.toString()
        holder.view.findViewById<TextView>(R.id.fanFavMovieTitle).text = moviesData[position].title

//        val dateString = moviesData[position].releaceDate
//        stringDateArr = dateString.split(" ")

        holder.view.findViewById<TextView>(R.id.fanFavMoviesDate).text = "2021"

        holder.view.findViewById<TextView>(R.id.fanFavMovieDuration).text = "${DataGlobal.randomHour}h:${DataGlobal.randomMinute}min"

        DownloadImage(holder.view.findViewById(R.id.poster)).execute(moviesData[position].posterPath)

    }

    override fun getItemCount(): Int {
        return moviesData.count()
    }

    inner class DownloadImage(val imageHolder: ImageView): AsyncTask<String, Void, Bitmap?>(){

        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val imageUrl = params[0]

            try {
                val imageDownloaded = URL(imageUrl).openStream()
                image = BitmapFactory.decodeStream(imageDownloaded)
            } catch (e:Exception){
                e.printStackTrace()
            }

            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            imageHolder.setImageBitmap(result)

        }

    }


}



class CustomMoviesViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}