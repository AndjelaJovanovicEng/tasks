package it.engineering.imdbcloneapp.entity

data class ActorEntity(
    var name: String,
    var age: String,
    var image: Int
) {
}