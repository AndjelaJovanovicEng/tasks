package it.engineering.navigacijafragmenti

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Layout
import android.widget.Button
import android.widget.LinearLayout

class MainActivity : AppCompatActivity() {
    //nabrojivi tipovi

    enum class Transparency(val value: String){
        osamdeset("#CC"),
        pedeset("#80"),
        trideset("#4D")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btn = findViewById<Button>(R.id.promeni)
        btn.setOnClickListener {
            val linear = findViewById<LinearLayout>(R.id.newLinear)
            linear.setBackgroundColor(Color.parseColor(zameni("#990000", Transparency.pedeset)))
        }
    }

    fun zameni(boja: String, vrednost: Transparency): String{
        //#990000
        val novaBoja = boja.replace("#", vrednost.value)

        return novaBoja
    }
}