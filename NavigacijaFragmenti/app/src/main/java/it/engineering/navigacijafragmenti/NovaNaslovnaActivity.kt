package it.engineering.navigacijafragmenti

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class NovaNaslovnaActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nova_naslovna)
    }
}