package it.engineering.task01


import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout

import android.widget.TextView
import androidx.core.content.ContextCompat

import androidx.recyclerview.widget.RecyclerView

import it.engineering.task01.models.CompanyModelEntity

class FilterRecyclerViewAdapter(private val carCompanyFilterData: ArrayList<CompanyModelEntity>) : RecyclerView.Adapter<CustomFilteredViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomFilteredViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val recycleViewRow = layoutInflater.inflate(R.layout.recycler_view_filter, parent, false)
        return CustomFilteredViewHolder(recycleViewRow)

    }

    override fun onBindViewHolder(holder: CustomFilteredViewHolder, position: Int) {
        val companyTitle = holder.view.findViewById<TextView>(R.id.companyModelTitle)
        val modelTitle = holder.view.findViewById<TextView>(R.id.modelTitle)
        val modelPrice = holder.view.findViewById<TextView>(R.id.modelPrice)

        companyTitle.text = carCompanyFilterData[position].companyName
        modelTitle.text = carCompanyFilterData[position].modelName
        modelPrice.text = carCompanyFilterData[position].price



    }

    override fun getItemCount(): Int {
        return carCompanyFilterData.count()
    }

}

class CustomFilteredViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}