package it.engineering.task01.models

data class CompanyEntity (
    var title: String,
    var image: Int
)
{}