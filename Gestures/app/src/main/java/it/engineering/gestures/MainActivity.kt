package it.engineering.gestures

import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.ImageView
import android.widget.Toast
import kotlin.math.abs

class MainActivity : AppCompatActivity(), GestureDetector.OnGestureListener {

   // var nizSlika: ArrayList<Int> = arrayListOf()
    //var brojacSlike = 0

    lateinit var gestureDetector: GestureDetector
    var x1: Float = 0.0f
    var x2: Float = 0.0f

    var y1: Float = 0.0f
    var y2: Float = 0.0f

    val MIN_DISTANCE = 150

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        gestureDetector = GestureDetector(this, this)

//        nizSlika.add(R.drawable.img2)
//        nizSlika.add(R.drawable.img3)
//        nizSlika.add(R.drawable.img4)

        //prikaziSliku()

    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {

        when(event!!.action){
            0 -> {
                x1 = event.x
                y1 = event.y
            }

            1 -> {
                x2 = event.x
                y2 = event.y

                val vrednostX: Float = x2-x1
                val vrednostY: Float = y2-y1

                if(abs(vrednostX) > MIN_DISTANCE){
                    if(x2 > x1) {
                        Toast.makeText(this, "Dogodio se swipeDesno", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "Dogodio se swipeLevo", Toast.LENGTH_SHORT).show()
                    }
                } else if(abs(vrednostY) > MIN_DISTANCE){
                    if(y2>y1){
                        Toast.makeText(this, "Dogodio se swipeDole", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "Dogodio se swipeGore", Toast.LENGTH_SHORT).show()
                    }
                }
                
            }
        }


        return gestureDetector.onTouchEvent(event)
    }
    override fun onShowPress(e: MotionEvent?) {

    }

    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        return false
    }

    override fun onDown(e: MotionEvent?): Boolean {
        return false
    }

    override fun onFling(
        e1: MotionEvent?,
        e2: MotionEvent?,
        velocityX: Float,
        velocityY: Float
    ): Boolean {
        return false
    }

    override fun onScroll(
        e1: MotionEvent?,
        e2: MotionEvent?,
        distanceX: Float,
        distanceY: Float
    ): Boolean {
       return false
    }

    override fun onLongPress(e: MotionEvent?) {
       // Toast.makeText(this, "Dogodio se LongPress", Toast.LENGTH_SHORT).show()
    }

//    private fun prikaziSliku(){
//
//        Handler(Looper.getMainLooper()).postDelayed({
//            val imageHolder = findViewById<ImageView>(R.id.prikazSlike)
//            imageHolder.setImageResource(menjajSliku())
//            prikaziSliku()
//        }, 2500)
//
//    }
//
//    private fun menjajSliku():Int {
//        if(brojacSlike == 3){
//            brojacSlike = 0
//        }
//        val image = nizSlika[brojacSlike]
//        brojacSlike++
//        return image
//    }
}