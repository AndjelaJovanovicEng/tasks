package it.engineering.firebaseauth

import android.content.ContentValues.TAG
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class SignInActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        val goToSignUpForm = findViewById<Button>(R.id.goToSignUp)
        val signInBtn = findViewById<Button>(R.id.signInBtn)
        val userEmailSignIn = findViewById<EditText>(R.id.email)
        val userPasswdSignIn = findViewById<EditText>(R.id.passwd)

        auth = FirebaseAuth.getInstance()

        goToSignUpForm.setOnClickListener {
            startActivity(Intent(this, SignUpActivity::class.java))
        }

        //validacija username i passworda
        signInBtn.setOnClickListener {
            if(userEmailSignIn.text.toString().isEmpty() || userPasswdSignIn.text.toString().isEmpty()){
                Toast.makeText(baseContext, "Login failed, enter information.", Toast.LENGTH_SHORT).show()

            } else {
                auth.signInWithEmailAndPassword(userEmailSignIn.text.toString(), userPasswdSignIn.text.toString())
                        .addOnCompleteListener(this) { task ->
                            if (task.isSuccessful) {
                                val user = auth.currentUser
                                reload(user)
                            } else {

                                Toast.makeText(baseContext, "Login failed.", Toast.LENGTH_SHORT).show()
                                reload(null)
                            }
                        }
                }
            }

        }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        if(currentUser != null){
            reload(currentUser)
        }
    }

    private fun reload(currentUser: FirebaseUser?){
            if(currentUser != null){
                if(currentUser.isEmailVerified){
                    startActivity(Intent(this, MainActivity::class.java))

                } else {
                    Toast.makeText(baseContext, "Verify email.", Toast.LENGTH_SHORT).show()
                }

            } else {
                Toast.makeText(baseContext, "Login failed.", Toast.LENGTH_SHORT).show()
            }
    }
}