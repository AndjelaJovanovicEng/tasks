package it.engineering.firebaseauth

import android.content.ContentValues.TAG
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val deleteAccBtn = findViewById<Button>(R.id.deleteAccount)
        val progressBar = findViewById<ProgressBar>(R.id.progressBar)

        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser!!

        findViewById<TextView>(R.id.userInfo).text = user.email

        findViewById<Button>(R.id.logOutBtn).setOnClickListener {
            auth.signOut()
            startActivity(Intent(this, SignInActivity::class.java))
        }

        findViewById<TextView>(R.id.changePasswd).setOnClickListener {
            startActivity(Intent(this, UpdatePasswdActivity::class.java))
        }

        deleteAccBtn.setOnClickListener {
            val dialogBuilder = AlertDialog.Builder(this)
            dialogBuilder.setTitle("Are you sure?")
            dialogBuilder.setMessage("Deleting this account wll remove it from the system, and you can't access this app!")

            //ok dugme
            dialogBuilder.setPositiveButton("Delete", DialogInterface.OnClickListener { dialog, which ->

                progressBar.visibility = View.VISIBLE
                //brisanje korisnika
                user.delete()
                    .addOnCompleteListener { task ->
                        progressBar.visibility = View.GONE

                        if (task.isSuccessful) {
                            Toast.makeText(this, "Success!.", Toast.LENGTH_SHORT).show()
                            startActivity(Intent(this, SignInActivity::class.java))
                        } else {
                            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
                        }
                    }
            })

            //cancel dugme
            dialogBuilder.setNegativeButton("Dismiss", DialogInterface.OnClickListener { dialog, which ->
                dialog.dismiss()
            })

            val dialog = dialogBuilder.create()
            dialog.show()
        }


    }

}