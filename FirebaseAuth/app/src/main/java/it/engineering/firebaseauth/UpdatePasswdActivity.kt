package it.engineering.firebaseauth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth

class UpdatePasswdActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_passwd)

        val currentPasswd = findViewById<EditText>(R.id.currentPasswd)
        val newPasswd = findViewById<EditText>(R.id.newPasswd)
        val confirmPasswd = findViewById<EditText>(R.id.confirmPasswd)
        val changePasswd = findViewById<Button>(R.id.confirmChangePasswd)

        auth = FirebaseAuth.getInstance()

        val user = auth.currentUser!!

        changePasswd.setOnClickListener {
            if (currentPasswd.text.isNotEmpty() && newPasswd.text.isNotEmpty() && confirmPasswd.text.isNotEmpty()){
                if(newPasswd.text.toString().equals(confirmPasswd.text.toString())){

                    val credential = EmailAuthProvider.getCredential(user.email!!, currentPasswd.text.toString())

                    // Prompt the user to re-provide their sign-in credentials
                    user.reauthenticate(credential).addOnCompleteListener {
                            if(it.isSuccessful){

                                user.updatePassword(newPasswd.text.toString())
                                    .addOnCompleteListener { task ->
                                        if (task.isSuccessful) {
                                            Toast.makeText(baseContext, "User password updated.", Toast.LENGTH_SHORT).show()
                                            auth.signOut()
                                            startActivity(Intent(this, SignInActivity::class.java))

                                        }
                                    }
                            } else {
                                Toast.makeText(baseContext, "Error!", Toast.LENGTH_SHORT).show()
                            }

                        }
                } else {
                    Toast.makeText(baseContext, "Password dont match!", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(baseContext, "Fill all fields!", Toast.LENGTH_SHORT).show()
            }
        }


    }
}