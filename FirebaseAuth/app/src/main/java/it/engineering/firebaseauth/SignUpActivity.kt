package it.engineering.firebaseauth

import android.content.ContentValues
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class SignUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        val userEmail = findViewById<EditText>(R.id.emailRegister)
        val userPasswd = findViewById<EditText>(R.id.passwdRegister)

        val signUpBtn = findViewById<Button>(R.id.signUpBtn)


        auth = FirebaseAuth.getInstance()

        //validiramo email i password, ako su validni pravimo usera

        signUpBtn.setOnClickListener {
            if(userEmail.text.toString().isEmpty() && userPasswd.text.toString().isEmpty()){
                Toast.makeText(baseContext, "Enter information.", Toast.LENGTH_SHORT).show()

            } else {
                auth.createUserWithEmailAndPassword(userEmail.text.toString(), userPasswd.text.toString())
                        .addOnCompleteListener(this) { task ->
                            if (task.isSuccessful) {
                                //slanje verifikacionog mejla
                                val user = auth.currentUser!!
                                user.sendEmailVerification()
                                        .addOnCompleteListener(this) { task ->
                                            // Email Verification sent
                                            if(task.isSuccessful){
                                                startActivity(Intent(this, SignInActivity::class.java))

                                            }
                                        }

                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w(ContentValues.TAG, "createUserWithEmail:failure", task.exception)

                                Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT).show()

                            }
                        }
                }

        }
    }
}