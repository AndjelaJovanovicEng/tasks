package it.engineering.pickersdomaci

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import org.w3c.dom.Text
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {
    var language : String = ""
    var date : String = ""
    var time : String = ""
    var dateTimeLang : String = ""

    var dateFormat = SimpleDateFormat("dd-MM-YYYY", Locale.US)
    var timeFormat = SimpleDateFormat("HH:mm", Locale.ITALY)

    var btnClicked: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val buttonSumbit = findViewById<Button>(R.id.btnSumbit)
        val buttonSubmitInline = findViewById<Button>(R.id.btnSumbitAll)

        buttonSumbit.setOnClickListener {
            btnClicked = 1
            showLanguages()
        }

        buttonSubmitInline.setOnClickListener {
            btnClicked = 2
            showLanguages()
        }
    }

    private fun showLanguages(){
        val languagesArray = arrayOf("Srpski", "Engleski", "Nemacki", "Ruski")

        val pickerChooseLang = AlertDialog.Builder(this)
        pickerChooseLang.setTitle("Choose language")
        pickerChooseLang.setSingleChoiceItems(languagesArray, -1){dialog, lang ->
            language = languagesArray[lang]
            showDate()
           // dialog.dismiss()
        }

        pickerChooseLang.create().show()

    }

    private fun showDate() {
        val currentDate = Calendar.getInstance()
        val pickerDate = DatePickerDialog(this, DatePickerDialog.OnDateSetListener{
            picker, year, month, dayOfMonth ->
            val currentDateTemp = Calendar.getInstance()
            currentDateTemp.set(Calendar.YEAR, year)
            currentDateTemp.set(Calendar.MONTH, month)
            currentDateTemp.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            //Instanca Calendar-a uzima trenutni datum i vreme
            val dateFormatted = dateFormat.format(currentDateTemp.time)
            date = dateFormatted
            if (btnClicked == 1){
                showTime()
            } else if (btnClicked == 2){
                showTimeAndPrintInline()
            }

        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DAY_OF_MONTH))

        pickerDate.show()
    }

    private fun showTime() {
        val languageView = findViewById<TextView>(R.id.textViewLanguage)
        val dateView = findViewById<TextView>(R.id.textViewDate)
        val timeView = findViewById<TextView>(R.id.textViewTime)

        val currentTime = Calendar.getInstance()
        val pickerTime = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener{
            picker, hourOfDay, minute ->
            val currentTimeTemp = Calendar.getInstance()
            currentTimeTemp.set(Calendar.HOUR_OF_DAY, hourOfDay)
            currentTimeTemp.set(Calendar.MINUTE, minute)
            val timeFormatted = timeFormat.format(currentTimeTemp.time)
            time = timeFormatted

            languageView.text = language
            dateView.text = date
            timeView.text = time

        }, currentTime.get(Calendar.HOUR_OF_DAY), currentTime.get(Calendar.MINUTE), true)
        pickerTime.show()

    }

    private fun showTimeAndPrintInline() {
        val textViewShowAll = findViewById<TextView>(R.id.textViewShowAll)
        val currentTime = Calendar.getInstance()
        val pickerTime = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener{
            picker, hourOfDay, minute ->
            val currentTimeTemp = Calendar.getInstance()
            currentTimeTemp.set(Calendar.HOUR_OF_DAY, hourOfDay)
            currentTimeTemp.set(Calendar.MINUTE, minute)
            val timeFormatted = timeFormat.format(currentTimeTemp.time)
            time = timeFormatted

            dateTimeLang = "$language $date $time"
            textViewShowAll.text = dateTimeLang

        }, currentTime.get(Calendar.HOUR_OF_DAY), currentTime.get(Calendar.MINUTE), true)
        pickerTime.show()

    }

}