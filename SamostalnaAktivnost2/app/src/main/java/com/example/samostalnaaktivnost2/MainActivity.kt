package com.example.samostalnaaktivnost2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val scrollLink = findViewById<LinearLayout>(R.id.scrollDown)
        val hiddenLayout = findViewById<LinearLayout>(R.id.hiddenLayout)

        hiddenLayout.visibility = View.GONE

        scrollLink.setOnClickListener(View.OnClickListener {
            if(hiddenLayout.visibility == View.GONE){
               // scrollLink.visibility = View.GONE
                hiddenLayout.visibility = View.VISIBLE
            } else {
                //scrollLink.visibility = View.VISIBLE
                hiddenLayout.visibility = View.GONE
            }

        })

    }
}