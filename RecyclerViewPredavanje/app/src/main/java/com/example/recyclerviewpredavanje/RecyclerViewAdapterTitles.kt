package com.example.recyclerviewpredavanje

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.recycler_view_title.view.*

class RecyclerViewAdapterTitles(val naslovi:ArrayList<Titles>): RecyclerView.Adapter<CustomViewHolderTitles>() {

    var rowIndex : Int? = null
    //var rowIndexToClick: Int? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolderTitles {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.recycler_view_title, parent, false)
        return  CustomViewHolderTitles(cellForRow)

    }

    override fun onBindViewHolder(holder: CustomViewHolderTitles, position: Int) {
        holder.view.textTitle.text = naslovi[position].naziv

        holder.view.setOnClickListener{
            rowIndex = position
            notifyDataSetChanged()
        }

         if (rowIndex == position){
            holder.view.layoutTitles.setBackgroundResource(R.color.rose)
            holder.view.textTitle.setTextColor(Color.parseColor("#000000"))
        } else {
            holder.view.layoutTitles.setBackgroundResource(R.color.purple)
            holder.view.textTitle.setTextColor(Color.parseColor("#ffffff"))
        }


    }

    override fun getItemCount(): Int {
        return naslovi.count()
    }
}

class CustomViewHolderTitles(val view: View) : RecyclerView.ViewHolder(view) {

}