package com.example.recyclerviewpredavanje

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
data class People(val ime: String, val prezime: String, val slika: Int)
data class Titles(val naziv: String)
data class NavBarItem(val title: String, val image: Int)

class MainActivity : AppCompatActivity() {
    val objectArray: ArrayList<People> = arrayListOf()
    val titles: ArrayList<Titles> = arrayListOf()
    val navBarItems: ArrayList<NavBarItem> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createDataSource()


        val kreiraniAdapter = RecyclerVewAdapter(objectArray)
        recyclerView.adapter = kreiraniAdapter
        recyclerView.layoutManager = GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false)
        recyclerView.setBackgroundResource(R.color.blue)
        recyclerView.setPadding(20,20,20,20)

        createTitleSource()

        val kreiraniAdapterNaslovi = RecyclerViewAdapterTitles(titles)
        recyclerViewNews.adapter = kreiraniAdapterNaslovi
        recyclerViewNews.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerViewNews.setBackgroundResource(R.color.purple)
        recyclerViewNews.setPadding(20,20,20,20)

        createNavBarItemsSource()

        val kreiraniAdapterNavBar = RecyclerViewAdapterNavBar(navBarItems)
        recyclerViewNavBar.adapter = kreiraniAdapterNavBar
        recyclerViewNavBar.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerViewNavBar.setBackgroundResource(R.color.purple)
        recyclerViewNavBar.setPadding(20,20,20,20)

    }

    private fun createDataSource() {
        objectArray.add(People("Mihajlo", "Jezdić", R.drawable.logo))
        objectArray.add(People("Anita", "Bojanić", R.drawable.logo))
        objectArray.add(People("Strahinja", "Marković", R.drawable.logo))
        objectArray.add(People("Anita", "Bojanić", R.drawable.logo))
        objectArray.add(People("Mihajlo", "Jezdić", R.drawable.logo))
        objectArray.add(People("Strahinja", "Marković", R.drawable.logo))
        objectArray.add(People("Anita", "Bojanić", R.drawable.logo))
        objectArray.add(People("Mihajlo", "Jezdić", R.drawable.logo))
        objectArray.add(People("Strahinja", "Marković", R.drawable.logo))
        objectArray.add(People("Anita", "Bojanić", R.drawable.logo))
        objectArray.add(People("Strahinja", "Marković", R.drawable.logo))
        objectArray.add(People("Anita", "Bojanić", R.drawable.logo))
        objectArray.add(People("Mihajlo", "Jezdić", R.drawable.logo))
        objectArray.add(People("Strahinja", "Marković", R.drawable.logo))
        objectArray.add(People("Anita", "Bojanić", R.drawable.logo))
        objectArray.add(People("Strahinja", "Marković", R.drawable.logo))
        objectArray.add(People("Anita", "Bojanić", R.drawable.logo))
        objectArray.add(People("Mihajlo", "Jezdić", R.drawable.logo))
        objectArray.add(People("Strahinja", "Marković", R.drawable.logo))
        objectArray.add(People("Anita", "Bojanić", R.drawable.logo))
    }

    private fun createTitleSource(){
        titles.add(Titles("Novosti"))
        titles.add(Titles("Hronika"))
        titles.add(Titles("Vest dana"))
        titles.add(Titles("Prognoza"))
        titles.add(Titles("Hi-Tech"))
        titles.add(Titles("Zivot pise drame"))

    }

    private fun createNavBarItemsSource(){
        navBarItems.add(NavBarItem("Account", R.drawable.ic_baseline_account_circle_24))
        navBarItems.add(NavBarItem("Add photo", R.drawable.ic_baseline_add_a_photo_24))
        navBarItems.add(NavBarItem("Videos", R.drawable.ic_baseline_video_library_24))
        navBarItems.add(NavBarItem("Playlist", R.drawable.ic_baseline_headset_24))
        navBarItems.add(NavBarItem("Favourites", R.drawable.ic_baseline_favorite_24))
        navBarItems.add(NavBarItem("Admin", R.drawable.ic_baseline_admin_panel_settings_24))
    }
}