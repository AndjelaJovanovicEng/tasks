package com.example.recyclerviewpredavanje

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.recycler_view_bottombar.view.*
import kotlinx.android.synthetic.main.recycler_view_item.view.*

class RecyclerViewAdapterNavBar (val navBarItems:ArrayList<NavBarItem>): RecyclerView.Adapter<CustomViewHolderNavBar>() {

    var rowIndex: Int? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolderNavBar {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.recycler_view_bottombar, parent, false)
        return  CustomViewHolderNavBar(cellForRow)

    }

    override fun onBindViewHolder(holder: CustomViewHolderNavBar, position: Int) {
        holder.view.navBarTitle.text = navBarItems[position].title
        holder.view.navBarIcon.setImageResource(navBarItems[position].image)

        holder.view.setOnClickListener{
            rowIndex = position
            notifyDataSetChanged()
        }

        if(rowIndex == position){
            holder.view.layoutNavBar.setBackgroundResource(R.color.rose)
            holder.view.navBarTitle.setTextColor(Color.parseColor("#000000"))
        } else {
            holder.view.layoutNavBar.setBackgroundResource(R.color.purple)
            holder.view.navBarTitle.setTextColor(Color.parseColor("#ffffff"))
        }
    }

    override fun getItemCount(): Int {
        return navBarItems.count()
    }
}

class CustomViewHolderNavBar(val view: View) : RecyclerView.ViewHolder(view) {

}