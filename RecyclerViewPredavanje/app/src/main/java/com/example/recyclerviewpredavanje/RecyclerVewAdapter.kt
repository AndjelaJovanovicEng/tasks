package com.example.recyclerviewpredavanje

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.recycler_view_item.view.*

class RecyclerVewAdapter(val preuzetiPodaci:ArrayList<People>): RecyclerView.Adapter<CustomViewHolder>() {

    var rowIndex: Int? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.recycler_view_item, parent, false)
        return  CustomViewHolder(cellForRow)

    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.imeTextView.text = preuzetiPodaci[position].ime
        holder.view.prezimeTextView.text = preuzetiPodaci[position].prezime
        holder.view.imageView.setImageResource(preuzetiPodaci[position].slika)
        holder.view.setOnClickListener{
            rowIndex = position
            notifyDataSetChanged()
            Toast.makeText(holder.view.context, preuzetiPodaci[position].ime, Toast.LENGTH_SHORT).show()
        }

        if(rowIndex == position){
            holder.view.rowCell.setBackgroundResource(R.color.rose)
        }
    }

    override fun getItemCount(): Int {
        return preuzetiPodaci.count()
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}