package it.engineering.novastranica

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class NewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new)

        val textView = findViewById<TextView>(R.id.textView)
        textView.setOnClickListener{
            startActivity(Intent(this, MainActivity::class.java))
            finish()

        }
    }

    override fun onBackPressed() {
        startActivity(Intent(this, NazadActivity::class.java))
    }
}