package it.engineering.novastranica

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button

var logovanje = false

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (logovanje) {
            Log.d("LOGIN", "Korisnik je ulogovan")
        } else {
            Log.d("LOGIN", "Korisnik nije ulogovan")
            startActivity(Intent(this, Login::class.java))
        }
        val dugme = findViewById<Button>(R.id.startNewActivity)

        dugme.setOnClickListener {
            startActivity(Intent(this, NewActivity::class.java))
            //finish()



        }
    }
}