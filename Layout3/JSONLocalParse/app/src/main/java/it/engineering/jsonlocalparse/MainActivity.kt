package it.engineering.jsonlocalparse

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOError
import java.io.IOException
import java.io.InputStream

class MainActivity : AppCompatActivity() {

    val studenti:ArrayList<Student> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        procitajJSON()

        for(i in studenti){
            Log.d("STUDENT", i.ime)
        }
    }


    private fun procitajJSON(){
        val json: String?

        try {
            val InputStream: InputStream = assets.open("localJSON.json")
            json = InputStream.bufferedReader().use {
                it.readText()
            }

            val jsonNiz = JSONArray(json)
            val ispis = findViewById<TextView>(R.id.ispis)

            jsonNiz.let {
                (0 until it.length()).forEach{
                    val jsonObjekat = jsonNiz.optJSONObject(it)

                    val ime = jsonObjekat.getString("ime")
                    val prezime = jsonObjekat.getString("prezime")
                    val mesto = jsonObjekat.getString("mesto")
                    val godiste = jsonObjekat.getString("godiste")
                    val visina = jsonObjekat.getString("visina")
                    val prosek = jsonObjekat.getString("prosek")

                    studenti.add(Student(ime, prezime, mesto, godiste, visina, prosek))
                }

            }


//            val jsonObjekat = JSONObject(json)
//            val ime = jsonObjekat.getString("ime")
//            val prezime = jsonObjekat.getString("prezime")
//            val mesto = jsonObjekat.getString("mesto")
//            val godiste = jsonObjekat.getString("godiste")
//            val visina = jsonObjekat.getString("visina")
//            val prosek = jsonObjekat.getString("prosek")



            //ispis.text = "$ime $prezime $mesto $godiste $visina $prosek"


        } catch (e:IOException){
            Log.d("JSON Exception", e.toString())
        }


    }
}

class Student (
        val ime: String,
        val prezime: String,
        val mesto: String,
        val godiste: String,
        val visina: String,
        val prosek: String
)