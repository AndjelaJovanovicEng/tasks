package it.engineering.task01.models

data class CompanyModelEntity (
        var companyName: String,
        var modelName: String,
        var price: String

)
{}