package it.engineering.task01

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import it.engineering.task01.models.CompanyEntity
import org.w3c.dom.Text

class RecyclerViewAdapter(private val carCompanyData: ArrayList<CompanyEntity>) : RecyclerView.Adapter<CustomViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val recycleViewRow = layoutInflater.inflate(R.layout.recycler_view_model, parent, false)
        return CustomViewHolder(recycleViewRow)

    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val modelTitle = holder.view.findViewById<TextView>(R.id.modelTitle)
        val modelLogo = holder.view.findViewById<ImageView>(R.id.modelLogo)

        val cell = holder.view.findViewById<LinearLayout>(R.id.selectedModel)

        modelLogo.setImageResource(carCompanyData[position].image)
        modelTitle.text = carCompanyData[position].title


        cell.setOnClickListener {
            val intent = Intent(holder.view.context, CompanyModelsActivity::class.java)
            intent.putExtra("manufacturer", modelTitle.text.toString())
            startActivity(holder.view.context, intent,null)
        }
    }



    override fun getItemCount(): Int {
        return carCompanyData.count()
    }
}


class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}