package it.engineering.task01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import it.engineering.task01.DataGlobal.modelsForCompany
import org.w3c.dom.Text


class ModelDetailsActivity : AppCompatActivity() {

   // var modelDetails:List<String> = listOf()
  //  var attributes:List<String> = listOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_model_details)

        val carCompany = findViewById<TextView>(R.id.carCompany)
        val carModelName = findViewById<TextView>(R.id.carModelName)
        val carEngine = findViewById<TextView>(R.id.carEngine)
        val carModelEquipment = findViewById<TextView>(R.id.carModelEquiment)
        val carPrice = findViewById<TextView>(R.id.carPrice)
        val carCilindres = findViewById<TextView>(R.id.carCilindres)
        val carVentiles = findViewById<TextView>(R.id.carVentiles)
        val carDiametar = findViewById<TextView>(R.id.carDiametar)
        val injectionType = findViewById<TextView>(R.id.injectionType)
        val openSystem = findViewById<TextView>(R.id.openSystem)
        val turbo = findViewById<TextView>(R.id.turbo)
        val volume = findViewById<TextView>(R.id.volume)
        val kw = findViewById<TextView>(R.id.kw)
        val ks = findViewById<TextView>(R.id.ks)
        val spinPower = findViewById<TextView>(R.id.spinPower)
        val spinMoment = findViewById<TextView>(R.id.spinMoment)

        val model = intent.getStringExtra("model")
        val price = intent.getStringExtra("price")

        carModelName.text = model
        carPrice.text = price.plus(" €")

        for (modelAttribute in modelsForCompany){
            if((modelAttribute.model == model) && (modelAttribute.price == price)){
                carCompany.text = modelAttribute.company
                carEngine.text = modelAttribute.engine
                carModelEquipment.text = modelAttribute.equipment
                carCilindres.text = modelAttribute.cilinders
                carVentiles.text = modelAttribute.ventiles
                carDiametar.text = modelAttribute.diametar
                injectionType.text = modelAttribute.injectionType
                openSystem.text = modelAttribute.openSystem
                turbo.text = modelAttribute.turbo
                volume.text = modelAttribute.volume
                kw.text = modelAttribute.KW
                ks.text = modelAttribute.KS
                spinPower.text = modelAttribute.powerSpin
                spinMoment.text = modelAttribute.spinMoment
            }
        }

        val moreDetails = findViewById<ScrollView>(R.id.moreDetails)
        val seeMore = findViewById<ImageView>(R.id.seeMore)
        seeMore.setOnClickListener {
            if(moreDetails.visibility == View.INVISIBLE){
                moreDetails.visibility = View.VISIBLE
            } else {
                moreDetails.visibility = View.INVISIBLE
            }
        }

    }


}