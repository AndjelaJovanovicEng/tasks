package it.engineering.task01

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import it.engineering.task01.models.CarModelEntity


class CarModelRecyclerViewAdapter(private val carModelsData: ArrayList<CarModelEntity>) : RecyclerView.Adapter<CustomViewHolderModels>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolderModels {
        val layoutInflater = LayoutInflater.from(parent.context)
        val recycleViewRow = layoutInflater.inflate(R.layout.recycler_view_cars, parent, false)
        return CustomViewHolderModels(recycleViewRow)

    }

    override fun onBindViewHolder(holder: CustomViewHolderModels, position: Int) {
        val modelTitle = holder.view.findViewById<TextView>(R.id.modelName)
        val modelLogo = holder.view.findViewById<ImageView>(R.id.modelImage)
        val modelPrice = holder.view.findViewById<TextView>(R.id.modelPrice)

        val cell = holder.view.findViewById<LinearLayout>(R.id.selectedCar)

        modelTitle.text = carModelsData[position].model
        modelLogo.setImageResource(R.drawable.logo)
        modelPrice.text = carModelsData[position].price.plus("€")

        cell.setOnClickListener {
            val intent = Intent(holder.view.context, ModelDetailsActivity::class.java)

            intent.putExtra("model", carModelsData[position].model)
            intent.putExtra("price", carModelsData[position].price)

            startActivity(holder.view.context, intent, null)

        }

    }


    override fun getItemCount(): Int {
        return carModelsData.count()
    }
}



class CustomViewHolderModels(val view: View) : RecyclerView.ViewHolder(view) {

}





