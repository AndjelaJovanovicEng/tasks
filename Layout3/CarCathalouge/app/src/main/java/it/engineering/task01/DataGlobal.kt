package it.engineering.task01

import it.engineering.task01.models.CarModelEntity
import it.engineering.task01.models.CompanyModelEntity

object DataGlobal {
    var dataFromCSV : ArrayList<CarModelEntity> = arrayListOf()
    var companyModelFromCSV : ArrayList<CompanyModelEntity> = arrayListOf()
    var modelsForCompany:ArrayList<CarModelEntity> = arrayListOf()
}