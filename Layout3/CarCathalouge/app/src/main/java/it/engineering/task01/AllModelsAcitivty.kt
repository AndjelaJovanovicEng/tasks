package it.engineering.task01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.engineering.task01.DataGlobal.companyModelFromCSV
import it.engineering.task01.models.CarModelEntity
import it.engineering.task01.models.CompanyEntity
import it.engineering.task01.models.CompanyModelEntity
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.concurrent.ConcurrentMap

class AllModelsAcitivty : AppCompatActivity() {

    val carCompanyDataArray : ArrayList<CompanyEntity> = arrayListOf()
    val filteredCompanyModelEntities : ArrayList<CompanyModelEntity> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_models_acitivty)

        createDataSource()
        loadCSV()
        val adapter = RecyclerViewAdapter(carCompanyDataArray)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewModels)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
        //recyclerView.setPadding(20,20,20,20)

        val editSearch = findViewById<EditText>(R.id.inputSearch)
        val searchBtn = findViewById<Button>(R.id.searchBtn)

        searchBtn.setOnClickListener {
            var inputSearchText = editSearch.text.toString().trim().split(",")
            println(inputSearchText)
            var companyName = inputSearchText[0].trim().capitalize()
            var modelName = inputSearchText[1].trim().capitalize()

            for(data in companyModelFromCSV){
                if((companyName == data.companyName) && (modelName==data.modelName)){
                    filteredCompanyModelEntities.add(data)
                }
            }

            val adapter = FilterRecyclerViewAdapter(filteredCompanyModelEntities)
            val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewModels)
            recyclerView.adapter = adapter
            recyclerView.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
        }

    }

    private fun createDataSource(){
        carCompanyDataArray.add(CompanyEntity("Alfa Romeo", R.drawable.alfa_romeo))
        carCompanyDataArray.add(CompanyEntity("Audi", R.drawable.audi))
        carCompanyDataArray.add(CompanyEntity("Citroen", R.drawable.citroen))
        carCompanyDataArray.add(CompanyEntity("Dacia", R.drawable.dacia))
        carCompanyDataArray.add(CompanyEntity("Fiat", R.drawable.fiat))
        carCompanyDataArray.add(CompanyEntity("Ford", R.drawable.ford))
        carCompanyDataArray.add(CompanyEntity("Honda", R.drawable.honda))
        carCompanyDataArray.add(CompanyEntity("Hyndai", R.drawable.hyundai))
        carCompanyDataArray.add(CompanyEntity("Infititi", R.drawable.infiniti))
        carCompanyDataArray.add(CompanyEntity("Isuzu", R.drawable.isuzu))
        carCompanyDataArray.add(CompanyEntity("Jeep", R.drawable.jeep))
        carCompanyDataArray.add(CompanyEntity("Lada", R.drawable.lada))
        carCompanyDataArray.add(CompanyEntity("Mazda", R.drawable.mazda))
        carCompanyDataArray.add(CompanyEntity("Mercedes Benz", R.drawable.mercedes_benz))
        carCompanyDataArray.add(CompanyEntity("Mini", R.drawable.mini))
        carCompanyDataArray.add(CompanyEntity("Mitsubisi", R.drawable.mitsubishi))
        carCompanyDataArray.add(CompanyEntity("Nissan", R.drawable.nissan))
        carCompanyDataArray.add(CompanyEntity("Opel", R.drawable.opel))
        carCompanyDataArray.add(CompanyEntity("Peugeot", R.drawable.peugeot))
        carCompanyDataArray.add(CompanyEntity("Renault", R.drawable.renault))
        carCompanyDataArray.add(CompanyEntity("Seat", R.drawable.seat))
        carCompanyDataArray.add(CompanyEntity("Skoda", R.drawable.skoda))
        carCompanyDataArray.add(CompanyEntity("Smart", R.drawable.smart))
        carCompanyDataArray.add(CompanyEntity("Subaru", R.drawable.subaru))
        carCompanyDataArray.add(CompanyEntity("Suzuki", R.drawable.suzuki))
        carCompanyDataArray.add(CompanyEntity("Volkswagen", R.drawable.volkswagen))
        carCompanyDataArray.add(CompanyEntity("Volvo", R.drawable.volvo))
    }

    private fun loadCSV(){
        var linija: String?

        val otvoriCSV = InputStreamReader(assets.open("modelExcel.csv"))

        val procitajLiniju = BufferedReader(otvoriCSV)

        procitajLiniju.readLine()

        while(procitajLiniju.readLine().also {linija = it
                } != null){
            //Log.d("CSV", linija!!)
            val red : List<String> = linija!!.split(";")

            Log.d("CSV RED", red.toString())

            if(red[0].isEmpty()){
                Log.d("CSV", "Red je prazan")
            } else {
                companyModelFromCSV.add(CompanyModelEntity(red[0],red[1],red[4]))
            }
        }

    }
}