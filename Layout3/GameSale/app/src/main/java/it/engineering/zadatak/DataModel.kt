package it.engineering.zadatak

import android.graphics.Bitmap

data class DataModel(
    var title: String,
    var gameId: String,
    var salePrice: String,
    var normalPrice: String,
    var rating: String,
    var imgURL: String
) {
}