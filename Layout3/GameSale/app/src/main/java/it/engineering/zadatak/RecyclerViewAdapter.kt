package it.engineering.zadatak

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.lang.Exception
import java.net.URL

class RecyclerViewAdapter(private val sellData: ArrayList<DataModel>) : RecyclerView.Adapter<CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val recycleViewRow = layoutInflater.inflate(R.layout.sale_row, parent, false)
        return CustomViewHolder(recycleViewRow)

    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.findViewById<TextView>(R.id.title).text = sellData[position].title
        holder.view.findViewById<TextView>(R.id.salePrice).text = sellData[position].salePrice.plus(" $")
        holder.view.findViewById<TextView>(R.id.normalPrice).text = sellData[position].normalPrice.plus(" $")
        holder.view.findViewById<TextView>(R.id.rating).text = sellData[position].rating
        downloadImage(holder.view.findViewById(R.id.gameImage)).execute(sellData[position].imgURL)

    }

    override fun getItemCount(): Int {

        return sellData.count()
    }

    inner class downloadImage(val imageHolder: ImageView): AsyncTask<String, Void, Bitmap?>(){

        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val imageURL = params[0]

            try {
                val imageObj = URL(imageURL).openStream()
                image = BitmapFactory.decodeStream(imageObj)
            } catch (e: Exception){
                e.printStackTrace()
            }

            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            imageHolder.setImageBitmap(result)
        }

    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}