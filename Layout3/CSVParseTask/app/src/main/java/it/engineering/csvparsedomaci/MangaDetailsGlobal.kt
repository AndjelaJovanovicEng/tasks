package it.engineering.csvparsedomaci

import it.engineering.csvparsedomaci.entity.MangaEntity

object MangaDetailsGlobal {
    var mangaTitle = ""
    var mangaAuthor = ""
    var mangaIllustrator = ""
    var mangasToPass : ArrayList<MangaEntity> = arrayListOf()
}