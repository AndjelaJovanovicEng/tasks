package it.engineering.csvparsedomaci

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import it.engineering.csvparsedomaci.entity.MangaEntity
import java.io.BufferedReader
import java.io.InputStreamReader

var btnClickedStart = 0

class StartActivity : AppCompatActivity() {

    val mangaArray: ArrayList<MangaEntity> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        val btnGlobal = findViewById<Button>(R.id.globalBtn)

        btnGlobal.setOnClickListener {
            parseCSV(MangaDetailsGlobal.mangasToPass)
            btnClickedStart = 1
            startActivity(Intent(this, MainActivity::class.java))
            Toast.makeText(this, "Data import from global", Toast.LENGTH_SHORT).show()
        }

        val btnIntent = findViewById<Button>(R.id.intentBtn)
        btnIntent.setOnClickListener {
            parseCSV(mangaArray)
            btnClickedStart = 2
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("mangaArrayFromIntent", mangaArray)
            startActivity(intent)
            Toast.makeText(this, "Data import from intent", Toast.LENGTH_SHORT).show()
        }
    }

    private fun parseCSV(arrayToFill : ArrayList<MangaEntity>){
        var linija: String?

        val otvoriCSV = InputStreamReader(assets.open("textFile.csv"))

        val procitajLiniju = BufferedReader(otvoriCSV)

        Log.d("CSV", procitajLiniju.toString())

        while(procitajLiniju.readLine().also {linija = it
            } != null){
            //Log.d("CSV", linija!!)
            val red : List<String> = linija!!.split(",")
            Log.d("CSV RED", red.toString())

            if(red[0].isEmpty()){
                Log.d("CSV", "Red je prazan")
            } else {
                arrayToFill.add(MangaEntity(red[0], red[1], red[2], red[3]))
            }
        }
    }


//    private fun createDataSourceGlobal(){
//        var linija: String?
//
//        val otvoriCSV = InputStreamReader(assets.open("textFile.csv"))
//
//        val procitajLiniju = BufferedReader(otvoriCSV)
//
//        Log.d("CSV", procitajLiniju.toString())
//
//        while(procitajLiniju.readLine().also {linija = it
//            } != null){
//            Log.d("CSV", linija!!)
//            val red : List<String> = linija!!.split(",")
//            Log.d("CSV", red[0])
//
//            if(red[0].isEmpty()){
//                Log.d("CSV", "Red je prazan")
//            } else {
//                MangaDetailsGlobal.mangasToPass.add(MangaEntity(red[0], red[1], red[2], red[3]))
//            }
//        }
//    }
//
//    private fun createDataSourceIntent(){
//
//        var linija: String?
//
//        val otvoriCSV = InputStreamReader(assets.open("textFile.csv"))
//
//        val procitajLiniju = BufferedReader(otvoriCSV)
//
//        Log.d("CSV", procitajLiniju.toString())
//
//        while(procitajLiniju.readLine().also {linija = it
//            } != null){
//            Log.d("CSV", linija!!)
//            val red : List<String> = linija!!.split(",")
//            Log.d("CSV", red[0])
//
//            if(red[0].isEmpty()){
//                Log.d("CSV", "Red je prazan")
//            } else {
//                mangaArray.add(MangaEntity(red[0], red[1], red[2], red[3]))
//            }
//        }
//    }
}