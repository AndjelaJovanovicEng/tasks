package it.engineering.csvparsedomaci.entity

import java.io.Serializable


data class MangaEntity (
    var title: String,
    var author: String,
    var illustrator: String,
    var published: String
) :Serializable
