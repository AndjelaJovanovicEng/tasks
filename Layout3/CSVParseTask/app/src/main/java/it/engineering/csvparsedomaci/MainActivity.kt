package it.engineering.csvparsedomaci

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.engineering.csvparsedomaci.adapter.RecyclerViewAdapter
import it.engineering.csvparsedomaci.entity.MangaEntity
import java.io.BufferedReader
import java.io.InputStreamReader

class MainActivity : AppCompatActivity() {




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //createDataSource()

        if(btnClickedStart == 1){
            val adapter = RecyclerViewAdapter(MangaDetailsGlobal.mangasToPass)
            val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewMangas)
            recyclerView.adapter = adapter
            recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            recyclerView.setPadding(20,20,20,20)
        } else if (btnClickedStart == 2){
            val mangaArrayToPass = intent.getSerializableExtra("mangaArrayFromIntent") as ArrayList<MangaEntity>
            val adapter = RecyclerViewAdapter(mangaArrayToPass)
            val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewMangas)
            recyclerView.adapter = adapter
            recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            recyclerView.setPadding(20,20,20,20)
        }



    }

//    private fun createDataSource() {
//        var linija: String?
//
//        val otvoriCSV = InputStreamReader(assets.open("textFile.csv"))
//
//        val procitajLiniju = BufferedReader(otvoriCSV)
//
//        Log.d("CSV", procitajLiniju.toString())
//
//        while(procitajLiniju.readLine().also {linija = it
//            } != null){
//            Log.d("CSV", linija!!)
//            val red : List<String> = linija!!.split(",")
//            Log.d("CSV", red[0])
//
//            if(red[0].isEmpty()){
//                Log.d("CSV", "Red je prazan")
//            } else {
//                mangaArray.add(MangaEntity(red[0], red[1], red[2], red[3]))
//            }
//
//
//        }
//    }
}