package it.engineering.task01

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import it.engineering.task01.models.CompanyEntity
import it.engineering.task01.models.CompanyModelEntity
import org.w3c.dom.Text
import java.util.*
import kotlin.collections.ArrayList

class RecyclerViewAdapter(private val carCompanyData: ArrayList<CompanyEntity>) : RecyclerView.Adapter<CustomViewHolder>(), Filterable{

    private var filteredCompanyEntities : ArrayList<CompanyEntity> = arrayListOf()

    init {
        filteredCompanyEntities = carCompanyData
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val recycleViewRow = layoutInflater.inflate(R.layout.recycler_view_model, parent, false)
        return CustomViewHolder(recycleViewRow)

    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val modelTitle = holder.view.findViewById<TextView>(R.id.modelTitle)
        val modelLogo = holder.view.findViewById<ImageView>(R.id.modelLogo)

        val cell = holder.view.findViewById<LinearLayout>(R.id.selectedModel)

        modelLogo.setImageResource(filteredCompanyEntities[position].image)
        modelTitle.text = filteredCompanyEntities[position].title


        cell.setOnClickListener {
            val intent = Intent(holder.view.context, CompanyModelsActivity::class.java)
            intent.putExtra("manufacturer", modelTitle.text.toString())
            startActivity(holder.view.context, intent,null)
        }
    }


    override fun getItemCount(): Int {
        return filteredCompanyEntities.count()
    }

    override fun getFilter(): Filter {
        return object: Filter(){
            override fun performFiltering(constraint: CharSequence?): FilterResults {

                val inputSearch = constraint.toString()

                if(inputSearch.isEmpty()){
                    filteredCompanyEntities = carCompanyData
                } else {
                    val resultArray = ArrayList<CompanyEntity>()

                    for(row in carCompanyData){
                        if(row.title.toLowerCase().contains(inputSearch.toLowerCase())){
                            resultArray.add(row)
                        }
                    }

                    filteredCompanyEntities = resultArray
                }

                val filteredResults = FilterResults()
                filteredResults.values = filteredCompanyEntities
                return filteredResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filteredCompanyEntities = results?.values as ArrayList<CompanyEntity>
                notifyDataSetChanged()
            }
        }
    }
}


class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}