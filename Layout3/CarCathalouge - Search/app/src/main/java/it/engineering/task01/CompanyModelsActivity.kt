package it.engineering.task01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.engineering.task01.DataGlobal.dataFromCSV
import it.engineering.task01.DataGlobal.modelsForCompany
import it.engineering.task01.models.CarModelEntity
import java.io.BufferedReader
import java.io.InputStreamReader

class CompanyModelsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company_models)

        var manufacturerName = intent.getStringExtra("manufacturer")

        val companyLogo = findViewById<ImageView>(R.id.manufactLogo)

        DataGlobal.dataUtilityClass.loadLogo(this, manufacturerName, companyLogo)

        val otvoriCSV = InputStreamReader(assets.open("modelExcel.csv"))
        DataGlobal.dataUtilityClass.loadModelData(this, otvoriCSV)

        for(data in dataFromCSV){
            if(manufacturerName == data.company){
                modelsForCompany.add(data)

            }
        }

        val searchModels = findViewById<EditText>(R.id.searchModels)

        searchModels.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                filterData(p0.toString())
            }

        })

        val adapter = CarModelRecyclerViewAdapter(modelsForCompany)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewCarModels)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
    }

    private fun filterData(textInput:String){
        val filteredModels = ArrayList<CarModelEntity>()
        for(model in modelsForCompany){
            if(model.model.toLowerCase().contains(textInput.toLowerCase().trim())){
                filteredModels.add(model)
            }
        }
        val adapter1 = CarModelRecyclerViewAdapter(filteredModels)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewCarModels)
        recyclerView.adapter = adapter1
        recyclerView.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
    }

}