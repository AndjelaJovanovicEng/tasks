package it.engineering.aktivnostidomaci

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class LevelTwoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_level_two)

        val startBtn = findViewById<Button>(R.id.mainBtn)
        val forestBtn = findViewById<Button>(R.id.forestBtn)
        val tavernBtn = findViewById<Button>(R.id.tavernBtn)

        startBtn.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }

        forestBtn.setOnClickListener {
            startActivity(Intent(this, LevelOneActivity::class.java))
        }

        tavernBtn.setOnClickListener {
            startActivity(Intent(this, LevelThreeActivity::class.java))
        }
    }
}