package it.engineering.aktivnostidomaci

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class LevelThreeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_level_three_avtivity)

        val startBtn = findViewById<Button>(R.id.mainBtn)
        val forestBtn = findViewById<Button>(R.id.forestBtn)
        val castleBtn = findViewById<Button>(R.id.castleBtn)

        startBtn.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }

        forestBtn.setOnClickListener {
            startActivity(Intent(this, LevelOneActivity::class.java))
        }

        castleBtn.setOnClickListener {
            startActivity(Intent(this, LevelTwoActivity::class.java))
        }
    }
}