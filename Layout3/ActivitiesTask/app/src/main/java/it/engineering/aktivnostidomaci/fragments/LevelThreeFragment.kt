package it.engineering.aktivnostidomaci.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import it.engineering.aktivnostidomaci.R

class LevelThreeFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_level_three, container, false)

        view.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.levelThreeToLevelFour)
        }

        return view

    }
}