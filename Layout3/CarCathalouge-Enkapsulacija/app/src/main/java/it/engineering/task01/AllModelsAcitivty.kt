package it.engineering.task01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.engineering.task01.DataGlobal.companyModelFromCSV
import it.engineering.task01.models.CompanyEntity
import it.engineering.task01.models.CompanyModelEntity
import java.io.InputStreamReader


class AllModelsAcitivty : AppCompatActivity() {

    private val carCompanyDataArray : ArrayList<CompanyEntity> = arrayListOf()
    private val filteredCompanyModelEntities : ArrayList<CompanyModelEntity> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_models_acitivty)

        DataGlobal.dataUtilityClass.createDataSource(this, carCompanyDataArray)

        val otvoriCSV = InputStreamReader(assets.open("modelExcel.csv"))
        DataGlobal.dataUtilityClass.loadCompanyData(this, otvoriCSV)

        val adapter = RecyclerViewAdapter(carCompanyDataArray)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewModels)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)

        val editSearch = findViewById<EditText>(R.id.inputSearch)
        val searchBtn = findViewById<Button>(R.id.searchBtn)

        searchBtn.setOnClickListener {
            var inputSearchText = editSearch.text.toString().trim().split(",")
            println(inputSearchText)
            var companyName = inputSearchText[0].trim().capitalize()
            var modelName = inputSearchText[1].trim().capitalize()

            for(data in companyModelFromCSV){
                if((companyName == data.companyName) && (modelName==data.modelName)){
                    filteredCompanyModelEntities.add(data)

                }
            }
            val adapter = FilterRecyclerViewAdapter(filteredCompanyModelEntities)
            val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewModels)
            recyclerView.adapter = adapter
            recyclerView.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)

        }

    }
}