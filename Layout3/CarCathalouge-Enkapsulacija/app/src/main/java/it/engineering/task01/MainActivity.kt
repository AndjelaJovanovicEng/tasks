package it.engineering.task01

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val goToAllModelsBtn = findViewById<Button>(R.id.goToAllModels)
        goToAllModelsBtn.setOnClickListener {
            startActivity(Intent(this, AllModelsAcitivty::class.java))
        }

    }
}