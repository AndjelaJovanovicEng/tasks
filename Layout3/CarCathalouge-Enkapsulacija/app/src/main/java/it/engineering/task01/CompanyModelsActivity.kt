package it.engineering.task01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.engineering.task01.DataGlobal.dataFromCSV
import it.engineering.task01.DataGlobal.modelsForCompany
import it.engineering.task01.models.CarModelEntity
import java.io.BufferedReader
import java.io.InputStreamReader

class CompanyModelsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company_models)

        var manufacturerName = intent.getStringExtra("manufacturer")

        val companyLogo = findViewById<ImageView>(R.id.manufactLogo)

        DataGlobal.dataUtilityClass.loadLogo(this, manufacturerName, companyLogo)

        val otvoriCSV = InputStreamReader(assets.open("modelExcel.csv"))
        DataGlobal.dataUtilityClass.loadModelData(this, otvoriCSV)

        for(data in dataFromCSV){
            if(manufacturerName == data.company){
                modelsForCompany.add(data)

            }
        }

        val adapter = CarModelRecyclerViewAdapter(modelsForCompany)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewCarModels)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
    }

}