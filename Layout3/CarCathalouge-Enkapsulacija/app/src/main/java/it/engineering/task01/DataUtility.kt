package it.engineering.task01

import android.content.Context
import android.util.Log
import android.widget.ImageView
import it.engineering.task01.DataGlobal.dataFromCSV
import it.engineering.task01.models.CarModelEntity
import it.engineering.task01.models.CompanyEntity
import it.engineering.task01.models.CompanyModelEntity
import java.io.BufferedReader
import java.io.InputStreamReader


class DataUtility {

     fun createDataSource(context: Context, arrayToFill: ArrayList<CompanyEntity>){
        arrayToFill.add(CompanyEntity("Alfa Romeo", R.drawable.alfa_romeo))
        arrayToFill.add(CompanyEntity("Audi", R.drawable.audi))
        arrayToFill.add(CompanyEntity("Citroen", R.drawable.citroen))
        arrayToFill.add(CompanyEntity("Dacia", R.drawable.dacia))
        arrayToFill.add(CompanyEntity("Fiat", R.drawable.fiat))
        arrayToFill.add(CompanyEntity("Ford", R.drawable.ford))
        arrayToFill.add(CompanyEntity("Honda", R.drawable.honda))
        arrayToFill.add(CompanyEntity("Hyndai", R.drawable.hyundai))
        arrayToFill.add(CompanyEntity("Infititi", R.drawable.infiniti))
        arrayToFill.add(CompanyEntity("Isuzu", R.drawable.isuzu))
        arrayToFill.add(CompanyEntity("Jeep", R.drawable.jeep))
        arrayToFill.add(CompanyEntity("Lada", R.drawable.lada))
        arrayToFill.add(CompanyEntity("Mazda", R.drawable.mazda))
        arrayToFill.add(CompanyEntity("Mercedes Benz", R.drawable.mercedes_benz))
        arrayToFill.add(CompanyEntity("Mini", R.drawable.mini))
        arrayToFill.add(CompanyEntity("Mitsubisi", R.drawable.mitsubishi))
        arrayToFill.add(CompanyEntity("Nissan", R.drawable.nissan))
        arrayToFill.add(CompanyEntity("Opel", R.drawable.opel))
        arrayToFill.add(CompanyEntity("Peugeot", R.drawable.peugeot))
        arrayToFill.add(CompanyEntity("Renault", R.drawable.renault))
        arrayToFill.add(CompanyEntity("Seat", R.drawable.seat))
        arrayToFill.add(CompanyEntity("Skoda", R.drawable.skoda))
        arrayToFill.add(CompanyEntity("Smart", R.drawable.smart))
        arrayToFill.add(CompanyEntity("Subaru", R.drawable.subaru))
        arrayToFill.add(CompanyEntity("Suzuki", R.drawable.suzuki))
        arrayToFill.add(CompanyEntity("Volkswagen", R.drawable.volkswagen))
        arrayToFill.add(CompanyEntity("Volvo", R.drawable.volvo))
    }

     fun loadCompanyData(context: Context, inputStreamReader: InputStreamReader){
         var linija: String?

         val procitajLiniju = BufferedReader(inputStreamReader)

         procitajLiniju.readLine()

         while(procitajLiniju.readLine().also {linija = it
             } != null){
             //Log.d("CSV", linija!!)
             val red : List<String> = linija!!.split(";")

             Log.d("CSV RED", red.toString())

             if(red[0].isEmpty()){
                 Log.d("CSV", "Red je prazan")
             } else {
                 DataGlobal.companyModelFromCSV.add(CompanyModelEntity(red[0],red[1],red[4]))
             }
         }
     }

     fun loadLogo(context: Context, manufactName:String?, imageView: ImageView){

        when(manufactName){
            "Alfa Romeo" -> imageView.setImageResource(R.drawable.alfa_romeo)
            "Audio"-> imageView.setImageResource(R.drawable.audi)
            "Citroen"-> imageView.setImageResource(R.drawable.citroen)
            "Dacia"-> imageView.setImageResource(R.drawable.dacia)
            "Fiat"-> imageView.setImageResource(R.drawable.fiat)
            "Ford"-> imageView.setImageResource(R.drawable.ford)
            "Honda"-> imageView.setImageResource(R.drawable.honda)
            "Hyndai"-> imageView.setImageResource(R.drawable.hyundai)
            "Infititi"-> imageView.setImageResource(R.drawable.infiniti)
            "Isuzu"-> imageView.setImageResource(R.drawable.isuzu)
            "Jeep"-> imageView.setImageResource(R.drawable.jeep)
            "Lada"-> imageView.setImageResource(R.drawable.lada)
            "Mazda"-> imageView.setImageResource(R.drawable.mazda)
            "Mercedes Benz"-> imageView.setImageResource(R.drawable.mercedes_benz)
            "Mini"-> imageView.setImageResource(R.drawable.mini)
            "Mitsubisi"-> imageView.setImageResource(R.drawable.mitsubishi)
            "Nissan"-> imageView.setImageResource(R.drawable.nissan)
            "Opel"-> imageView.setImageResource(R.drawable.opel)
            "Peugeot"-> imageView.setImageResource(R.drawable.peugeot)
            "Renault"-> imageView.setImageResource(R.drawable.renault)
            "Seat"-> imageView.setImageResource(R.drawable.seat)
            "Skoda"-> imageView.setImageResource(R.drawable.skoda)
            "Smart"-> imageView.setImageResource(R.drawable.smart)
            "Subaru"-> imageView.setImageResource(R.drawable.subaru)
            "Suzuki"-> imageView.setImageResource(R.drawable.suzuki)
            "Volkswagen"-> imageView.setImageResource(R.drawable.volkswagen)
            "Volvo"-> imageView.setImageResource(R.drawable.volvo)
        }
    }

     fun loadModelData(context: Context, inputStreamReader: InputStreamReader){
         var linija: String?

         val procitajLiniju = BufferedReader(inputStreamReader)

        procitajLiniju.readLine()

        while(procitajLiniju.readLine().also {linija = it
            } != null){
            //Log.d("CSV", linija!!)
            val red : List<String> = linija!!.split(";")

            Log.d("CSV RED", red.toString())

            if(red[0].isEmpty()){
                Log.d("CSV", "Red je prazan")
            } else {
                dataFromCSV.add(
                    CarModelEntity(red[0],red[1],red[2],red[3],red[4],red[5],red[6],red[7],red[8],red[9], red[10],
                    red[11],red[12],red[13],red[14],red[15])
                )
            }
        }

    }
}