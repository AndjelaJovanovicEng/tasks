package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun printInputData(view: View){
        val editFirstNumber= findViewById<EditText>(R.id.editWord)
        val editSecondNumber = findViewById<EditText>(R.id.editSentence)

        val labelResult = findViewById<TextView>(R.id.labelResult)

        val numberA : Int? = editFirstNumber.text.toString().toIntOrNull();
        val numberB : Int? = editSecondNumber.text.toString().toIntOrNull();

        if(numberA != null && numberB != null){
            val result : Int = numberA!! + numberB!!
            labelResult.text = "Result: $result"
        } else {
            labelResult.text  = "Error! Input values are null!"
        }
    }

    fun checkWordPalindrom(view: View){
        val editWord = findViewById<EditText>(R.id.editWord)
        val labelResult = findViewById<TextView>(R.id.labelResult)

        val inputWord = editWord.text.toString().toUpperCase()
        val reverseWord = inputWord.reversed();


        if(!inputWord.equals("")){
            if(inputWord.equals(reverseWord)){
                labelResult.text = "Word is palindrome!"
            } else {
                labelResult.text = "Word is not palindrome!"
            }
        } else {
            labelResult.text = "You haven't inserted a word!"
        }

    }

    fun checkSentencePalindrome(view:View){
        val editSentence = findViewById<EditText>(R.id.editSentence)
        val labelResult = findViewById<TextView>(R.id.labelResult)

        val inputSentence = editSentence.text.toString().toUpperCase()
        //println("Input sentece: " + inputSentence)
        val sentenceToCheck = inputSentence.replace("[^a-zA-Z]".toRegex(), "")
       // println("Sentence to check: " + sentenceToCheck)
        val reverseSentece = sentenceToCheck.reversed();
       // println("Reveresed: " + reverseSentece)

        if(!inputSentence.equals("")){
            if(sentenceToCheck.equals(reverseSentece)){
                labelResult.text = "Sentence is palindrome!"
            } else {
                labelResult.text = "Sentence is not palindrome"
            }
        } else {
            labelResult.text = "You haven't entered a sentence!"
        }
    }
}