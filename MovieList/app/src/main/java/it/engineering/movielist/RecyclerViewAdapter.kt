package it.engineering.movielist

import android.content.Intent
import android.media.Image
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewAdapter(private val moviesData: ArrayList<MovieEntity>) : RecyclerView.Adapter<CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val recycleViewRow = layoutInflater.inflate(R.layout.movie_cell, parent, false)
        return CustomViewHolder(recycleViewRow)

    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.findViewById<TextView>(R.id.movieTitle).text = moviesData[position].title
        holder.view.findViewById<TextView>(R.id.originalTitle).text = moviesData[position].originalTitle
        holder.view.findViewById<TextView>(R.id.director).text = moviesData[position].director
        holder.view.findViewById<TextView>(R.id.producer).text = moviesData[position].producer

        holder.view.findViewById<CardView>(R.id.movieCell).setOnClickListener {
            val intent = Intent(holder.view.context, MoveDetailsActivity::class.java)
            intent.putExtra("movie", moviesData[position].title)
            intent.putExtra("desc", moviesData[position].description)
            intent.putExtra("runningTime", moviesData[position].runningTime)
            ContextCompat.startActivity(holder.view.context, intent, null)
        }

    }

    override fun getItemCount(): Int {

        return moviesData.count()
    }

}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}