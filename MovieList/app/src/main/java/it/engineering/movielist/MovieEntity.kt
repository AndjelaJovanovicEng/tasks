package it.engineering.movielist

data class MovieEntity(
    var title: String,
    var originalTitle: String,
    var director: String,
    var producer: String,
    var description: String,
    var year: String,
    var runningTime: String
) {
}