package it.engineering.movielist

import android.content.DialogInterface
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MoveDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_move_details)

        val movieTitle = intent.getStringExtra("movie")
        val runningTime = intent.getStringExtra("runningTime")

        val runningTimeInt = runningTime!!.toInt()

        if(runningTimeInt > 120){
            val movieDetailsBackground = findViewById<LinearLayout>(R.id.movieDetailsBackground)
            movieDetailsBackground.setBackgroundColor(Color.parseColor("#990000"))

            val dialogBuilder = AlertDialog.Builder(this)
            dialogBuilder.setMessage("Running time longer than 2h!").setPositiveButton(
                    "OK", DialogInterface.OnClickListener { dialog, id -> dialog.dismiss() }
            ).setNegativeButton(
                    "CANCEL", DialogInterface.OnClickListener{dialog, id -> dialog.dismiss()}
            )

            val alertDialog = dialogBuilder.create()
            alertDialog.setTitle("Warning!")
            alertDialog.show()

        } else {
            val movieDetailsBackground = findViewById<LinearLayout>(R.id.movieDetailsBackground)
            Toast.makeText(this, "Running time less than 2h", Toast.LENGTH_SHORT).show()
            movieDetailsBackground.setBackgroundColor(Color.parseColor("#009900"))
        }

        setImage(movieTitle)

        findViewById<TextView>(R.id.description).text = intent.getStringExtra("desc")


    }

    private fun setImage(title:String?){
        val movieImage = findViewById<ImageView>(R.id.movieImage)
        when(title){
            "Castle in the Sky" -> movieImage.setImageResource(R.drawable.castle_in_the_sky)
            "Grave of the Fireflies" -> movieImage.setImageResource(R.drawable.grave)
            "My Neighbor Totoro" -> movieImage.setImageResource(R.drawable.totoro)
            "Kiki's Delivery Service" -> movieImage.setImageResource(R.drawable.kiki)
            "Only Yesterday" -> movieImage.setImageResource(R.drawable.yesterday)
            "Porco Rosso" -> movieImage.setImageResource(R.drawable.porco)
            "Pom Poko" -> movieImage.setImageResource(R.drawable.pom)
            "Whisper of the Heart" -> movieImage.setImageResource(R.drawable.whisper)
            "Princess Mononoke" -> movieImage.setImageResource(R.drawable.mononoke)
            "My Neighbors the Yamadas" -> movieImage.setImageResource(R.drawable.yamadas)
            "Spirited Away" -> movieImage.setImageResource(R.drawable.spirited)
            "The Cat Returns" -> movieImage.setImageResource(R.drawable.cat_returns)
            "Howl's Moving Castle" -> movieImage.setImageResource(R.drawable.howl)
            "Tales from Earthsea" -> movieImage.setImageResource(R.drawable.tales)
            "Ponyo" -> movieImage.setImageResource(R.drawable.ponyo)
            "Arrietty" -> movieImage.setImageResource(R.drawable.arrietty)
            "From Up on Poppy Hill" -> movieImage.setImageResource(R.drawable.from_up)
            "The Wind Rises" -> movieImage.setImageResource(R.drawable.wind)
            "The Tale of the Princess Kaguya" -> movieImage.setImageResource(R.drawable.tale_princess)
            "When Marnie Was There" -> movieImage.setImageResource(R.drawable.marine)
            "The Red Turtle" -> movieImage.setImageResource(R.drawable.red_turtle)
        }
    }
}

