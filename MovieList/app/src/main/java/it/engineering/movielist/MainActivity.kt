package it.engineering.movielist

import android.graphics.Movie
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONArray
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity() {

    val movieList:ArrayList<MovieEntity> = arrayListOf()
    val filterMoviesByYear: ArrayList<MovieEntity> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val jsonURL = "https://ghibliapi.herokuapp.com/films"
        val buttonFilterYear1 = findViewById<Button>(R.id.year1)
        val buttonFilterYear2 = findViewById<Button>(R.id.year2)

        getJSON().execute(jsonURL)

        buttonFilterYear1.setOnClickListener {
            filterMoviesByYear.clear()
            filterByYear(buttonFilterYear1.text.toString())
        }

        buttonFilterYear2.setOnClickListener {
            filterMoviesByYear.clear()
            filterByYear(buttonFilterYear2.text.toString())
        }

        val searchMovies = findViewById<EditText>(R.id.searchMovies)

        searchMovies.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                filterMovies(p0.toString())
            }

        })

    }

    private fun filterMovies(textInput:String){
        val filteredMovies = ArrayList<MovieEntity>()

        for(movie in movieList){
            if(movie.title.toLowerCase().contains(textInput.toLowerCase().trim())){
                filteredMovies.add(movie)
            }
        }

        val recyclerViewMovies = findViewById<RecyclerView>(R.id.allMovies)
        val adapter = RecyclerViewAdapter(filteredMovies)
        recyclerViewMovies.adapter = adapter
        recyclerViewMovies.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerViewMovies.setPadding(20,20,20,20)
    }

    private fun filterByYear(year: String){
        for (movie in movieList){
            if(movie.year == year){
                filterMoviesByYear.add(movie)
            }
        }

        val recyclerViewMovies = findViewById<RecyclerView>(R.id.allMovies)
        val adapter = RecyclerViewAdapter(filterMoviesByYear)
        recyclerViewMovies.adapter = adapter
        recyclerViewMovies.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerViewMovies.setPadding(20,20,20,20)
    }

    inner  class getJSON: AsyncTask<String, String, String>(){

        override fun doInBackground(vararg params: String?): String {
            var json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                            reader -> reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }

            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            parseJSON(result)

            val recyclerViewMovies = findViewById<RecyclerView>(R.id.allMovies)
            val adapter = RecyclerViewAdapter(movieList)
            recyclerViewMovies.adapter = adapter
            recyclerViewMovies.layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)
            recyclerViewMovies.setPadding(20,20,20,20)


        }

        private fun parseJSON(jsonToParse:String?){
            val jsonArray = JSONArray(jsonToParse)

            println(jsonArray)

            jsonArray.let {
                (0 until it.length()).forEach{
                    val jsonObj = jsonArray.optJSONObject(it)

                    val title = jsonObj.getString("title")
                    val originalTitle = jsonObj.getString("original_title")
                    val director = jsonObj.getString("director")
                    val producer = jsonObj.getString("producer")
                    val description = jsonObj.getString("description")
                    val year = jsonObj.getString("release_date")
                    var runningTime = jsonObj.getString("running_time")

                    movieList.add(MovieEntity(title, originalTitle, director, producer, description, year, runningTime))

                }

            }

            println(movieList)

        }

    }




}