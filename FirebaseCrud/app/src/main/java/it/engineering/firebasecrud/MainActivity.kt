package it.engineering.firebasecrud

import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.Movie
import android.media.Image
import android.media.Rating
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.google.firebase.database.*

class MainActivity : AppCompatActivity() {

    lateinit var movieName: EditText
    lateinit var movieRating: RatingBar

    lateinit var btnCreate: Button
    lateinit var btnRead: Button

    lateinit var getByRatingBtn: Button
    lateinit var getBySortingAsc : ImageView
    lateinit var getBySortingDesc : ImageView

    lateinit var movieList: ListView
    lateinit var movieListByRating: MutableList<MovieEntity>
    lateinit var sortedMoviesAsc: MutableList<MovieEntity>
    lateinit var sortedMoviesDesc: List<MovieEntity>
    lateinit var movies: MutableList<MovieEntity>

    lateinit var ref: DatabaseReference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        movieName = findViewById(R.id.movieName)
        movieRating = findViewById(R.id.movieRating)
        btnCreate = findViewById(R.id.createBtn)
        btnRead = findViewById(R.id.readBtn)
        getByRatingBtn = findViewById(R.id.getByRating)
        getBySortingAsc = findViewById(R.id.getBySortingAsc)
        getBySortingDesc = findViewById(R.id.getBySortingDesc)

        movieList = findViewById(R.id.displayData)

        movies = mutableListOf()
        movieListByRating = mutableListOf()
        sortedMoviesAsc = mutableListOf()
        sortedMoviesDesc = listOf()




        btnCreate.setOnClickListener {
            saveMovie()
        }

        btnRead.setOnClickListener {
            getBySortingAsc.visibility = View.VISIBLE
            getBySortingDesc.visibility = View.VISIBLE
            readMovies()
        }

        getByRatingBtn.setOnClickListener {
            getBySortingAsc.visibility = View.GONE
            getBySortingDesc.visibility = View.GONE
            readMoviesByRating()
        }

        getBySortingAsc.setOnClickListener {
            sortMoviesAsc()
        }

        getBySortingDesc.setOnClickListener {
            sortMoviesDesc()
        }

        //definisemo node u okviru koga cuvamo podatke
        ref = FirebaseDatabase.getInstance().getReference("movies")

    }

    //cuvanje
    private fun saveMovie() {
        val title = movieName.text.toString()
        if(title.isEmpty()){
            movieName.error = "Please enter title"
            return
        }

        //definisemo unique key u okviru movies cvora
        val movieId = ref.push().key

        if(movieId != null){
            val movie = MovieEntity(movieId, title, movieRating.rating.toInt())
            ref.child(movieId).setValue(movie).addOnCompleteListener {
                if(it.isSuccessful){
                    Toast.makeText(this, "Success!", Toast.LENGTH_SHORT)
                } else {
                    Toast.makeText(this, "Error!", Toast.LENGTH_SHORT)
                }
            }
        }
    }


    //iscitavanje
    private fun readMovies(){
        ref.addValueEventListener(object: ValueEventListener{
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

            //metod za iscitavanje podataka iz firebase-a
            override fun onDataChange(snapshot: DataSnapshot) {
                //snapshot - svi podaci iz cvora ref. preko ref promeljive

                if(snapshot.exists()){
                    movies.clear()

                    for(movie in snapshot.children){
                        val movieEntity = movie.getValue(MovieEntity::class.java)
                        movies.add(movieEntity!!)
                    }

                    val adapter = MovieListAdapter(this@MainActivity, R.layout.movies, movies)
                    movieList.adapter = adapter
                }

            }

        })
    }

    private fun readMoviesByRating(){
        val dialogBuilder =  AlertDialog.Builder(this)

        val inflater = LayoutInflater.from(this)
        val view = inflater.inflate(R.layout.alert_dialog_rating, null)

        val editTextRating = view.findViewById<EditText>(R.id.movieRating)


        dialogBuilder.setView(view)
        dialogBuilder.setTitle("Enter movie rating")

        dialogBuilder.setPositiveButton("OK", object: DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {

                val ratingToSearch = editTextRating.text.toString()
                //println(ratingToSearch.toInt().toDouble())

                if(ratingToSearch.isEmpty()){
                    editTextRating.error = "Enter rating"
                    return
                }

                val query = ref.orderByChild("rating").endAt(ratingToSearch.toDouble())
                query.addListenerForSingleValueEvent(object: ValueEventListener{
                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }

                    override fun onDataChange(snapshot: DataSnapshot) {
                        if(snapshot.exists()){
                            movieListByRating.clear()

                            for(movie in snapshot.children){
                                val movieEntity = movie.getValue(MovieEntity::class.java)
                                movieListByRating.add(movieEntity!!)
                            }

                            val adapter = MovieListAdapter(this@MainActivity, R.layout.movies, movieListByRating)
                            movieList.adapter = adapter
                        }
                    }

                })



            }

        })

        dialogBuilder.setNegativeButton("Dismiss", object: DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                dialog!!.dismiss()
            }

        })

        val alertDialog = dialogBuilder.create()
        alertDialog.show()

    }

    private fun sortMoviesAsc(){
        val query = ref.orderByChild("rating")
        query.addListenerForSingleValueEvent(object: ValueEventListener{
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists()){
                    movies.clear()


                    for(movie in snapshot.children){
                        val movieEntity = movie.getValue(MovieEntity::class.java)
                        movies.add(movieEntity!!)
                    }

                    val adapter = MovieListAdapter(this@MainActivity, R.layout.movies, movies)
                    movieList.adapter = adapter

                }
            }

        })

    }

    private fun sortMoviesDesc(){

        //movies

        sortedMoviesDesc = movies.sortedByDescending { it.rating }
        val adapter = MovieListAdapter(this@MainActivity, R.layout.movies, sortedMoviesDesc)
        movieList.adapter = adapter

    }
}