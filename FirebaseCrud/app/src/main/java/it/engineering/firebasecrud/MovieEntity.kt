package it.engineering.firebasecrud

class MovieEntity ( val id:  String, val title:String, val rating: Int){
    constructor(): this("","",0)
}