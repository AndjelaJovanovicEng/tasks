package it.engineering.firebasecrud

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.firebase.database.FirebaseDatabase


class MovieListAdapter(val mCtx: Context, val layoutResId: Int, val movieList: List<MovieEntity>) : ArrayAdapter<MovieEntity>(mCtx, layoutResId, movieList) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater: LayoutInflater = LayoutInflater.from(mCtx)

        val view: View = layoutInflater.inflate(layoutResId, null)

        val textViewTitle = view.findViewById<TextView>(R.id.movie)

        val textViewRating = view.findViewById<TextView>(R.id.rating)

        val textViewUpdate = view.findViewById<TextView>(R.id.updateMovie)

        val deleteMovie = view.findViewById<TextView>(R.id.deleteMovie)


        val movie = movieList[position]

        textViewUpdate.setOnClickListener {
            showDialog(movie)
        }

        textViewTitle.text = movie.title
        textViewRating.text = movie.rating.toString()

        deleteMovie.setOnClickListener {
            val ref = FirebaseDatabase.getInstance().getReference("movies")

            ref.child(movie.id).removeValue()
        }

        return view


    }

    private fun showDialog(movie: MovieEntity) {
        val dialogBuilder =  AlertDialog.Builder(mCtx)

        val inflater = LayoutInflater.from(mCtx)
        val view = inflater.inflate(R.layout.alert_dialog, null)

        val editText = view.findViewById<EditText>(R.id.movieNameUpdate)
        val ratingBar = view.findViewById<RatingBar>(R.id.movieRatingUpdate)

        editText.setText(movie.title)
        ratingBar.rating = movie.rating.toFloat()

        dialogBuilder.setView(view)
        dialogBuilder.setTitle("Update movie")

        dialogBuilder.setPositiveButton("Update", object: DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                val ref = FirebaseDatabase.getInstance().getReference("movies")

                val newTitle = editText.text.toString()

                if(newTitle.isEmpty()){
                    editText.error = "Enter title"
                    return
                }

                val newMovie = MovieEntity(movie.id, newTitle, ratingBar.rating.toInt())

                ref.child(movie.id).setValue(newMovie)

                Toast.makeText(mCtx, "Updated", Toast.LENGTH_SHORT)
            }

        })

        dialogBuilder.setNegativeButton("Dismiss", object: DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                dialog!!.dismiss()
            }

        })

        val alertDialog = dialogBuilder.create()
        alertDialog.show()

    }

}