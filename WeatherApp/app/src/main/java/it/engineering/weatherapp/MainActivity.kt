package it.engineering.weatherapp

import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.ScrollView
import android.widget.TextView
import org.json.JSONObject
import org.w3c.dom.Text
import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        readJson()
        val showMore = findViewById<ImageView>(R.id.showMore)
        showMore.setOnClickListener {
            toogleVisibility()
        }
    }



    private fun readJson() {

        val json: String?
        val city = findViewById<TextView>(R.id.city)
        val lon = findViewById<TextView>(R.id.lon)
        val lat = findViewById<TextView>(R.id.lat)
        val temperature = findViewById<TextView>(R.id.temperature)
        val feelsLike = findViewById<TextView>(R.id.feelsLike)
        val tempMin = findViewById<TextView>(R.id.minTemp)
        val tempMax = findViewById<TextView>(R.id.maxTemp)
        val pressure = findViewById<TextView>(R.id.pressure)
        val humidity = findViewById<TextView>(R.id.humidity)

        val weatherMain = findViewById<TextView>(R.id.weather)
        val desc = findViewById<TextView>(R.id.description)

        val windSpeed = findViewById<TextView>(R.id.windSpeed)
        val windDeg = findViewById<TextView>(R.id.windDeg)
        val windGust = findViewById<TextView>(R.id.windGust)

        try{
            val inputStream: InputStream = assets.open("weather.json")

            json = inputStream.bufferedReader().use {
                it.readText()
            }

            val obj = JSONObject(json)

            val location = obj.getJSONObject("coord")
            lon.text = "Lon: " + location.getString("lon")
            lat.text = "Lat: " + location.getString("lat")

            val mainObj = obj.getJSONObject("main")

            temperature.text = mainObj.getString("temp").plus(" K")
            feelsLike.text = mainObj.getString("feels_like").plus(" K")
            tempMin.text = mainObj.getString("temp_min").plus(" K")
            tempMax.text = mainObj.getString("temp_max").plus(" K")
            pressure.text = mainObj.getString("pressure").plus(" mbar")
            humidity.text = mainObj.getString("humidity").plus(" %")

            city.text = obj.getString("name")

            val windObj = obj.getJSONObject("wind")
            windSpeed.text = windObj.getString("speed").plus(" m/s")
            windDeg.text = windObj.getString("deg")
            windGust.text = windObj.getString("gust")

            val weatherImg = findViewById<ImageView>(R.id.weatherImg)

            val weatherArr = obj.getJSONArray("weather")

            weatherArr.let{
                (0 until it.length()).forEach{
                    val weatherObj = weatherArr.getJSONObject(it)
                    weatherMain.text = weatherObj.getString("main")
                    val description = weatherObj.getString("description")
                    desc.text = description
                    if(description == "broken clouds"){
                        weatherImg.setImageResource(R.drawable.cloud)
                    } else if (description == "sunny"){
                        weatherImg.setImageResource(R.drawable.sunny)
                    } else if (description == "rainy"){
                        weatherImg.setImageResource(R.drawable.rainy)
                    }


                }
            }

        } catch (e: IOException){
            e.printStackTrace()
        }

    }

    private fun toogleVisibility(){
        val showMoreView = findViewById<ScrollView>(R.id.showMoreView)
        if(showMoreView.visibility == View.INVISIBLE){
            showMoreView.visibility = View.VISIBLE
        } else {
            showMoreView.visibility = View.INVISIBLE
        }


    }
}