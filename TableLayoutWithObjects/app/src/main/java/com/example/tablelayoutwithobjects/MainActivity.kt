package com.example.tablelayoutwithobjects

import android.app.Person
import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.setPadding

class MainActivity : AppCompatActivity() {
    data class People(val firstName: String, val lastName: String, val years: Int)

    var tableContent: ArrayList<People> = arrayListOf()




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createDatasource()
        addPersonToTable()

        val addButton = findViewById<Button>(R.id.addDataToArray)
        val editTexName = findViewById<EditText>(R.id.imeEditText)
        val editTexLastName = findViewById<EditText>(R.id.prezimeEditText)
        val editTexYears = findViewById<EditText>(R.id.godineEditText)



        addButton.setOnClickListener {
            var yearsText:Int = editTexYears.text.toString().toInt()
            tableContent.add(People(editTexName.text.toString(), editTexLastName.text.toString(), yearsText))
            addPersonToTable()
        }
    }

    private fun addPersonToTable(){
        for(person in tableContent){
            val row = TableRow(this)
            row.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            row.gravity = Gravity.CENTER
            for(count in 1..3){
                val textView = TextView(this)
                //property
                textView.textSize = 15f
                textView.gravity = Gravity.CENTER
                textView.setPadding(25)
                textView.setBackgroundResource(R.color.blue)
                textView.setTextColor(Color.parseColor("#000000"))
                textView.setTypeface(null, Typeface.BOLD)
                //data
                textView.apply {
                    if(count == 1){
                        textView.text = person.firstName
                    } else if (count == 2){
                        textView.text = person.lastName
                    } else {
                        textView.text = person.years.toString()
                    }
                }
                row.addView(textView)
            }
            val tableView = findViewById<TableLayout>(R.id.tableView)
            tableView.isStretchAllColumns = true

            tableView.addView(row)
        }
    }

    private fun createDatasource(){
        tableContent.add(People("Pera", "Peric", 21))
        tableContent.add(People("Pera", "Peric", 21))
        tableContent.add(People("Pera", "Peric", 21))
        tableContent.add(People("Pera", "Peric", 21))
        tableContent.add(People("Pera", "Peric", 21))
        tableContent.add(People("Pera", "Peric", 21))
        tableContent.add(People("Pera", "Peric", 21))
        tableContent.add(People("Pera", "Peric", 21))
        tableContent.add(People("Pera", "Peric", 21))
        tableContent.add(People("Pera", "Peric", 21))
        tableContent.add(People("Pera", "Peric", 21))
        tableContent.add(People("Pera", "Peric", 21))
        tableContent.add(People("Pera", "Peric", 21))
        tableContent.add(People("Pera", "Peric", 21))
        tableContent.add(People("Pera", "Peric", 21))

    }


}