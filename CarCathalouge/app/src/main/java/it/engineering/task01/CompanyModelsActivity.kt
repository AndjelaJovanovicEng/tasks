package it.engineering.task01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.engineering.task01.DataGlobal.dataFromCSV
import it.engineering.task01.DataGlobal.modelsForCompany
import it.engineering.task01.models.CarModelEntity
import java.io.BufferedReader
import java.io.InputStreamReader

class CompanyModelsActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company_models)

        var manufacturerName = intent.getStringExtra("manufacturer")

        loadLogo(manufacturerName)

        loadCSV(dataFromCSV)

        for(data in dataFromCSV){
            if(manufacturerName == data.company){
                modelsForCompany.add(data)

            }
        }

        val adapter = CarModelRecyclerViewAdapter(modelsForCompany)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewCarModels)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
        //recyclerView.setPadding(20,20,20,20)


    }



    private fun loadCSV(arrayToFill : ArrayList<CarModelEntity>){
        var linija: String?

        val otvoriCSV = InputStreamReader(assets.open("modelExcel.csv"))

        val procitajLiniju = BufferedReader(otvoriCSV)

        procitajLiniju.readLine()

        while(procitajLiniju.readLine().also {linija = it
            } != null){
            //Log.d("CSV", linija!!)
            val red : List<String> = linija!!.split(";")

            Log.d("CSV RED", red.toString())

            if(red[0].isEmpty()){
                Log.d("CSV", "Red je prazan")
            } else {
                arrayToFill.add(CarModelEntity(red[0],red[1],red[2],red[3],red[4],red[5],red[6],red[7],red[8],red[9], red[10],
                                                                                    red[11],red[12],red[13],red[14],red[15]))
            }
        }

    }

    private fun loadLogo(manufactName:String?){
        val manufactLogo = findViewById<ImageView>(R.id.manufactLogo)

        when(manufactName){
            "Alfa Romeo" -> manufactLogo.setImageResource(R.drawable.alfa_romeo)
            "Audio"-> manufactLogo.setImageResource(R.drawable.audi)
            "Citroen"-> manufactLogo.setImageResource(R.drawable.citroen)
            "Dacia"-> manufactLogo.setImageResource(R.drawable.dacia)
            "Fiat"-> manufactLogo.setImageResource(R.drawable.fiat)
            "Ford"-> manufactLogo.setImageResource(R.drawable.ford)
            "Honda"-> manufactLogo.setImageResource(R.drawable.honda)
            "Hyndai"-> manufactLogo.setImageResource(R.drawable.hyundai)
            "Infititi"-> manufactLogo.setImageResource(R.drawable.infiniti)
            "Isuzu"-> manufactLogo.setImageResource(R.drawable.isuzu)
            "Jeep"-> manufactLogo.setImageResource(R.drawable.jeep)
            "Lada"-> manufactLogo.setImageResource(R.drawable.lada)
            "Mazda"-> manufactLogo.setImageResource(R.drawable.mazda)
            "Mercedes Benz"-> manufactLogo.setImageResource(R.drawable.mercedes_benz)
            "Mini"-> manufactLogo.setImageResource(R.drawable.mini)
            "Mitsubisi"-> manufactLogo.setImageResource(R.drawable.mitsubishi)
            "Nissan"-> manufactLogo.setImageResource(R.drawable.nissan)
            "Opel"-> manufactLogo.setImageResource(R.drawable.opel)
            "Peugeot"-> manufactLogo.setImageResource(R.drawable.peugeot)
            "Renault"-> manufactLogo.setImageResource(R.drawable.renault)
            "Seat"-> manufactLogo.setImageResource(R.drawable.seat)
            "Skoda"-> manufactLogo.setImageResource(R.drawable.skoda)
            "Smart"-> manufactLogo.setImageResource(R.drawable.smart)
            "Subaru"-> manufactLogo.setImageResource(R.drawable.subaru)
            "Suzuki"-> manufactLogo.setImageResource(R.drawable.suzuki)
            "Volkswagen"-> manufactLogo.setImageResource(R.drawable.volkswagen)
            "Volvo"-> manufactLogo.setImageResource(R.drawable.volvo)
        }
    }

}