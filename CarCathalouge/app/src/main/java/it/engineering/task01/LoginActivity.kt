package it.engineering.task01

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class LoginActivity : AppCompatActivity() {
    private val userName = "pera"
    private val passwd = "peric"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val editUsername = findViewById<EditText>(R.id.editUsername)
        val editPassword = findViewById<EditText>(R.id.editPasswd)
        val btnLogin = findViewById<Button>(R.id.buttonLogin)

        btnLogin.setOnClickListener {
            var inputUsernameString = editUsername.text.toString()
            var inputPasswordString = editPassword.text.toString()

            if((userName == inputUsernameString) && (passwd == inputPasswordString)){
                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra("username", inputUsernameString)
                intent.putExtra("passwd", inputPasswordString)
                startActivity(intent)
            } else {
                Toast.makeText(this, "Wrong credentials!", Toast.LENGTH_SHORT).show()
            }
        }
    }
}