package it.engineering.task01.models

data class CarModelEntity (
   var company : String,
   var model: String,
   var engine:String,
   var equipment:String,
   var price: String,
   var cilinders:String,
   var ventiles:String,
   var diametar:String,
   var injectionType:String,
   var openSystem: String,
   var turbo: String,
   var volume:String,
   var KW: String,
   var KS: String,
   var powerSpin: String,
   var spinMoment: String
)
{}