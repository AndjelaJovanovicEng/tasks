package it.engineering.shoppingapp.ui

import androidx.lifecycle.ViewModel
import it.engineering.shoppingapp.entity.ShoppingItem
import it.engineering.shoppingapp.repository.ShoppingRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ShoppingViewModel(
    private val repository: ShoppingRepository
) : ViewModel()  {

    //koristimo korutine za izvrsavanje metoda
    //slicno kao kontroler u MVC-u, poziva metode iz repozitorija

    fun upsert(item: ShoppingItem) =
        GlobalScope.launch {
            repository.upsert(item)
        }

    fun delete(item: ShoppingItem) = GlobalScope.launch {
        repository.delete(item)
    }

    fun getAllShoppingItems() = repository.getAllShoppingItems()
}