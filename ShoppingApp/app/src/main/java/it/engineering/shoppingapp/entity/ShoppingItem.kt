package it.engineering.shoppingapp.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


//govori kotlin kompajleru da ova klasa reprezentuje podatke

@Entity(tableName = "shopping_items")
data class ShoppingItem(
    //kolona u bazi, postoji opcija dodati anotaciju @ColumnInfo(name="")
    @ColumnInfo(name = "item_name")
    var name: String,
    @ColumnInfo(name = "item_amount")
    var amount: Int
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
}