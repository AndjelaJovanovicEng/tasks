package it.engineering.shoppingapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import it.engineering.shoppingapp.dialog.AddDialogListener
import it.engineering.shoppingapp.dialog.AddShoppingItemDialog
import it.engineering.shoppingapp.entity.ShoppingItem
import it.engineering.shoppingapp.ui.ShoppingViewModel
import it.engineering.shoppingapp.ui.ShoppingViewModelFactory
import kotlinx.android.synthetic.main.activity_shopping.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class ShoppingActivity : AppCompatActivity(), KodeinAware {

    //losa je praksa da se objekti instanciraju u okviru aktivnosti jer onda postaju
    //zavisni od te konkretne aktivnosti i zato je dobro koristiti dependency injection

    override val kodein by kodein()
    private val factory: ShoppingViewModelFactory by instance() //shoppingModelFactory instance

    lateinit var viewModel: ShoppingViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shopping)

        viewModel = ViewModelProvider(this, factory).get(ShoppingViewModel::class.java)

        val adapter = ShoppingItemAdapter(listOf(), viewModel)

        rvShoppingItems.layoutManager = LinearLayoutManager(this)
        rvShoppingItems.adapter = adapter

        viewModel.getAllShoppingItems().observe(this, Observer {
            //observer se poziva svaki put kada dodje do promene podatka u bazi
            //observer osluskuje LiveData
            adapter.items = it
            adapter.notifyDataSetChanged()
        })

        fab.setOnClickListener {
            AddShoppingItemDialog(
                this,
                object : AddDialogListener {
                    override fun onAddButtonClicked(item: ShoppingItem) {
                        viewModel.upsert(item)
                    }
                }).show()
        }
    }
}