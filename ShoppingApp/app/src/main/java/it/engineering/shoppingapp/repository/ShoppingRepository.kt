package it.engineering.shoppingapp.repository

import it.engineering.shoppingapp.db.ShoppingDatabase
import it.engineering.shoppingapp.entity.ShoppingItem

class ShoppingRepository(
    private val db: ShoppingDatabase
) {
    //implementira sve metode iz Dao klase kako bi ViewModel mogao da pristupi ovim metodama

    suspend fun upsert(item: ShoppingItem) = db.getShoppingDao().upsert(item)

    suspend fun delete(item: ShoppingItem) = db.getShoppingDao().delete(item)

    fun getAllShoppingItems() = db.getShoppingDao().getAllShoppingItems()

}