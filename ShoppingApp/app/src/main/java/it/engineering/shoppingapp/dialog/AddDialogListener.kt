package it.engineering.shoppingapp.dialog

import it.engineering.shoppingapp.entity.ShoppingItem

interface AddDialogListener {
    fun onAddButtonClicked(item: ShoppingItem)
}