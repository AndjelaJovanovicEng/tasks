package it.engineering.shoppingapp.db

import android.content.Context
import androidx.room.*
import it.engineering.shoppingapp.dao.ShoppingDao
import it.engineering.shoppingapp.entity.ShoppingItem

@Database(
    entities = [ShoppingItem::class], //specificiramo koji sve entity ulaze u nasu bazu
    version = 1 //verzija baze, svaki put kada se menja nesto u bazi mora se promeniti verzije inace Room izbacuje gresku
)
abstract class ShoppingDatabase: RoomDatabase() {

    //dobijamo objekat tipa ShoppingDao prkeo koga pristupamo samoj bazi

    abstract fun getShoppingDao(): ShoppingDao

    companion object {
        //ova instanca postaje vidljiva svim nitima
        //osiguravamo da se uvek radi sa jednom instancom ShoppingDatabase-a
        @Volatile
        private var instance: ShoppingDatabase? = null

        private val LOCK = Any() //sinhronizacija po katancu

        //poziva se svaki put kada se kreira instanca ShoppingDatabase klase
        //osiguravamo da vise niti ne pristupa istoj instanci bazi vec samo jedna u jednom momentu
        //proverava se da li postoji instanca, ukoliko je null onda se poziva synchronized blok, tako da joj pristupa samo jedna nit
        //onda se pravi instanca same baze i dodaljeuje promenljivu instance
        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
                instance ?: createDatabase(context).also { instance = it } //also se poziva uvek nakon metoda bez obzira sta je vrednost
                                                                            //ternarnog operatora, tj. da li je instance null ili ne
            }

        private fun createDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                ShoppingDatabase::class.java, "ShoppingDB.db").build()
    }
}