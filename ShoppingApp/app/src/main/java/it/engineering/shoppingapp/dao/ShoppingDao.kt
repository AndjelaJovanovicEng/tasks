package it.engineering.shoppingapp.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import it.engineering.shoppingapp.entity.ShoppingItem

//Data Acess Object interfejs koji sadrzi sve metode za rad sa bazom
//metod za dodavanje, za brisanje, update i prikaz podataka

@Dao
interface ShoppingDao {
    //posto su ovo operacije koju traju prilicno dugo ne mogu da se izvrasvaju u main thread-u
    //zato se sve one izvrsavaju u pozadini preko korutina (asinhrono izvrsavanje)
    //potrebno je da se doda kljucan rec suspeng

    //mesavina update i insert-a
    //ako item ne postoji u bazi dodace novi, a ako postoji onda ga menja - OnConflicktStrategy.REPLACE
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(item: ShoppingItem)

    //brisanje item-a
    @Delete
    suspend fun delete(item: ShoppingItem)

    @Query("SELECT * FROM shopping_items")
    //u obicnim slucajevima bismo vratili samo listu item-a
    //ali ovde se koristi LiveData kako bi se dinamicki ucitao RecyclerView preko observera
    fun getAllShoppingItems(): LiveData<List<ShoppingItem>>

}