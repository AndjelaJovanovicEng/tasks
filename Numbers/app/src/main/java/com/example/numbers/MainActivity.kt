package com.example.numbers

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun checkNumber(view:View){
        val inputNumber = findViewById<EditText>(R.id.editNumber).text.toString()
        val resultLabel = findViewById<TextView>(R.id.resultLabel)

        val convertedNumber : Int? = inputNumber.toIntOrNull(2)

        if(convertedNumber != null){
            resultLabel.text = convertedNumber.toString()
        } else {
            resultLabel.text = "Number is not in binary format"
        }


    }
}