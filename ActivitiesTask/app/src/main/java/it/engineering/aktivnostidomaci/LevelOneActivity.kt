package it.engineering.aktivnostidomaci

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class LevelOneActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_level_one)

        val startBtn = findViewById<Button>(R.id.mainBtn)
        val castleBtn = findViewById<Button>(R.id.castleBtn)
        val tavernBtn = findViewById<Button>(R.id.tavernBtn)

        startBtn.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }

        castleBtn.setOnClickListener {
            startActivity(Intent(this, LevelTwoActivity::class.java))
        }

        tavernBtn.setOnClickListener {
            startActivity(Intent(this, LevelThreeActivity::class.java))
        }
    }
}