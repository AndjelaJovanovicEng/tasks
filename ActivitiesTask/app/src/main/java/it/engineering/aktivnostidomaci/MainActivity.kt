package it.engineering.aktivnostidomaci

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val forestBtn = findViewById<Button>(R.id.forestBtn)
        val castleBtn = findViewById<Button>(R.id.castleBtn)
        val tavernBtn = findViewById<Button>(R.id.tavernBtn)
        val chooseNextGame = findViewById<Button>(R.id.nextLevel)

        forestBtn.setOnClickListener {
            startActivity(Intent(this, LevelOneActivity::class.java))
        }

        castleBtn.setOnClickListener {
            startActivity(Intent(this, LevelTwoActivity::class.java))
        }

        tavernBtn.setOnClickListener {
            startActivity(Intent(this, LevelThreeActivity::class.java))
        }

        chooseNextGame.setOnClickListener {
            startActivity(Intent(this, NewGameActivity::class.java))
        }

    }
}