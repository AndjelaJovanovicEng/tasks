package it.engineering.jsoninternet

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val jsonURL = "https://evolutiondoo.com/localJSON.json"

        val slikaUrl = "https://rs.n1info.com/wp-content/uploads/2018/10/aurora-236402-725x408.jpeg"

        preuzmiJSON().execute(jsonURL)
        preuzmiSliku(findViewById(R.id.prikazSlika)).execute(slikaUrl)

    }

    //ugnjezdene klase - vise funkcionalnosti koje rade zajedno

    inner  class preuzmiJSON: AsyncTask<String, String, String>(){
        override fun doInBackground(vararg params: String?): String {
            var json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                        reader -> reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }

            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            prikaziJSON(result)
        }

    }

    private fun prikaziJSON(json:String?){
        val prikaz = findViewById<TextView>(R.id.prikaz)
        if(json == null){
            prikaz.text = "Nothing to show"
        } else {
            prikaz.text = json
        }

    }

    inner class preuzmiSliku(val okvirZaSliku:ImageView): AsyncTask<String, Void, Bitmap?>(){
        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val urlZaPreuzimanje = params[0]

            try {
                val slikaSaInterneta = URL(urlZaPreuzimanje).openStream()
                image = BitmapFactory.decodeStream(slikaSaInterneta)
            } catch (e:Exception){
                e.printStackTrace()
            }

            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            okvirZaSliku.setImageBitmap(result)
        }

    }
}