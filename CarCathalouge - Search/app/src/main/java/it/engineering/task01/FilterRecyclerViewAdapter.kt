package it.engineering.task01


import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.LinearLayout

import android.widget.TextView
import androidx.core.content.ContextCompat

import androidx.recyclerview.widget.RecyclerView

import it.engineering.task01.models.CompanyModelEntity
import java.util.*
import kotlin.collections.ArrayList

class FilterRecyclerViewAdapter(private val carCompanyFilterData: ArrayList<CompanyModelEntity>) : RecyclerView.Adapter<CustomFilteredViewHolder>(), Filterable {

    private var filteredCompanyModelEntities : ArrayList<CompanyModelEntity> = arrayListOf()

    init {
        filteredCompanyModelEntities = carCompanyFilterData
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomFilteredViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val recycleViewRow = layoutInflater.inflate(R.layout.recycler_view_filter, parent, false)
        return CustomFilteredViewHolder(recycleViewRow)

    }

    override fun onBindViewHolder(holder: CustomFilteredViewHolder, position: Int) {
        val companyTitle = holder.view.findViewById<TextView>(R.id.companyModelTitle)
        val modelTitle = holder.view.findViewById<TextView>(R.id.modelTitle)
        val modelPrice = holder.view.findViewById<TextView>(R.id.modelPrice)


        companyTitle.text = filteredCompanyModelEntities[position].companyName
        modelTitle.text = filteredCompanyModelEntities[position].modelName
        modelPrice.text = filteredCompanyModelEntities[position].price


    }

    override fun getItemCount(): Int {
        return filteredCompanyModelEntities.count()
    }

    override fun getFilter(): Filter {
        return object: Filter(){
            override fun performFiltering(constraint: CharSequence?): FilterResults {

                val inputSearch = constraint.toString()

                if(inputSearch.isEmpty()){
                    filteredCompanyModelEntities = carCompanyFilterData
                } else {
                    val resultArray = ArrayList<CompanyModelEntity>()
                    for(row in carCompanyFilterData){
                        if(row.companyName.toLowerCase(Locale.ROOT).contains(inputSearch.toLowerCase(Locale.ROOT))){
                            resultArray.add(row)
                        }
                    }

                    filteredCompanyModelEntities = resultArray
                }

                val filteredResults = FilterResults()
                filteredResults.values = filteredCompanyModelEntities
                return filteredResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filteredCompanyModelEntities = results?.values as ArrayList<CompanyModelEntity>
                notifyDataSetChanged()
            }
        }
    }

}

class CustomFilteredViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}