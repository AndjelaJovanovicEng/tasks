package it.engineering.task01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import androidx.appcompat.widget.SearchView

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.engineering.task01.models.CompanyEntity
import java.io.InputStreamReader
import kotlin.collections.ArrayList


class AllModelsAcitivty : AppCompatActivity() {

    private val carCompanyDataArray : ArrayList<CompanyEntity> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_models_acitivty)

        DataGlobal.dataUtilityClass.createDataSource(this, carCompanyDataArray)

        val otvoriCSV = InputStreamReader(assets.open("modelExcel.csv"))
        DataGlobal.dataUtilityClass.loadCompanyData(this, otvoriCSV)

        val adapter = RecyclerViewAdapter(carCompanyDataArray)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewModels)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)

        val carSearch = findViewById<SearchView>(R.id.carSearch)

        carSearch.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return false
            }

        })


    }


}
