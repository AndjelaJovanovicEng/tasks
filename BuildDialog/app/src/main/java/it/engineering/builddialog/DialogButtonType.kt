package it.engineering.builddialog

enum class DialogButtonType {

    PRIMARY,
    SECONDARY

}