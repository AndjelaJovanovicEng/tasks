package it.engineering.builddialog

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    var dialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dialog = Dialog.build {
            title = "Ovo je naslov"
            message = "Ovo je poruka"
            setNegativeButton("Ostani", View.OnClickListener { Log.d("Button", "Desio se klik nad dugmetom") }, DialogButtonType.PRIMARY)
            setPositiveButton("Izadji", View.OnClickListener {
                dialog?.dismiss()
                Handler(Looper.getMainLooper()).postDelayed({
                    finish()
                }, 1.toLong())
            }, DialogButtonType.SECONDARY)
        }


        findViewById<TextView>(R.id.textDialogShow).setOnClickListener {
            dialog?.show(supportFragmentManager, "TAG")
        }
    }
}