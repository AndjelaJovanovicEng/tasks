package it.engineering.builddialog

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import kotlin.reflect.typeOf

class Dialog(private val title: String, private val message: String,
             private val positiveButtonTriple: Triple<String, View.OnClickListener, DialogButtonType?>,
             private val negativeButtonTriple: Triple<String, View.OnClickListener, DialogButtonType?>) : DialogFragment() {

    class Builder{
        var title: String? = null
        var message: String? = null
        var  positiveButtonTriple: Triple<String, View.OnClickListener, DialogButtonType?>? = null
            private set

        var  negativeButtonTriple: Triple<String, View.OnClickListener, DialogButtonType?>? = null
            private set

        fun setNegativeButton(text: String, listener: View.OnClickListener, buttonType: DialogButtonType = DialogButtonType.PRIMARY){
            negativeButtonTriple = Triple(text, listener, buttonType)
        }

        fun setPositiveButton(text: String, listener: View.OnClickListener, buttonType: DialogButtonType = DialogButtonType.SECONDARY){
            positiveButtonTriple = Triple(text, listener, buttonType)
        }

        fun build() = Dialog(this)

    }

    private constructor(builder: Builder): this(
        builder.title!!,
        builder.message!!,
        builder.positiveButtonTriple!!,
        builder.negativeButtonTriple!!
    )

    companion object {
        inline fun build(block: Builder.() -> Unit) = Builder().apply(block).build()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.dialog_layout, container, false)

    override fun onStart() {
        super.onStart()

        dialog?.let { dialog ->
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT

            dialog.window?.let {
                it.apply {
                    setLayout(width, height)
                    setBackgroundDrawableResource(R.drawable.dialog_background)
                }
            }

        }

    }

    private fun Button.setTypeStyle(type: DialogButtonType){
        when(type){
            DialogButtonType.PRIMARY -> {
                this.apply {
                    setBackgroundColor(Color.parseColor("#990000"))
                    setTextColor(ContextCompat.getColor(context, R.color.white))
                }
            }

            DialogButtonType.SECONDARY -> {
                this.apply {
                    setBackgroundColor(Color.parseColor("#c1c1c1"))
                    setTextColor(ContextCompat.getColor(context, R.color.teal_200))
                }
            }
        }
    }

    private fun Button.setupAsDialogButton(buttonText: String?, clickListener: View.OnClickListener?){
        if(buttonText != null && clickListener != null){
            this.apply {
                text = buttonText
                visibility = View.VISIBLE
                setOnClickListener {
                    clickListener.onClick(this)
                    dismiss()
                }
            }
        } else {
            Log.d("BUTTON", "Greska prilikom klika")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<TextView>(R.id.titleDialog).text = "Title"
        view.findViewById<TextView>(R.id.messageDialog).text = "Message"

        positiveButtonTriple.let {
            view.findViewById<Button>(R.id.positiveButton)
                .setupAsDialogButton(positiveButtonTriple.first, positiveButtonTriple.second)
            view.findViewById<Button>(R.id.positiveButton).setTypeStyle(positiveButtonTriple.third!!)
        }

        negativeButtonTriple.let {
            view.findViewById<Button>(R.id.negativeButton)
                .setupAsDialogButton(negativeButtonTriple.first, negativeButtonTriple.second)
            view.findViewById<Button>(R.id.negativeButton).setTypeStyle(positiveButtonTriple.third!!)
        }
    }
}