package it.engineering.webclient

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

val URL = "https://www.audi.it"

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<TextView>(R.id.tochrome).setOnClickListener {
            val uri = Uri.parse("googlechrome://navigate?url="+ URL)
            val intent = Intent(Intent.ACTION_VIEW, uri)
           // intent.data =  Uri.parse("googlechrome://navigate?url="+ URL)
            startActivity(intent)
        }

        findViewById<TextView>(R.id.toWebView).setOnClickListener {
            startActivity(Intent(this, WebActivity::class.java))
        }
    }
}