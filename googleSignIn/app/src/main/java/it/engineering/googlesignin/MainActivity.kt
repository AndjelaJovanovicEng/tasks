package it.engineering.googlesignin

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import org.w3c.dom.Text




class MainActivity : AppCompatActivity() {

    private val RC_SIGN_IN = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val signInBtn = findViewById<SignInButton>(R.id.sign_in_button)
        val userText = findViewById<TextView>(R.id.userSignedIn)
        val signOutBtn = findViewById<Button>(R.id.signOutBtn)

        signInBtn.setSize(SignInButton.SIZE_STANDARD)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()

        val mGoogleSignInClient = GoogleSignIn.getClient(this, gso)




        signInBtn.visibility = View.VISIBLE
        userText.visibility = View.GONE


        signInBtn.setOnClickListener {
            val signInIntent = mGoogleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }

        signOutBtn.setOnClickListener {
            mGoogleSignInClient.signOut()
                    .addOnCompleteListener(this) {
                        signInBtn.visibility = View.VISIBLE
                        userText.visibility = View.GONE
                    }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        val signInBtn = findViewById<SignInButton>(R.id.sign_in_button)
        val userText = findViewById<TextView>(R.id.userSignedIn)

        try {
            val account = completedTask.getResult(ApiException::class.java)!!

            // Signed in successfully, show authenticated UI.
            signInBtn.visibility = View.GONE
            userText.visibility = View.VISIBLE
            userText.text = account.displayName

        } catch (e: ApiException) {

            signInBtn.visibility = View.VISIBLE
            userText.visibility = View.GONE

            Log.e("TAG","signInResult:failed code=" + e.statusCode)
        }
    }
}