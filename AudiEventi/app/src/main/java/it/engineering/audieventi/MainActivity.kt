package it.engineering.audieventi

import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import it.engineering.audieventi.global.DataGlobal
import it.engineering.audieventi.utility.TestData
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val jsonUrl = DataGlobal.dataUtility.createUrlLink(DataGlobal.baseUrl, DataGlobal.langsUrl)

        downloadJson().execute(jsonUrl)


    }

    inner  class downloadJson: AsyncTask<String, String, String>(){

        override fun doInBackground(vararg params: String?): String {
            var json: String
            val connection = URL(params[0]).openConnection() as HttpURLConnection

            try {
                connection.connect()
                json = connection.inputStream.use {
                    it.reader().use {
                        reader -> reader.readText()
                    }
                }
            } finally {
                connection.disconnect()
            }

            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            DataGlobal.dataUtility.parseJSONLangs(JSONObject(result))
            TestData().populatePublicEventsData(this@MainActivity)
            TestData().populatePremiumEventsData(this@MainActivity)
            TestData().populateFoodExperienceData(this@MainActivity)
            TestData().populateAudiDrivingExperienceData(this@MainActivity)
            TestData().populatePlacesAndTerritoryData(this@MainActivity)
            startActivity(Intent(this@MainActivity, HomeGuestActivity::class.java))

        }

    }



}