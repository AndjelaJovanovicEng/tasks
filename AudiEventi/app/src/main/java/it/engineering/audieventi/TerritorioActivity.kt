package it.engineering.audieventi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import it.engineering.audieventi.global.DataGlobal

class TerritorioActivity : AppCompatActivity() {

    lateinit var toggle: ActionBarDrawerToggle


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_territorio)

        val navDrawerLayout = findViewById<DrawerLayout>(R.id.drawerLayoutTerritory)
        val navigationView = findViewById<NavigationView>(R.id.navigationViewTerritory)


        val territoryBannerImage = findViewById<ImageView>(R.id.territoryBannerImage)
        val territoryTitle = findViewById<TextView>(R.id.territoryTitle)
        val territorySubtitle = findViewById<TextView>(R.id.territorySubtitle)
        val territoryText = findViewById<TextView>(R.id.territoryText)

        toggle = ActionBarDrawerToggle(this, navDrawerLayout, R.string.open, R.string.close)
        navDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        findViewById<ImageView>(R.id.btnHamburgerMenuTerritory).setOnClickListener {
            navDrawerLayout.openDrawer(navigationView)
        }

        navigationView.setNavigationItemSelectedListener {

            when(it.itemId){
                R.id.home -> navDrawerLayout.closeDrawer(navigationView)
                R.id.programma -> startActivity(Intent(this, IlTuoEventoActivity::class.java))
                R.id.food_exp -> startActivity(Intent(this, FoodExperienceActivity::class.java))
                R.id.territory -> startActivity(Intent(this, TerritorioActivity::class.java))
                R.id.audi_exp -> startActivity(Intent(this, AudiDriveExperienceActivity::class.java))
                R.id.sondaggio -> {
                    Toast.makeText(this, "Not yet implemented", Toast.LENGTH_SHORT).show()
                    navDrawerLayout.closeDrawer(navigationView)
                }
                R.id.calendar -> startActivity(Intent(this, CalendarioEventiActivity::class.java))

                R.id.coupon -> {
                    navDrawerLayout.closeDrawer(navigationView)
                    CustomDialogEntity().show(supportFragmentManager, "MyCustomFragment")
                }

            }

            true
        }

        DataGlobal.dataUtility.DownloadImage(territoryBannerImage).execute(DataGlobal.placesList[0].imagePlace.href)
        territoryTitle.text = DataGlobal.placesList[0].title
        territorySubtitle.text = DataGlobal.placesList[0].placeSubtitle
        territoryText.text = DataGlobal.placesList[0].description
    }
}