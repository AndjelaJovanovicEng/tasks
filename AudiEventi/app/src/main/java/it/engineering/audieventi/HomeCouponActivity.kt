package it.engineering.audieventi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import it.engineering.audieventi.global.DataGlobal

class HomeCouponActivity : AppCompatActivity() {

    lateinit var toggle: ActionBarDrawerToggle


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_coupon)

        val reconnectToNatureImage = findViewById<ImageView>(R.id.reconnect_to_nature_image)
        val reconnectToNatureTitle = findViewById<TextView>(R.id.reconnect_to_nature_title)
        val reconnectToNatureSubtitle = findViewById<TextView>(R.id.reconnect_to_nature_subtitle)

        val foodExperienceImage = findViewById<ImageView>(R.id.food_experience_image)
        val foodExperienceTitle = findViewById<TextView>(R.id.food_experience_title)
        val foodExperienceSubtitle = findViewById<TextView>(R.id.food_experience_subtitle)

        val luoghiETerritorioImage = findViewById<ImageView>(R.id.luoghi_e_territorio_image)
        val luoghiETerritorioTitle = findViewById<TextView>(R.id.luoghi_e_territorio_title)
        val luoghiETerritorioSubtitle = findViewById<TextView>(R.id.luoghi_e_territorio_subtitle)

        val audiDrivingExperienceImage = findViewById<ImageView>(R.id.audi_driving_experience_image)
        val audiDrivingExperienceTitle = findViewById<TextView>(R.id.audi_driving_experience_title)
        val audiDrivingExperienceSubtitle = findViewById<TextView>(R.id.audi_driving_experience_subtitle)

        val infoAndContattiImage = findViewById<ImageView>(R.id.info_and_contatti_image)
        val infoAndContattiTitle = findViewById<TextView>(R.id.info_and_contatti_title)

        val sondaggioImage = findViewById<ImageView>(R.id.sondaggio_image)
        val sondaggioTitle = findViewById<TextView>(R.id.sondaggio_title)
        val sondaggioSubtitle = findViewById<TextView>(R.id.sondaggio_subtitle)

        val eventiAudiImage = findViewById<ImageView>(R.id.eventi_audi_image)
        val eventiAudiTitle = findViewById<TextView>(R.id.eventi_audi_title)
        val eventiAudiSubtitle = findViewById<TextView>(R.id.eventi_audi_subtitle)

        val ilTuoEventoImage = findViewById<ImageView>(R.id.il_tuo_evento_image)
        val ilTuoEventoTitle = findViewById<TextView>(R.id.il_tuo_evento_title)
        val ilTuoEventoSubtitle = findViewById<TextView>(R.id.il_tuo_evento_subtitle)

        val gammaAudiImage = findViewById<ImageView>(R.id.gamma_audi_image)
        val gammaAudiTitle = findViewById<TextView>(R.id.gamma_audi_title)
        val gammaAudiSubtitle = findViewById<TextView>(R.id.gamma_audi_subtitle)


        DataGlobal.dataUtility.populateView(reconnectToNatureTitle, DataGlobal.RECCONECT_TO_NATURE)
        DataGlobal.dataUtility.populateView(reconnectToNatureSubtitle, DataGlobal.RECCONECT_TO_NATURE_SUBTITLE)
        DataGlobal.dataUtility.DownloadImage(reconnectToNatureImage).execute(DataGlobal.premiumEvents[0].image.get("href") as String)


        DataGlobal.dataUtility.populateView(foodExperienceTitle, DataGlobal.langs.get("SIDE_MENU_PREMIUM_FOOD_EXPERIENCE"))
        DataGlobal.dataUtility.populateView(foodExperienceSubtitle, DataGlobal.FOOD_EXP_SUBTITLE)
        DataGlobal.dataUtility.DownloadImage(foodExperienceImage).execute(DataGlobal.foodExperienceList[0].image_food.get("href") as String)

        DataGlobal.dataUtility.populateView(luoghiETerritorioTitle, DataGlobal.langs.get("HOME_PLACES_TITLE"))
        DataGlobal.dataUtility.populateView(luoghiETerritorioSubtitle, DataGlobal.langs.get("HOME_PLACES_SUBTITLE"))
        DataGlobal.dataUtility.DownloadImage(luoghiETerritorioImage).execute(DataGlobal.placesList[0].imagePlace.href)

        DataGlobal.dataUtility.populateView(audiDrivingExperienceTitle, DataGlobal.langs.get("HOME_ADE_SUBTITLE"))
        DataGlobal.dataUtility.populateView(audiDrivingExperienceSubtitle, DataGlobal.AUDI_DRIVE_EXP)
        DataGlobal.dataUtility.DownloadImage(audiDrivingExperienceImage).execute(DataGlobal.audiDriveExperienceList[0].imageAde.href)

        DataGlobal.dataUtility.populateView(infoAndContattiTitle, DataGlobal.INFO_I_CONTATI)
        DataGlobal.dataUtility.DownloadImage(infoAndContattiImage).execute(DataGlobal.INFO_I_CONTATI_IMG_URL)

        DataGlobal.dataUtility.populateView(sondaggioTitle, DataGlobal.langs.get("SURVEY_APPBAR_TITLE"))
        DataGlobal.dataUtility.populateView(sondaggioSubtitle, DataGlobal.langs.get("SURVEY_BOTTOM_HEADER"))
        DataGlobal.dataUtility.DownloadImage(sondaggioImage).execute(DataGlobal.SONDAGIO_IMAGE_URL)

        DataGlobal.dataUtility.populateView(sondaggioTitle, DataGlobal.langs.get("SURVEY_APPBAR_TITLE"))
        DataGlobal.dataUtility.populateView(sondaggioSubtitle, DataGlobal.langs.get("SURVEY_BOTTOM_HEADER"))

        DataGlobal.dataUtility.populateView(eventiAudiTitle, DataGlobal.langs.get("HOME_GUEST_EVENT_LIST"))
        DataGlobal.dataUtility.populateView(eventiAudiSubtitle,  DataGlobal.langs.get("HOME_GUEST_EVENT_LIST_BUTTON"))
        eventiAudiImage.setImageResource(R.drawable.eventi_home_guest)

        DataGlobal.dataUtility.populateView(gammaAudiTitle,  DataGlobal.langs.get("HOME_GUEST_AUDI_RANGE"))
        DataGlobal.dataUtility.populateView(gammaAudiSubtitle, DataGlobal.langs.get("HOME_GUEST_AUDI_RANGE_BUTTON"))
        ilTuoEventoImage.setImageResource(R.drawable.evento_coupon_home_guest)

        DataGlobal.dataUtility.populateView(ilTuoEventoTitle,  DataGlobal.langs.get("HOME_GUEST_COUPON_LOGIN"))
        DataGlobal.dataUtility.populateView(ilTuoEventoSubtitle, DataGlobal.INSERISCI_COUPON)
        gammaAudiImage.setImageResource(R.drawable.gamma_audi_home)


        val navDrawerLayout = findViewById<DrawerLayout>(R.id.drawerLayoutPremium)
        val navigationView = findViewById<NavigationView>(R.id.navigationViewPremium)

        toggle = ActionBarDrawerToggle(this, navDrawerLayout, R.string.open, R.string.close)
        navDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        findViewById<ImageView>(R.id.btnHamburgerMenuPremium).setOnClickListener {
            navDrawerLayout.openDrawer(navigationView)
        }

        navigationView.setNavigationItemSelectedListener {

            when(it.itemId){
                R.id.home -> navDrawerLayout.closeDrawer(navigationView)
                R.id.programma -> startActivity(Intent(this, IlTuoEventoActivity::class.java))
                R.id.food_exp -> startActivity(Intent(this, FoodExperienceActivity::class.java))
                R.id.territory -> startActivity(Intent(this, TerritorioActivity::class.java))
                R.id.audi_exp -> startActivity(Intent(this, AudiDriveExperienceActivity::class.java))
                R.id.sondaggio -> {
                    Toast.makeText(this, "Not yet implemented", Toast.LENGTH_SHORT).show()
                    navDrawerLayout.closeDrawer(navigationView)
                }
                R.id.calendar -> startActivity(Intent(this, CalendarioEventiActivity::class.java))

                R.id.coupon -> {
                    navDrawerLayout.closeDrawer(navigationView)
                    CustomDialogEntity().show(supportFragmentManager, "MyCustomFragment")
                }

            }

            true
        }

        findViewById<ImageView>(R.id.impostazioniPremium).setOnClickListener {
            startActivity(Intent(this, SettingsActivity::class.java))
        }

        reconnectToNatureTitle.setOnClickListener {
            startActivity(Intent(this, IlTuoEventoActivity::class.java))
        }

        foodExperienceTitle.setOnClickListener {
            startActivity(Intent(this, FoodExperienceActivity::class.java))
        }

        luoghiETerritorioTitle.setOnClickListener {
            startActivity(Intent(this, TerritorioActivity::class.java))
        }

        audiDrivingExperienceTitle.setOnClickListener {
            startActivity(Intent(this, AudiDriveExperienceActivity::class.java))
        }

        eventiAudiTitle.setOnClickListener {
            startActivity(Intent(this, CalendarioEventiActivity::class.java))
        }

        ilTuoEventoTitle.setOnClickListener {
            CustomDialogEntity().show(supportFragmentManager, "MyCustomFragment")
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)){
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}