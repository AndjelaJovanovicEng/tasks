package it.engineering.audieventi.entity

// premium event - link_my_audi_premium
data class LinkEntity(
    val uri: String,
    val title: String,
    val options: Any
)
