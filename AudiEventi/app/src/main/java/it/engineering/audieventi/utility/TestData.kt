package it.engineering.audieventi.utility

import android.content.Context
import android.util.Log
import it.engineering.audieventi.entity.*
import it.engineering.audieventi.global.DataGlobal
import org.json.JSONArray
import java.io.IOException
import java.io.InputStream

class TestData {

    fun populatePublicEventsData(context: Context) {
        val json: String?

        try {
            val InputStream: InputStream = context.assets.open("events_public.json")
            json = InputStream.bufferedReader().use {
                it.readText()
            }

            val jsonArray = JSONArray(json)
            jsonArray.let {
                (0 until it.length()).forEach {
                    val jsonObject = jsonArray.getJSONObject(it)

                    val id = jsonObject.getString("id")
                    val title = jsonObject.getString("titolo")

                    val description = jsonObject.getJSONObject("descrizione")
                    val format = description.getString("format")
                    val value = description.getString("value")

                    val eventDate = jsonObject.getString("data_evento")

                    val linkMyAudi = jsonObject.getJSONObject("link_myaudi")
                    val uri = linkMyAudi.getString("uri")
                    val linkMyAudiTitle = linkMyAudi.getString("title")
                    val options = linkMyAudi.getJSONArray("options")

                    val priority = jsonObject.getInt("priorita")
                    val status = jsonObject.getString("stato")

                    val backgroundImage = jsonObject.getJSONObject("immagine_background")
                    val backgroundImageId = backgroundImage.getString("id")
                    val href = backgroundImage.getString("href")
                    val meta = backgroundImage.getJSONObject("meta")
                    val alt = meta.getString("alt")
                    val metaTitle = meta.getString("title")
                    val width = meta.getString("width")
                    val height = meta.getString("height")


                    val descriptionHashMap: HashMap<String, String> = hashMapOf()

                    descriptionHashMap.put("format", format)
                    descriptionHashMap.put("value", value)

                    val linkMyAudiHashMap: HashMap<String, Any> = hashMapOf()

                    linkMyAudiHashMap.put("uri", uri)
                    linkMyAudiHashMap.put("title", linkMyAudiTitle)
                    linkMyAudiHashMap.put("options", options)

                    val backgroundImageHashMap: HashMap<String, Any> = hashMapOf()

                    backgroundImageHashMap.put("id", backgroundImageId)
                    backgroundImageHashMap.put("href", href)
                    backgroundImageHashMap.put("meta", MetaDataEntity(alt, metaTitle, width, height))


                    DataGlobal.publicEvents.add(EventEntityPublic(id, title, descriptionHashMap, eventDate, linkMyAudiHashMap, priority, status, backgroundImageHashMap))
                }
            }
        } catch (e: IOException){
            Log.d("JSON Exception", e.toString())
        }

        for (e in DataGlobal.publicEvents) {
            Log.d("PUBLICEVENT", e.toString())
        }

    }


    //premium events data

    fun populatePremiumEventsData(context: Context) {
        val json: String?

        try {
            val InputStream: InputStream = context.assets.open("events_premium.json")
            json = InputStream.bufferedReader().use {
                it.readText()
            }

            val jsonArray = JSONArray(json)
            jsonArray.let {
                (0 until it.length()).forEach {
                    val jsonObject = jsonArray.getJSONObject(it)

                    val id = jsonObject.getString("id")

                    val title = jsonObject.getString("title")

                    val description = jsonObject.getJSONObject("descrizione")
                    val value = description.getString("value")
                    val format = description.getString("format")
                    val processed = description.getString("processed")

                    val headerPremium = jsonObject.getString("header_premium")

                    val linkMyAudiPremium = jsonObject.getJSONArray("link_myaudi_premium")
                    val linkMyAudiPremiumArray: ArrayList<LinkEntity> = arrayListOf()
                    linkMyAudiPremium.let {
                        (0 until it.length()).forEach {
                            val linkMyAudiJsonObject = linkMyAudiPremium.getJSONObject(it)

                            val uri = linkMyAudiJsonObject.getString("uri")
                            val linkMyAudiTitle = linkMyAudiJsonObject.getString("title")
                            val options = linkMyAudiJsonObject.getJSONArray("options")

                            linkMyAudiPremiumArray.add(
                                    LinkEntity(
                                            uri,
                                            linkMyAudiTitle,
                                            options
                                    )
                            )
                        }
                    }

                    val programmeNote = jsonObject.getJSONObject("note_programma")
                    val programmeNoteValue = programmeNote.getString("value")
                    val programmeNoteFormat = programmeNote.getString("format")
                    val programmeNotepPocessed = programmeNote.getString("processed")

                    val detailedPlan = jsonObject.getJSONArray("programma_dettagliato")
                    val detailedPlanArray: ArrayList<HashMap<String, Any>> = arrayListOf()
                    detailedPlan.let {
                        (0 until it.length()).forEach {
                            val detailedPlanJsonObject = detailedPlan.getJSONObject(it)

                            val day = detailedPlanJsonObject.getString("giorno")
                            val activities = detailedPlanJsonObject.getJSONArray("activities")
                            val activitiesArray: ArrayList<HashMap<String, String>> = arrayListOf()

                            activities.let {
                                (0 until it.length()).forEach {
                                    val activityJsonObject = activities.getJSONObject(it)

                                    val start = activityJsonObject.getString("inizio")
                                    val end = activityJsonObject.getString("fine")
                                    val activity = activityJsonObject.getString("activity")

                                    val activityHashMap: HashMap<String, String> = hashMapOf()
                                    activityHashMap.put("inizio", start)
                                    activityHashMap.put("fine", end)
                                    activityHashMap.put("activity", activity)

                                    activitiesArray.add(activityHashMap)
                                }
                            }

                            val detailedPlanHashMap: HashMap<String, Any> = hashMapOf()
                            detailedPlanHashMap.put("giorno", day)
                            detailedPlanHashMap.put("activities", activitiesArray)

                            detailedPlanArray.add(detailedPlanHashMap)
                        }
                    }

                    val subtitle = jsonObject.getString("sottotitolo")

                    val image = jsonObject.getJSONObject("immagine")
                    val imageId = image.getString("id")
                    val href = image.getString("href")
                    val meta = image.getJSONObject("meta")
                    val alt = meta.getString("alt")
                    val metaTitle = meta.getString("title")
                    val width = meta.getString("width")
                    val height = meta.getString("height")

                    val descriptionHashMap: HashMap<String, String> = hashMapOf()
                    descriptionHashMap.put("value", value)
                    descriptionHashMap.put("format", format)
                    descriptionHashMap.put("processed", processed)

                    val programmeNoteHashMap: HashMap<String, String> = hashMapOf()
                    programmeNoteHashMap.put("value", programmeNoteValue)
                    programmeNoteHashMap.put("format", programmeNoteFormat)
                    programmeNoteHashMap.put("processed", programmeNotepPocessed)

                    val imageHashMap: HashMap<String, Any> = hashMapOf()
                    imageHashMap.put("id", imageId)
                    imageHashMap.put("href", href)
                    imageHashMap.put("meta", MetaDataEntity(alt, metaTitle, width, height))

                    DataGlobal.premiumEvents.add(EventEntityPremium(id, title, descriptionHashMap, headerPremium, linkMyAudiPremiumArray, programmeNoteHashMap, detailedPlanArray, subtitle, imageHashMap))
                }
            }
        } catch (e: IOException){
            Log.d("JSON Exception", e.toString())
        }

        for (e in DataGlobal.premiumEvents) {
            Log.d("PREMIUMEVENT", e.toString())
        }
    }

    //food experience

    fun populateFoodExperienceData(context: Context) {
        val json: String?

        try {
            val InputStream: InputStream = context.assets.open("food_experience.json")
            json = InputStream.bufferedReader().use {
                it.readText()
            }

            val jsonArray = JSONArray(json)
            jsonArray.let {
                (0 until it.length()).forEach {
                    val jsonObject = jsonArray.getJSONObject(it)

                    val id = jsonObject.getString("id")

                    val title = jsonObject.getString("title")

                    val header = jsonObject.getString("header")

                    val subtitleFood = jsonObject.getJSONObject("sottotitolo_food")
                    val value = subtitleFood.getString("value")
                    val format = subtitleFood.getString("format")
                    val processed = subtitleFood.getString("processed")

                    val foodImage = jsonObject.getJSONObject("immagine_food")
                    val foodImageId = foodImage.getString("id")
                    val href = foodImage.getString("href")
                    val meta = foodImage.getJSONObject("meta")
                    val alt = meta.getString("alt")
                    val metaTitle = meta.getString("title")
                    val width = meta.getString("width")
                    val height = meta.getString("height")

                    val programmeExperience = jsonObject.getJSONArray("programma_experience")
                    val programmeExperienceArray: ArrayList<FoodExperienceDay> = arrayListOf()
                    programmeExperience.let {
                        (0 until it.length()).forEach {
                            val programmeExperienceJsonObject = programmeExperience.getJSONObject(it)

                            val day = programmeExperienceJsonObject.getString("giorno")
                            val experience = programmeExperienceJsonObject.getJSONObject("experience")
                            val startTime = experience.getString("inizio")
                            val type = experience.getString("tipo")
                            val activity = experience.getString("attivita")
                            val place = experience.getString("sito")
                            val description = experience.getString("descrizione")
                            val food = experience.getString("vivande")
                            val allergens = experience.getString("allergeni")

                            programmeExperienceArray.add(FoodExperienceDay(day, ExperienceEntity(startTime, type, activity, place, description, food, allergens)))
                        }
                    }

                    val subtitleFoodHashMap: HashMap<String, String> = hashMapOf()
                    subtitleFoodHashMap.put("value", value)
                    subtitleFoodHashMap.put("format", format)
                    subtitleFoodHashMap.put("processed", processed)

                    val foodImageHashMap: HashMap<String, Any> = hashMapOf()
                    foodImageHashMap.put("id", foodImageId)
                    foodImageHashMap.put("href", href)
                    foodImageHashMap.put("meta", MetaDataEntity(alt, metaTitle, width, height))

                    DataGlobal.foodExperienceList.add(FoodExperienceEntity(id, title, header, subtitleFoodHashMap, foodImageHashMap, programmeExperienceArray))
                }
            }
        } catch (e: IOException){
            Log.d("JSON Exception", e.toString())
        }

        for (e in DataGlobal.foodExperienceList) {
            Log.d("FOODEXPERIENCE", e.toString())
        }
    }

    //audi experience

    fun populateAudiDrivingExperienceData(context: Context) {
        val json: String?

        try {
            val InputStream: InputStream = context.assets.open("audi_driving_exp.json")
            json = InputStream.bufferedReader().use {
                it.readText()
            }

            val jsonArray = JSONArray(json)
            jsonArray.let {
                (0 until it.length()).forEach {
                    val jsonObject = jsonArray.getJSONObject(it)

                    val data = jsonObject.getJSONObject("data")

                    val id = jsonObject.getString("id")

                    val freeText = jsonObject.getString("testo_libero")

                    val subtitleAde = jsonObject.getString("sottotitolo_ade")

                    val titleAde = jsonObject.getString("titolo_ade")

                    val carouselAde = jsonObject.getJSONArray("carosello_ade")
                    val carouselAdeArray: ArrayList<BackgroundImage> = arrayListOf()
                    carouselAde.let {
                        (0 until it.length()).forEach {
                            val carouselAdeJsonObject = carouselAde.getJSONObject(it)

                            val id = carouselAdeJsonObject.getString("id")
                            val href = carouselAdeJsonObject.getString("href")
                            val meta = carouselAdeJsonObject.getJSONObject("meta")
                            val alt = meta.getString("alt")
                            val metaTitle = meta.getString("title")
                            val width = meta.getString("width")
                            val height = meta.getString("height")

                            carouselAdeArray.add(BackgroundImage(id, href, MetaDataEntity(alt, metaTitle, width, height)))
                        }
                    }

                    val imageAde = jsonObject.getJSONObject("immagine_ade")
                    val imageAdeId = imageAde.getString("id")
                    val href = imageAde.getString("href")
                    val meta = imageAde.getJSONObject("meta")
                    val alt = meta.getString("alt")
                    val metaTitle = meta.getString("title")
                    val width = meta.getString("width")
                    val height = meta.getString("height")

                    DataGlobal.audiDriveExperienceList.add(AudiDriveExperienceEntity(data, id, freeText, subtitleAde, titleAde, carouselAdeArray, BackgroundImage(imageAdeId, href, MetaDataEntity(alt, metaTitle, width, height))))
                }
            }
        } catch (e: IOException){
            Log.d("JSON Exception", e.toString())
        }

        for (e in DataGlobal.audiDriveExperienceList) {
            Log.d("ADE", e.toString())
        }
    }

    // places and terittories

    fun populatePlacesAndTerritoryData(context: Context) {
        val json: String?

        try {
            val InputStream: InputStream = context.assets.open("places.json")
            json = InputStream.bufferedReader().use {
                it.readText()
            }

            val jsonArray = JSONArray(json)
            jsonArray.let {
                (0 until it.length()).forEach {
                    val jsonObject = jsonArray.getJSONObject(it)

                    val id = jsonObject.getString("id")

                    val title = jsonObject.getString("title")

                    val description = jsonObject.getString("descrizione")

                    val placeSubtitle = jsonObject.getString("sottotitolo_luoghi")

                    val placeTitle = jsonObject.getString("titolo_luoghi")

                    val carouselPlace = jsonObject.getJSONArray("carosello_luoghi")
                    val carouselPlaceArray: ArrayList<BackgroundImage> = arrayListOf()
                    carouselPlace.let {
                        (0 until it.length()).forEach {
                            val carouselPlaceJsonObject = carouselPlace.getJSONObject(it)

                            val id = carouselPlaceJsonObject.getString("id")
                            val href = carouselPlaceJsonObject.getString("href")
                            val meta = carouselPlaceJsonObject.getJSONObject("meta")
                            val alt = meta.getString("alt")
                            val metaTitle = meta.getString("title")
                            val width = meta.getString("width")
                            val height = meta.getString("height")

                            carouselPlaceArray.add(BackgroundImage(id, href, MetaDataEntity(alt, metaTitle, width, height)))
                        }
                    }

                    val imagePlace = jsonObject.getJSONObject("immagine_luoghi")
                    val imagePlaceId = imagePlace.getString("id")
                    val href = imagePlace.getString("href")
                    val meta = imagePlace.getJSONObject("meta")
                    val alt = meta.getString("alt")
                    val metaTitle = meta.getString("title")
                    val width = meta.getString("width")
                    val height = meta.getString("height")

                    DataGlobal.placesList.add(PlacesAndTerritories(id, title, description, placeSubtitle, placeTitle, carouselPlaceArray, BackgroundImage(imagePlaceId, href, MetaDataEntity(alt, title, width, height))))
                }
            }
        } catch (e: IOException){
            Log.d("JSON Exception", e.toString())
        }

        for (e in DataGlobal.placesList) {
            Log.d("PLACES", e.toString())
        }
    }

}