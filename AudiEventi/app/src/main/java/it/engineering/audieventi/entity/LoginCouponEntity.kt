package it.engineering.audieventi.entity

//2.2.2.1 - login dell api coupon

data class LoginCouponEntity(
        val jwtToken: String
)
