package it.engineering.audieventi.entity

//2.3.1.7. - Sondaggio
data class SurveyEntity(
        val questions: ArrayList<QuestionEntity>,
        val status: String,
        val result: String,
        val resultCode: String,
        val resultMessage: String
)
