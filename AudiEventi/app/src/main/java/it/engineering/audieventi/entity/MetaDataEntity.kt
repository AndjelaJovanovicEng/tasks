package it.engineering.audieventi.entity

data class MetaDataEntity(
    val alt: String,
    val title: String,
    val width: String,
    val height: String
)
