package it.engineering.audieventi

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment

class CustomDialogEntity: DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val layout = inflater.inflate(R.layout.insert_coupon_dialog, container, false)

        val textViewGenerateCoupon = layout.findViewById<TextView>(R.id.generateCoupon)
        val submitCoupon = layout.findViewById<Button>(R.id.submitCoupon)
        val dismissDialog = layout.findViewById<ImageView>(R.id.dismissDialog)
        val confirmCoupon = layout.findViewById<Button>(R.id.confirmCoupon)


        submitCoupon.setOnClickListener {
            val coupon = generateCoupon()

            textViewGenerateCoupon.text = coupon
            submitCoupon.visibility = View.GONE
            confirmCoupon.visibility = View.VISIBLE

        }

        confirmCoupon.setOnClickListener {
            dialog!!.dismiss()
            startActivity(Intent(context, HomeCouponActivity::class.java))
        }

        dismissDialog.setOnClickListener {
            dialog!!.dismiss()
        }


        return layout
    }

    private fun generateCoupon(): String {

        val randomNumber = (2000000..3000000).random()

        return "Audi $randomNumber"

    }

}