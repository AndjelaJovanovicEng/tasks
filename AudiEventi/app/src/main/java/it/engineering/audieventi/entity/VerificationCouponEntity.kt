package it.engineering.audieventi.entity

// 2.3.1.2. verifica coupon
data class VerificationCouponEntity(
        val coupon: String,
        val eventId: String,
        val status: String,
        val valid: String,
        val eventStatus: String,
        val result: String,
        val resultCode: String,
        val resultMessage: String
)
