package it.engineering.audieventi.entity

//food experience - based on Id of event
//2.3.1.5.
data class FoodExperienceEntity(
    val id: String,
    val title: String,
    val header: String,
    val subtitle_food: HashMap<String, String>,
    val image_food: HashMap<String, Any>,
    val programme_experience: ArrayList<FoodExperienceDay> // array of experiences, each element represents a food experience for spesific day
)
