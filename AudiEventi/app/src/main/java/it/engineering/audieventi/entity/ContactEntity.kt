package it.engineering.audieventi.entity

//2.3.1.6 - Contatii e info
//contact list of events
data class ContactEntity(
        val id: Int,
        val title: HashMap<String, String>,
        val freeText: HashMap<String, String>,
        val contactImage: HashMap<String, Any>
)
