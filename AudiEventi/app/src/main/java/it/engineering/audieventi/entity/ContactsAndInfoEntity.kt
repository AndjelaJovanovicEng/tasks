package it.engineering.audieventi.entity

data class ContactsAndInfoEntity(
        val id: String,
        val title: HashMap<String, String>,
        val freeText: HashMap<String, String>
)
