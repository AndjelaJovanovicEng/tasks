package it.engineering.audieventi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import it.engineering.audieventi.global.DataGlobal

class SettingsActivity : AppCompatActivity() {

    lateinit var toggle: ActionBarDrawerToggle

    lateinit var navDrawerLayout: DrawerLayout
    lateinit var navigationView: NavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        navDrawerLayout = findViewById(R.id.drawerMenuLayoutSettings)
        navigationView = findViewById(R.id.navigationViewSettings)

        toggle = ActionBarDrawerToggle(this, navDrawerLayout, R.string.open, R.string.close)
        navDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        findViewById<ImageView>(R.id.btnHamburgerMenuSettings).setOnClickListener {
            navDrawerLayout.openDrawer(navigationView)
        }

        navigationView.setNavigationItemSelectedListener {

            when(it.itemId){
                R.id.home -> startActivity(Intent(this, HomeGuestActivity::class.java))
                R.id.calendar -> {
                    startActivity(Intent(this, CalendarioEventiActivity::class.java))
                }
                R.id.coupon -> {
                    startActivity(Intent(this, HomeGuestActivity::class.java))
                    DataGlobal.startHomeFromCalendarioEventi = true
                }
            }

            true
        }
    }
}