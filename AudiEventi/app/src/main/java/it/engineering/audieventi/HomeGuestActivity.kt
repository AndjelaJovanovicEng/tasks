package it.engineering.audieventi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import it.engineering.audieventi.global.DataGlobal

class HomeGuestActivity : AppCompatActivity() {

    lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_guest)

        val eventiAudiTitle = findViewById<TextView>(R.id.goToCalendarioEventi)
        val eventiAudiSubtitle = findViewById<TextView>(R.id.eventiAudiSubtitle)

        val gammaAudiTitle = findViewById<TextView>(R.id.gammaAudiTitle)
        val gammaAudiSubtitle = findViewById<TextView>(R.id.gammaAudiSubtitle)

        val ilTuoEventoTitle = findViewById<TextView>(R.id.showDialog)
        val ilTuoEventoSubtitle = findViewById<TextView>(R.id.ilTuoEventoSubtitle)

        val navDrawerLayout = findViewById<DrawerLayout>(R.id.drawerMenuLayout)
        val navigationView = findViewById<NavigationView>(R.id.navigationView)

        toggle = ActionBarDrawerToggle(this, navDrawerLayout, R.string.open, R.string.close)
        navDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        findViewById<ImageView>(R.id.btnHamburgerMenu).setOnClickListener {
            navDrawerLayout.openDrawer(navigationView)
        }

        navigationView.setNavigationItemSelectedListener {

            when(it.itemId){
                R.id.home -> navDrawerLayout.closeDrawer(navigationView)
                R.id.calendar -> {
                    startActivity(Intent(this, CalendarioEventiActivity::class.java))
                }
                R.id.coupon -> {
                    navDrawerLayout.closeDrawer(navigationView)
                    CustomDialogEntity().show(supportFragmentManager, "MyCustomFragment")
                }
            }

            true
        }

        DataGlobal.dataUtility.populateView(eventiAudiTitle, DataGlobal.langs.get("HOME_GUEST_EVENT_LIST"))
        DataGlobal.dataUtility.populateView(eventiAudiSubtitle,  DataGlobal.langs.get("HOME_GUEST_EVENT_LIST_BUTTON"))

        DataGlobal.dataUtility.populateView(gammaAudiTitle,  DataGlobal.langs.get("HOME_GUEST_AUDI_RANGE"))
        DataGlobal.dataUtility.populateView(gammaAudiSubtitle, DataGlobal.langs.get("HOME_GUEST_AUDI_RANGE_BUTTON"))

        DataGlobal.dataUtility.populateView(ilTuoEventoTitle,  DataGlobal.langs.get("HOME_GUEST_COUPON_LOGIN"))
        DataGlobal.dataUtility.populateView(ilTuoEventoSubtitle, DataGlobal.INSERISCI_COUPON)



        eventiAudiTitle.setOnClickListener {
            startActivity(Intent(this, CalendarioEventiActivity::class.java))
        }

        findViewById<TextView>(R.id.showDialog).setOnClickListener {
            CustomDialogEntity().show(supportFragmentManager, "MyCustomFragment")
        }

        if(DataGlobal.startHomeFromCalendarioEventi == true){
            CustomDialogEntity().show(supportFragmentManager, "MyCustomFragment")
        }

        findViewById<ImageView>(R.id.startSettingsActivity).setOnClickListener {
            startActivity(Intent(this, SettingsActivity::class.java))
        }

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)){
            return true
        }

        return super.onOptionsItemSelected(item)
    }

 }