package it.engineering.audieventi.entity

//2.3.1.3. - validazione coupon (punch)

data class PunchCouponEntity(
        val action: String,
        val coupon: String,
        val eventId: String,
        val status: String,
        val valid: String,
        val result: String,
        val resultCode: String,
        val resultMessage: String
)
