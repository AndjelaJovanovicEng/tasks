package it.engineering.audieventi.entity

data class BackgroundImage(
        val id: String,
        val href: String,
        val meta: MetaDataEntity
)
