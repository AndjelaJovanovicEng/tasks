package it.engineering.audieventi.entity

data class PlacesAndTerritories(
        val id: String,
        val title: String,
        val description: String,
        val placeSubtitle: String,
        val placeTitle: String,
        val carouselPlace: ArrayList<BackgroundImage>,
        val imagePlace: BackgroundImage
)
