package it.engineering.audieventi.entity

//programma evento premium - one premium event
//2.3.1.4.
data class EventEntityPremium(
        val id: String,
        val title: String,
        val description: HashMap<String, String>,
        val header_premium: String,
        val link_myaudi_premium: ArrayList<LinkEntity>,
        val programme_note: HashMap<String, String>,
        val activities: ArrayList<HashMap<String, Any>>,
        val subtitle: String,
        val image: HashMap<String, Any>
)
