package it.engineering.audieventi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import it.engineering.audieventi.global.DataGlobal

class AudiDriveExperienceActivity : AppCompatActivity() {

    lateinit var toggle: ActionBarDrawerToggle


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audi_drive_experience)

        val navDrawerLayout = findViewById<DrawerLayout>(R.id.drawerLayoutAudiDriveExp)
        val navigationView = findViewById<NavigationView>(R.id.navigationViewAudiDriveExp)

        val driveExpBannerImg = findViewById<ImageView>(R.id.driveExpBannerImg)
        val driveExpTitle = findViewById<TextView>(R.id.driveExpTitle)
        val driveExpSubtitle = findViewById<TextView>(R.id.driveExpSubtitle)
        val driveExpText = findViewById<TextView>(R.id.driveExpText)

        toggle = ActionBarDrawerToggle(this, navDrawerLayout, R.string.open, R.string.close)
        navDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        findViewById<ImageView>(R.id.btnHambuergerMenuDriveExp).setOnClickListener {
            navDrawerLayout.openDrawer(navigationView)
        }

        navigationView.setNavigationItemSelectedListener {

            when(it.itemId){
                R.id.home -> navDrawerLayout.closeDrawer(navigationView)
                R.id.programma -> startActivity(Intent(this, IlTuoEventoActivity::class.java))
                R.id.food_exp -> startActivity(Intent(this, FoodExperienceActivity::class.java))
                R.id.territory -> startActivity(Intent(this, TerritorioActivity::class.java))
                R.id.audi_exp -> startActivity(Intent(this, AudiDriveExperienceActivity::class.java))
                R.id.sondaggio -> {
                    Toast.makeText(this, "Not yet implemented", Toast.LENGTH_SHORT).show()
                    navDrawerLayout.closeDrawer(navigationView)
                }
                R.id.calendar -> startActivity(Intent(this, CalendarioEventiActivity::class.java))

                R.id.coupon -> {
                    navDrawerLayout.closeDrawer(navigationView)
                    CustomDialogEntity().show(supportFragmentManager, "MyCustomFragment")
                }

            }

            true
        }

        DataGlobal.dataUtility.DownloadImage(driveExpBannerImg).execute(DataGlobal.audiDriveExperienceList[0].imageAde.href)
        driveExpTitle.text = DataGlobal.audiDriveExperienceList[0].titleAde
        driveExpSubtitle.text = DataGlobal.audiDriveExperienceList[0].subtitleAde
        driveExpText.text = DataGlobal.audiDriveExperienceList[0].freeText

    }
}