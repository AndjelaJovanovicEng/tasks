package it.engineering.audieventi.utility

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.core.view.isVisible
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.navigation.NavigationView
import it.engineering.audieventi.CalendarioEventiActivity
import it.engineering.audieventi.CustomDialogEntity
import it.engineering.audieventi.R
import it.engineering.audieventi.global.DataGlobal
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.net.URL

class Utility: AppCompatActivity() {

     fun parseJSONLangs(data: JSONObject?) {
        if (data != null) {
            val it = data.keys()
            while (it.hasNext()) {
                val key = it.next()
                try {
                    if (data[key] is JSONArray) {
                        val arry = data.getJSONArray(key)
                        val size = arry.length()
                        for (i in 0 until size) {
                            parseJSONLangs(arry.getJSONObject(i))
                        }
                    } else if (data[key] is JSONObject) {
                        parseJSONLangs(data.getJSONObject(key))
                    } else {
                        println(key + ":" + data.getString(key))
                        DataGlobal.langs.put(key, data.getString(key))
                    }
                } catch (e: Throwable) {
                    try {
                        println(key + ":" + data.getString(key))
                    } catch (ee: Exception) {
                    }
                    e.printStackTrace()
                }
            }
        }
    }


    fun createUrlLink(str1: String, str2: String):String {
        return str1+str2
    }


    fun setAdapter(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>, recyclerView: RecyclerView, activity: AppCompatActivity){
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

    }

    fun populateView(textView: TextView, value: String?){
        textView.text = value
    }

    inner class DownloadImage(val imageHolder: ImageView): AsyncTask<String, Void, Bitmap?>(){

        override fun doInBackground(vararg params: String?): Bitmap? {
            var image: Bitmap? = null
            val imageUrl = params[0]

            try {
                val imageDownloaded = URL(imageUrl).openStream()
                image = BitmapFactory.decodeStream(imageDownloaded)
            } catch (e:Exception){
                e.printStackTrace()
            }

            return image
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)

            imageHolder.setImageBitmap(result)

        }
    }

     fun toogleHiddenView(layout: LinearLayout, img:ImageView){
        if(layout.isVisible != true){
            layout.visibility = View.VISIBLE
            img.setImageResource(R.drawable.up_icon)
        } else {
            layout.visibility = View.GONE
            img.setImageResource(R.drawable.down_icon)
        }
    }

}