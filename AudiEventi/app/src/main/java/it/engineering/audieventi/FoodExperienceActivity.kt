package it.engineering.audieventi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import it.engineering.audieventi.global.DataGlobal

class FoodExperienceActivity : AppCompatActivity() {

    lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_food_experience)

        val navDrawerLayout = findViewById<DrawerLayout>(R.id.drawerLayoutFoodExp)
        val navigationView = findViewById<NavigationView>(R.id.navigationViewFoodExp)

        val foodExpImg = findViewById<ImageView>(R.id.foodExpImg)
        val foodExpTitle = findViewById<TextView>(R.id.foodExpTitle)
        val foodExpDesc = findViewById<TextView>(R.id.foodExpDesc)

        val foodExpVenerdi = findViewById<TextView>(R.id.foodExpVenerdi)
        val foodExpSabato = findViewById<TextView>(R.id.foodExpSabato)
        val foodExpSabato27 = findViewById<TextView>(R.id.foodExpSabato27)
        val foodExpSabato27Cena = findViewById<TextView>(R.id.foodExpSabato27Cena)

        val foodExp1 = findViewById<LinearLayout>(R.id.foodExp1)
        val foodExp2 = findViewById<LinearLayout>(R.id.foodExp2)
        val foodExp3 = findViewById<LinearLayout>(R.id.foodExp3)
        val foodExp4 = findViewById<LinearLayout>(R.id.foodExp4)

        val hiddenLayout = findViewById<LinearLayout>(R.id.hiddenLayout)

        toggle = ActionBarDrawerToggle(this, navDrawerLayout, R.string.open, R.string.close)
        navDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        findViewById<ImageView>(R.id.btnHamburgerMenuFoodExp).setOnClickListener {
            navDrawerLayout.openDrawer(navigationView)
        }

        navigationView.setNavigationItemSelectedListener {

            when(it.itemId){
                R.id.home -> navDrawerLayout.closeDrawer(navigationView)
                R.id.programma -> startActivity(Intent(this, IlTuoEventoActivity::class.java))
                R.id.food_exp -> startActivity(Intent(this, FoodExperienceActivity::class.java))
                R.id.territory -> startActivity(Intent(this, TerritorioActivity::class.java))
                R.id.audi_exp -> startActivity(Intent(this, AudiDriveExperienceActivity::class.java))
                R.id.sondaggio -> {
                    Toast.makeText(this, "Not yet implemented", Toast.LENGTH_SHORT).show()
                    navDrawerLayout.closeDrawer(navigationView)
                }
                R.id.calendar -> startActivity(Intent(this, CalendarioEventiActivity::class.java))

                R.id.coupon -> {
                    navDrawerLayout.closeDrawer(navigationView)
                    CustomDialogEntity().show(supportFragmentManager, "MyCustomFragment")
                }

            }

            true
        }

        DataGlobal.dataUtility.DownloadImage(foodExpImg).execute(DataGlobal.foodExperienceList[0].image_food.get("href") as String)
        foodExpTitle.text = DataGlobal.foodExperienceList[0].title
        foodExpDesc.text = DataGlobal.foodExperienceList[0].subtitle_food.get("value")!!.drop(3).dropLast(4)

        val programmeExpArr = DataGlobal.foodExperienceList[0].programme_experience

        foodExpVenerdi.text = programmeExpArr[0].experience.startTime + " - " + programmeExpArr[0].experience.type
        foodExpSabato.text = programmeExpArr[1].experience.startTime + " - " + programmeExpArr[1].experience.type
        foodExpSabato27.text = programmeExpArr[2].experience.startTime + " - " +programmeExpArr[2].experience.type
        foodExpSabato27Cena.text = programmeExpArr[3].experience.startTime + " - " + programmeExpArr[3].experience.type


        foodExp2.setOnClickListener {
            DataGlobal.dataUtility.toogleHiddenView(hiddenLayout, findViewById(R.id.imgArrowIcon2))
        }

    }
}