package it.engineering.audieventi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.MenuItem
import android.widget.ImageView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.navigation.NavigationView
import it.engineering.audieventi.adapter.CalendarioEventiAdapter
import it.engineering.audieventi.global.DataGlobal

class CalendarioEventiActivity : AppCompatActivity() {

    lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calendario_eventi)

        val calendarioEventiRecyclerView = findViewById<RecyclerView>(R.id.calendarioEventiRecycler)

        DataGlobal.dataUtility.setAdapter(CalendarioEventiAdapter(DataGlobal.publicEvents) as RecyclerView.Adapter<RecyclerView.ViewHolder>, calendarioEventiRecyclerView, this)

        val navDrawerLayout = findViewById<DrawerLayout>(R.id.drawerMenuLayoutEventi)
        val navigationView = findViewById<NavigationView>(R.id.navigationViewEventi)

        toggle = ActionBarDrawerToggle(this, navDrawerLayout, R.string.open, R.string.close)
        navDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        findViewById<ImageView>(R.id.btnHamburgerMenuEventi).setOnClickListener {
            navDrawerLayout.openDrawer(navigationView)
        }

        navigationView.setNavigationItemSelectedListener {

            when(it.itemId){
                R.id.home -> startActivity(Intent(this, HomeGuestActivity::class.java))
                R.id.calendar -> {
                    navDrawerLayout.closeDrawer(navigationView)
                }
                R.id.coupon -> {
                    startActivity(Intent(this, HomeGuestActivity::class.java))
                    DataGlobal.startHomeFromCalendarioEventi = true
                }
            }

            true
        }

        findViewById<ImageView>(R.id.startSettingsActivityFromCalendario).setOnClickListener {
            startActivity(Intent(this, SettingsActivity::class.java))
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)){
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}