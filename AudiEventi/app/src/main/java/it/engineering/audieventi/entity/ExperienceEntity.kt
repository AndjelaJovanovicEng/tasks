package it.engineering.audieventi.entity

data class ExperienceEntity(
        val startTime: String,
        val type: String,
        val activity: String,
        val place: String, // (????)
        val description: String,
        val food: String,
        val allergens: String // html format
        )
