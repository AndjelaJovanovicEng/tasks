package it.engineering.audieventi.entity

// 2.3.1.1. eventi pubblici - one public event

data class EventEntityPublic(
    val id: String,
    val title : String,
    val description: HashMap<String, String>,
    val event_date: String,
    val link_myaudi: HashMap<String, Any>,
    val priority: Int,
    val status: String,
    val image_background: HashMap<String, Any>
)
