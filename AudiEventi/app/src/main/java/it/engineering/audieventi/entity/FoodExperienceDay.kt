package it.engineering.audieventi.entity

//experience day for programma_experience array
data class FoodExperienceDay(
    val day: String,
    val experience: ExperienceEntity
)
