package it.engineering.audieventi.entity

// 2.2.2.1 login dell api

data class LoginGuestEntity(
        val clientId: String,
        val clientSecret: String
)
