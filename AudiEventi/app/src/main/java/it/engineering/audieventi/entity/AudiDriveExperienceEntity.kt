package it.engineering.audieventi.entity

//2.3.1.9.
data class AudiDriveExperienceEntity(
        val data: Any,
        val id: String,
        val freeText: String,
        val subtitleAde: String,
        val titleAde: String,
        val carouselAde: ArrayList<BackgroundImage>,
        val imageAde: BackgroundImage
)
