package it.engineering.audieventi.enums

enum class QuestionType {
    WEBFORM_LIKERT,
    RADIOS,
    WEBFORM_RADIOS_OTHER,
    CHECKBOXES,
    WEBFORM_CHECKBOXES_OTHER,
    TEXTAREA
}