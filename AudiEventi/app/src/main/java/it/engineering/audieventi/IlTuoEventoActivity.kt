package it.engineering.audieventi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.isVisible
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import it.engineering.audieventi.global.DataGlobal

class IlTuoEventoActivity : AppCompatActivity() {

    lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_il_tuo_evento)

        val ilTuoBannerImage = findViewById<ImageView>(R.id.ilTuoEventoImg)
        val eventoTitle = findViewById<TextView>(R.id.eventoTitle)
        val eventoSubtitle = findViewById<TextView>(R.id.eventoSubtitle)
        val noteProgramma = findViewById<TextView>(R.id.noteProgramma)

        val activityVenerdi = findViewById<TextView>(R.id.activityVenerdi)
        val activitySabato = findViewById<TextView>(R.id.activitySabato)
        val activityDomencia = findViewById<TextView>(R.id.activityDomencia)

        val eventoVenerdi = findViewById<LinearLayout>(R.id.eventoVenerdi)
        val eventiSabato = findViewById<LinearLayout>(R.id.eventiSabato)
        val eventiDomencia = findViewById<LinearLayout>(R.id.eventiDomencia)

        val eventActivitesDetail = findViewById<LinearLayout>(R.id.eventActivitesDetail)


        val navDrawerLayout = findViewById<DrawerLayout>(R.id.drawerLayoutIlTuoEvento)
        val navigationView = findViewById<NavigationView>(R.id.navigationViewIlTuoEvento)

        toggle = ActionBarDrawerToggle(this, navDrawerLayout, R.string.open, R.string.close)
        navDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        findViewById<ImageView>(R.id.btnHamburgerMenuIlTuoEveneto).setOnClickListener {
            navDrawerLayout.openDrawer(navigationView)
        }


        navigationView.setNavigationItemSelectedListener {

            when(it.itemId){
                R.id.home -> navDrawerLayout.closeDrawer(navigationView)
                R.id.programma -> startActivity(Intent(this, IlTuoEventoActivity::class.java))
                R.id.food_exp -> startActivity(Intent(this, FoodExperienceActivity::class.java))
                R.id.territory -> startActivity(Intent(this, TerritorioActivity::class.java))
                R.id.audi_exp -> startActivity(Intent(this, AudiDriveExperienceActivity::class.java))
                R.id.sondaggio -> {
                    Toast.makeText(this, "Not yet implemented", Toast.LENGTH_SHORT).show()
                    navDrawerLayout.closeDrawer(navigationView)
                }
                R.id.calendar -> startActivity(Intent(this, CalendarioEventiActivity::class.java))

                R.id.coupon -> {
                    navDrawerLayout.closeDrawer(navigationView)
                    CustomDialogEntity().show(supportFragmentManager, "MyCustomFragment")
                }

            }

            true
        }

        DataGlobal.dataUtility.DownloadImage(ilTuoBannerImage).execute(DataGlobal.premiumEvents[0].image.get("href") as String)
        eventoTitle.text = DataGlobal.premiumEvents[0].title
        eventoSubtitle.text = DataGlobal.premiumEvents[0].description.get("value")!!.drop(3).dropLast(4)
        noteProgramma.text = DataGlobal.premiumEvents[0].programme_note.get("value")!!.drop(3).dropLast(4)

        val activityArray = DataGlobal.premiumEvents[0].activities[0].get("activities") as ArrayList<HashMap<String, String>>
        activityVenerdi.text = activityArray[0].get("inizio")
        activitySabato.text = activityArray[1].get("inizio")
        activityDomencia.text = activityArray[2].get("inizio")

        eventiSabato.setOnClickListener {

            DataGlobal.dataUtility.toogleHiddenView(eventActivitesDetail,findViewById(R.id.imgArrowDown1))

        }
    }

}