package it.engineering.audieventi.global

import android.annotation.SuppressLint
import android.widget.ImageView
import android.widget.TextView
import it.engineering.audieventi.R
import it.engineering.audieventi.entity.*
import it.engineering.audieventi.utility.Utility

object DataGlobal {

    //lista eventi pubblici - list of public events sorted by priority
    val publicEvents : ArrayList<EventEntityPublic> = arrayListOf()

    //lista eventi premium

    val premiumEvents: ArrayList<EventEntityPremium> = arrayListOf()

    //lista eventi food experience

    val foodExperienceList: ArrayList<FoodExperienceEntity> = arrayListOf()

    //list audi driving experience

    val audiDriveExperienceList: ArrayList<AudiDriveExperienceEntity> = arrayListOf()

    //list places/territories

    val placesList: ArrayList<PlacesAndTerritories> = arrayListOf()

    val langs: HashMap<String, String> = hashMapOf()

    val baseUrl = "http://mobileapp-coll.engds.it/AudiEventi"
    val langsUrl = "/langs/it_IT.json"
    val configUrl = "/config/config.json"


    val dataUtility = Utility()

    var startHomeFromCalendarioEventi = false

    //string constants

    val INSERISCI_COUPON = "Inserisci coupon"
    val RECCONECT_TO_NATURE = "Reconnect to nature"
    val RECCONECT_TO_NATURE_SUBTITLE = "con Hervé Barmasse"
    val FOOD_EXP_SUBTITLE = "Cosa mangiare"
    val AUDI_DRIVE_EXP = "Scopri il programma"
    val INFO_I_CONTATI = "Info & Contatti"


    val INFO_I_CONTATI_IMG_URL = "https://account.myaudi.it/api/v1/assets/images/013aeefc-8bae-4e00-840f-9fcb0bff0052/cortina-e-portrait.jpeg?mode=crop&w=600x600"
    val SONDAGIO_IMAGE_URL = "https://www.audi.com/content/dam/gbp2/company/contact/1920x600-desktop-Kontakt-AUDI-AG-2.jpg?imwidth=1920&imdensity=1"

}