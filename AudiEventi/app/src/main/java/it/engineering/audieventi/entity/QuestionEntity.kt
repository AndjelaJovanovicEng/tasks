package it.engineering.audieventi.entity

//questions for survey
data class QuestionEntity(
        val webformId: Int, // surveyId
        val title: String,
        val questionId: Int,
        val required: Boolean,
        val requiredError: String,
        val multipleWebform: Boolean,
        val type: String
)


