package it.engineering.csvparsedomaci.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import it.engineering.csvparsedomaci.MainActivity
import it.engineering.csvparsedomaci.MangaDetails
import it.engineering.csvparsedomaci.MangaDetailsGlobal
import it.engineering.csvparsedomaci.R
import it.engineering.csvparsedomaci.entity.MangaEntity

var btnClicked = 0

class RecyclerViewAdapter(private val mangaData: ArrayList<MangaEntity>): RecyclerView.Adapter<CustomViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val recycleViewRow = layoutInflater.inflate(R.layout.manga_row, parent, false)
        return CustomViewHolder(recycleViewRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val titleTextView = holder.view.findViewById<TextView>(R.id.mangaTitle)
        val authorTextView = holder.view.findViewById<TextView>(R.id.author)
        val illustratorTextView = holder.view.findViewById<TextView>(R.id.illustrator)
        val publishedTextView = holder.view.findViewById<TextView>(R.id.published)

        titleTextView.text = mangaData[position].title
        authorTextView.text = mangaData[position].author
        illustratorTextView.text = mangaData[position].illustrator
        publishedTextView.text = mangaData[position].published

        val details = holder.view.findViewById<TextView>(R.id.details)
        details.setOnClickListener {
            MangaDetailsGlobal.mangaTitle = mangaData[position].title
            MangaDetailsGlobal.mangaAuthor = mangaData[position].author
            MangaDetailsGlobal.mangaIllustrator = mangaData[position].illustrator
            btnClicked = 1
            startActivity(holder.view.context, Intent(holder.view.context, MangaDetails::class.java),null)
            Toast.makeText(holder.view.context, "Title, author and illustrator showed!", Toast.LENGTH_SHORT).show()
        }


        val detailsFull = holder.view.findViewById<TextView>(R.id.detailsFull)

        detailsFull.setOnClickListener {
            val intent = Intent(holder.view.context, MangaDetails::class.java)
            intent.putExtra("mangaTitle", mangaData[position].title)
            intent.putExtra("mangaAuthor", mangaData[position].author)
            btnClicked = 2
            startActivity(holder.view.context, intent,null)
            Toast.makeText(holder.view.context, "Title and author showed!", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getItemCount(): Int {
        return mangaData.count()
    }


}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}