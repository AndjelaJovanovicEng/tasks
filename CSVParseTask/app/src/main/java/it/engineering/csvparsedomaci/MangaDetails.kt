package it.engineering.csvparsedomaci

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import it.engineering.csvparsedomaci.adapter.btnClicked
import org.w3c.dom.Text

class MangaDetails : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manga_details)

        val btnBack = findViewById<Button>(R.id.goBack)
        btnBack.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }

        val titleManga = findViewById<TextView>(R.id.titleManga)
        val authorManga = findViewById<TextView>(R.id.authorManga)
        val illustratorManga = findViewById<TextView>(R.id.illustratorManga)

        if (btnClicked == 1){
            titleManga.text = MangaDetailsGlobal.mangaTitle
            authorManga.text = MangaDetailsGlobal.mangaAuthor
            illustratorManga.text = MangaDetailsGlobal.mangaIllustrator
        } else if (btnClicked == 2){
            titleManga.text = intent.getStringExtra("mangaTitle").toString()
            authorManga.text = intent.getStringExtra("mangaAuthor").toString()

        }

    }
}