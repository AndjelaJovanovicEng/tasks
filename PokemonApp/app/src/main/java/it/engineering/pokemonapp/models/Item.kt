package it.engineering.pokemonapp.models

data class Item(
    val name: String,
    val url: String
)