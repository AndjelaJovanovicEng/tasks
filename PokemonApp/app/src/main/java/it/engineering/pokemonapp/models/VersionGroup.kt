package it.engineering.pokemonapp.models

data class VersionGroup(
    val name: String,
    val url: String
)