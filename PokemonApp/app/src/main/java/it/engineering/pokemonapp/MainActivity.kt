package it.engineering.pokemonapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import dagger.hilt.android.AndroidEntryPoint
import it.engineering.pokemonapp.adapter.PokemonListAdapter
import it.engineering.pokemonapp.repository.PokemonRepository
import it.engineering.pokemonapp.util.Resource
import it.engineering.pokemonapp.util.UtilityFunctions
import it.engineering.pokemonapp.viewmodel.PokemonViewModel
import it.engineering.pokemonapp.viewmodelfactory.PokemonViewModelProviderFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.pokemon_cell.*
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel: PokemonViewModel by viewModels()

    lateinit var pokemonAdapter: PokemonListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        val pokemonRepository = PokemonRepository()
//        val viewModelProviderFactory = PokemonViewModelProviderFactory(pokemonRepository)

  //      viewModel = ViewModelProvider(this, viewModelProviderFactory).get(PokemonViewModel::class.java)

        pokemonAdapter = PokemonListAdapter()

        UtilityFunctions.setupRecyclerView(pokemonAdapter, rvAllPokemons, this@MainActivity)

        viewModel.allPokemons.observe(this, Observer { response ->
            when(response) {
                is Resource.Success -> {
                    UtilityFunctions.hideProgressBar(paginationProgressBar)
                    response.data?.let {
                        pokemonAdapter.differ.submitList(it.results)
                    }
                }

                is Resource.Error -> {
                    UtilityFunctions.hideProgressBar(paginationProgressBar)
                    response.message?.let {
                        Log.e("ERROR", "An error occured: ${it}")
                    }
                }

                is Resource.Loading -> {
                    UtilityFunctions.showProgressBar(paginationProgressBar)
                }
            }
        })

    }
}