package it.engineering.pokemonapp.models

data class GameIndice(
    val game_index: Int,
    val version: Version
)