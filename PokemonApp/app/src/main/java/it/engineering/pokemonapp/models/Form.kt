package it.engineering.pokemonapp.models

data class Form(
    val name: String,
    val url: String
)