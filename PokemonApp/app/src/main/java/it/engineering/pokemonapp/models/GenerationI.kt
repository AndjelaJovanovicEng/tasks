package it.engineering.pokemonapp.models

data class GenerationI(
    val red_blue: RedBlue,
    val yellow: Yellow
)