package it.engineering.pokemonapp.di

import android.content.Intent
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import it.engineering.pokemonapp.api.PokemonApi
import it.engineering.pokemonapp.util.Constants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideRetrofitInstance(): PokemonApi = Retrofit.Builder()
        .baseUrl(Constants.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(PokemonApi::class.java)

//    @Singleton
//    @Provides
//    fun providePokedexRespository(api: PokemonApi): PokedexRepository = DefaultPokedexRepository(api)

    @Singleton
    @Provides
    fun providePokemonName(): String = ""

//    @Singleton
//    @Provides
//    fun providePokemonApi(retrofit: Retrofit): PokemonApi = retrofit.create(PokemonApi::class.java)
}