package it.engineering.pokemonapp.models

data class MoveLearnMethod(
    val name: String,
    val url: String
)