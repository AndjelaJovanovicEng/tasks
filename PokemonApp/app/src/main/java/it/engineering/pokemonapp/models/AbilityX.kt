package it.engineering.pokemonapp.models

data class AbilityX(
    val name: String,
    val url: String
)