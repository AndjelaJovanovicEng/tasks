package it.engineering.pokemonapp.models

data class OfficialArtwork(
    val front_default: String
)