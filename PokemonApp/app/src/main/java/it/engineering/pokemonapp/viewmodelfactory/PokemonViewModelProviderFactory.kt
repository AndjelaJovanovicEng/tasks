package it.engineering.pokemonapp.viewmodelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import it.engineering.pokemonapp.repository.PokemonRepository
import it.engineering.pokemonapp.viewmodel.PokemonViewModel
import javax.inject.Inject

class PokemonViewModelProviderFactory @Inject constructor(
        val pokemonRepository: PokemonRepository
    ): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PokemonViewModel(pokemonRepository) as T
    }
}