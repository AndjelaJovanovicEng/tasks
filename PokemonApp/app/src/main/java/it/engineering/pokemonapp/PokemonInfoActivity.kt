package it.engineering.pokemonapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import it.engineering.pokemonapp.adapter.PokemonStatsAdapter
import it.engineering.pokemonapp.util.Resource
import it.engineering.pokemonapp.util.UtilityFunctions
import it.engineering.pokemonapp.viewmodel.PokemonInfoViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_pokemon_info.*
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class PokemonInfoActivity : AppCompatActivity() {

    private val viewModel: PokemonInfoViewModel by viewModels()

    @Inject
    lateinit var pokemonName: String

    lateinit var pokemonStatsAdapter: PokemonStatsAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pokemon_info)

        pokemonName = intent.getStringExtra("name")!!
        println(pokemonName)

//        val pokemonRepository = PokemonRepository()
//        val viewModelProviderFactory = PokemonInfoViewModelProviderFactory(pokemonRepository, pokemonName!!)
//
//        viewModel = ViewModelProvider(this, viewModelProviderFactory).get(PokemonInfoViewModel::class.java)

        pokemonStatsAdapter = PokemonStatsAdapter()

        UtilityFunctions.setupPokemonStatsRecyclerView(pokemonStatsAdapter, rvPokemonStats, this@PokemonInfoActivity)

        viewModel.pokemon.observe(this, Observer { response ->
            when(response) {
                is Resource.Success -> {
                   UtilityFunctions.hideProgressBar(statsProgressBar)
                    response.data?.let {
                        println(it)
                        tvPokemonId.text = "#" + it.id + " " + it.name
                        val types = it.types
                        for (pokemonType in types){
                            tvPokemonType.text = pokemonType.type.name
                        }
                        tvPokemonWeight.text = it.weight.toString()
                        tvPokemonHeight.text = it.height.toString()

                        Glide.with(this).load(it.sprites.front_default).into(ivPokemonDetailImage)
                        Glide.with(this).load(it.sprites.front_shiny).into(ivPokemonDetailImage1)
                        Glide.with(this).load(it.sprites.back_shiny).into(ivPokemonDetailImage2)

                        pokemonStatsAdapter.differ.submitList(it.stats)
                    }
                }

                is Resource.Error -> {
                    UtilityFunctions.hideProgressBar(statsProgressBar)
                    response.message?.let {
                        Log.e("ERROR", "An error occured: ${it}")
                    }
                }

                is Resource.Loading -> {
                    UtilityFunctions.showProgressBar(statsProgressBar)
                }
            }
        })

    }
}