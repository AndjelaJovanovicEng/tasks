package it.engineering.pokemonapp.models

data class Species(
    val name: String,
    val url: String
)