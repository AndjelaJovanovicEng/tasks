package it.engineering.pokemonapp.models

data class Type(
    val slot: Int,
    val type: TypeX
)