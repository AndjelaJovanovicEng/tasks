package it.engineering.pokemonapp.viewmodel

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import it.engineering.pokemonapp.models.Pokemon
import it.engineering.pokemonapp.repository.PokemonRepository
import it.engineering.pokemonapp.util.Resource
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

class PokemonInfoViewModel @ViewModelInject constructor(
    private val pokemonRepository: PokemonRepository,
    pokemonName: String

): ViewModel()  {

    val pokemon: MutableLiveData<Resource<Pokemon>> = MutableLiveData()

    init {
        getPokemonInfo(pokemonName)
    }

    private fun getPokemonInfo(name: String) = viewModelScope.launch {
        pokemon.postValue(Resource.Loading())
        val response = pokemonRepository.getPokemonInfo(name)
        pokemon.postValue(handlePokemonReponse(response))
    }


    private fun handlePokemonReponse(response: Response<Pokemon>): Resource<Pokemon> {
        if(response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Resource.Success(resultResponse)
            }
        }

        return Resource.Error(response.message())
    }

}