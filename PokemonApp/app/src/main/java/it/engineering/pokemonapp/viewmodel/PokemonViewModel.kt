package it.engineering.pokemonapp.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import it.engineering.pokemonapp.models.list.PokemonList
import it.engineering.pokemonapp.repository.PokemonRepository
import it.engineering.pokemonapp.util.Resource
import kotlinx.coroutines.launch
import retrofit2.Response

class PokemonViewModel @ViewModelInject constructor
    (
        private val pokemonRepository: PokemonRepository

    ): ViewModel() {

    val allPokemons: MutableLiveData<Resource<PokemonList>> = MutableLiveData()


    init {
        getAllPokemons(20,0)
    }

    private fun getAllPokemons(limit: Int, offset: Int) = viewModelScope.launch {
        allPokemons.postValue(Resource.Loading())
        val response = pokemonRepository.getPokemonList(limit, offset)
        allPokemons.postValue(handlePokemonReponse(response))
    }

    private fun handlePokemonReponse(response: Response<PokemonList>): Resource<PokemonList> {
        if(response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Resource.Success(resultResponse)
            }
        }

        return Resource.Error(response.message())
    }

}