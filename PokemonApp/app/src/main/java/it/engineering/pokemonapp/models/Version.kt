package it.engineering.pokemonapp.models

data class Version(
    val name: String,
    val url: String
)