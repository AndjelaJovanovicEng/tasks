package it.engineering.pokemonapp.models

data class HeldItem(
    val item: Item,
    val version_details: List<VersionDetail>
)