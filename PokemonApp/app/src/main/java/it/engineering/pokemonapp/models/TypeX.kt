package it.engineering.pokemonapp.models

data class TypeX(
    val name: String,
    val url: String
)