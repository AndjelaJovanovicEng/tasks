package it.engineering.pokemonapp.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import it.engineering.pokemonapp.PokemonInfoActivity
import it.engineering.pokemonapp.R
import it.engineering.pokemonapp.models.list.Result
import kotlinx.android.synthetic.main.pokemon_cell.view.*
import java.util.*
import kotlin.collections.ArrayList

class PokemonListAdapter: RecyclerView.Adapter<PokemonListAdapter.PokemonListViewHolder>() {


    private val differCalback = object: DiffUtil.ItemCallback<Result>() {
        override fun areItemsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differCalback)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonListViewHolder {
       return PokemonListViewHolder(
           LayoutInflater.from(parent.context).inflate(
               R.layout.pokemon_cell,
               parent,
               false
           )
       )

    }

    override fun onBindViewHolder(holder: PokemonListViewHolder, position: Int) {
        val pokemon = differ.currentList[position]


        holder.itemView.apply {
            //Glide.with(this).load(pokemon.url).into(ivPokemonImage)

            when(pokemon.name){
                "bulbasaur" -> ivPokemonImage.setImageResource(R.drawable.bulbasaur)
                "ivysaur" -> ivPokemonImage.setImageResource(R.drawable.bulbasaur1)
                "venusaur" -> ivPokemonImage.setImageResource(R.drawable.bulbasaur3)
                "charmander" -> ivPokemonImage.setImageResource(R.drawable.charmander)
                "charmeleon" -> ivPokemonImage.setImageResource(R.drawable.charmeleon)
                "charizard" -> ivPokemonImage.setImageResource(R.drawable.charizard)
                "squirtle" -> ivPokemonImage.setImageResource(R.drawable.squirtle)
                "wartortle" -> ivPokemonImage.setImageResource(R.drawable.wartortle)
                "blastoise" -> ivPokemonImage.setImageResource(R.drawable.blastoise)
                "caterpie" -> ivPokemonImage.setImageResource(R.drawable.caterpie)
                "metapod" -> ivPokemonImage.setImageResource(R.drawable.metapod)
                "butterfree" -> ivPokemonImage.setImageResource(R.drawable.butterfree)
                "weedle" -> ivPokemonImage.setImageResource(R.drawable.weedle)
                "kakuna" -> ivPokemonImage.setImageResource(R.drawable.kakuna)
                "beedrill" -> ivPokemonImage.setImageResource(R.drawable.beedrill)
                "pidgey" -> ivPokemonImage.setImageResource(R.drawable.pidgey)
                "pidgeotto" -> ivPokemonImage.setImageResource(R.drawable.pidgeotto)
                "pidgeot" -> ivPokemonImage.setImageResource(R.drawable.pidgeot)
                "rattata" -> ivPokemonImage.setImageResource(R.drawable.rattata)
                "raticate" -> ivPokemonImage.setImageResource(R.drawable.raticate)
            }

            tvTitle.text = pokemon.name.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
        }

        val pokemonCell = holder.itemView.findViewById<ConstraintLayout>(R.id.pokemonCell)

        pokemonCell.setOnClickListener {
            val intent = Intent(holder.itemView.context, PokemonInfoActivity::class.java)

            intent.putExtra("name", pokemon.name)

            startActivity(holder.itemView.context, intent, null)
        }

    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }


    inner class PokemonListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
}