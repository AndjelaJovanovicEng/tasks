package it.engineering.pokemonapp.models

data class Other(
    val dream_world: DreamWorld,
    val official_artwork: OfficialArtwork
)