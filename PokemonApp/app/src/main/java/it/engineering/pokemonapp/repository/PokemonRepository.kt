package it.engineering.pokemonapp.repository

import it.engineering.pokemonapp.api.PokemonApi
import it.engineering.pokemonapp.api.RetrofitInstance
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PokemonRepository @Inject constructor(
    private val pokemonApi: PokemonApi
) {

    suspend fun getPokemonList(limit: Int, offset: Int) =
        pokemonApi.getPokemonList(limit, offset)

    suspend fun getPokemonInfo(name: String) =
        pokemonApi.getPokemonInfo(name)
}