package it.engineering.pokemonapp.models

data class GenerationVi(
    val omegaruby_alphasapphire: OmegarubyAlphasapphire,
    val x_y: XY
)