package it.engineering.pokemonapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import it.engineering.pokemonapp.R
import it.engineering.pokemonapp.models.Stat
import kotlinx.android.synthetic.main.pokemon_stats_cell.view.*

class PokemonStatsAdapter: RecyclerView.Adapter<PokemonStatsAdapter.PokemonStatsViewHolder>() {

    private val differCalback = object: DiffUtil.ItemCallback<Stat>() {
        override fun areItemsTheSame(oldItem: Stat, newItem: Stat): Boolean {
            return oldItem.base_stat == newItem.base_stat

        }

        override fun areContentsTheSame(oldItem: Stat, newItem: Stat): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differCalback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonStatsViewHolder {
        return PokemonStatsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.pokemon_stats_cell,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: PokemonStatsViewHolder, position: Int) {
        val pokemonStat = differ.currentList[position]

        holder.itemView.apply {
            tvPokemonStatLabel.text = pokemonStat.stat.name
            tvPokemonStat.text = pokemonStat.base_stat.toString()
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    inner class PokemonStatsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
}