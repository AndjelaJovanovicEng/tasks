package it.engineering.pokemonapp.models

data class StatX(
    val name: String,
    val url: String
)