package it.engineering.pokemonapp.util

import android.content.Context
import android.content.res.Configuration
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.engineering.pokemonapp.adapter.PokemonListAdapter
import it.engineering.pokemonapp.adapter.PokemonStatsAdapter
import kotlinx.android.synthetic.main.activity_main.*

class UtilityFunctions {
    companion object {
         fun hideProgressBar(paginationProgressBar: ProgressBar){
            paginationProgressBar.visibility = View.INVISIBLE
        }

         fun showProgressBar(paginationProgressBar: ProgressBar){
            paginationProgressBar.visibility = View.VISIBLE
        }

         fun setupRecyclerView(pokemonAdapter: PokemonListAdapter, rvAllPokemons: RecyclerView, context: Context) {
            //pokemonAdapter = PokemonListAdapter()
            rvAllPokemons.apply {
                adapter = pokemonAdapter
                if(resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
                    layoutManager = GridLayoutManager(context, 2)
                } else {
                    layoutManager = GridLayoutManager(context, 3)
                }

            }

        }


        fun setupPokemonStatsRecyclerView(pokemonAdapter: PokemonStatsAdapter, rvPokemonStats: RecyclerView, context: Context) {
            //pokemonAdapter = PokemonListAdapter()
            rvPokemonStats.apply {
                adapter = pokemonAdapter
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

//                if(resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
//                    layoutManager = GridLayoutManager(context, 2)
//                } else {
//                    layoutManager = GridLayoutManager(context, 3)
//                }

            }

        }
    }
}