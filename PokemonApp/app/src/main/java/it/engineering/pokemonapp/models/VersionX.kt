package it.engineering.pokemonapp.models

data class VersionX(
    val name: String,
    val url: String
)