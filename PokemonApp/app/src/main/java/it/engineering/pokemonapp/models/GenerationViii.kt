package it.engineering.pokemonapp.models

data class GenerationViii(
    val icons: IconsX
)