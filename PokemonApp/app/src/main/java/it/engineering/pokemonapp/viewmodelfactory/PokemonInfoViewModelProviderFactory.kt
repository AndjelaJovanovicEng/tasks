package it.engineering.pokemonapp.viewmodelfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import it.engineering.pokemonapp.repository.PokemonRepository
import it.engineering.pokemonapp.viewmodel.PokemonInfoViewModel
import javax.inject.Inject

class PokemonInfoViewModelProviderFactory @Inject constructor(
    val pokemonRepository: PokemonRepository,
    val pokemonName: String): ViewModelProvider.Factory{

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PokemonInfoViewModel(pokemonRepository, pokemonName) as T
    }
}