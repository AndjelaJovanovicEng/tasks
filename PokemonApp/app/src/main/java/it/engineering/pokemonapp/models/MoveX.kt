package it.engineering.pokemonapp.models

data class MoveX(
    val name: String,
    val url: String
)