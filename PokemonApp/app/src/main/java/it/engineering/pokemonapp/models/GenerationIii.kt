package it.engineering.pokemonapp.models

data class GenerationIii(
    val emerald: Emerald,
    val firered_leafgreen: FireredLeafgreen,
    val ruby_sapphire: RubySapphire
)