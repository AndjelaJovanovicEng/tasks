package it.engineering.pokemonapp.models.list

data class Result(
    val name: String,
    val url: String
)