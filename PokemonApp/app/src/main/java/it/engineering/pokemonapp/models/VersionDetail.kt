package it.engineering.pokemonapp.models

data class VersionDetail(
    val rarity: Int,
    val version: VersionX
)