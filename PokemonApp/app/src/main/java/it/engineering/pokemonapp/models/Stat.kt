package it.engineering.pokemonapp.models

data class Stat(
    val base_stat: Int,
    val effort: Int,
    val stat: StatX
)