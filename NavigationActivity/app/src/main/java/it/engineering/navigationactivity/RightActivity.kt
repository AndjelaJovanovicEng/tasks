package it.engineering.navigationactivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_right.*

class RightActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_right)

        if(DataGlobal.isRight == true){
            rightArrowRightAct.visibility = View.GONE
        }
        leftArrowRightAct.setOnClickListener {
            DataGlobal.isRight = false
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }
}