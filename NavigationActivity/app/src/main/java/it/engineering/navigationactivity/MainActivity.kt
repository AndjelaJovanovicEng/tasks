package it.engineering.navigationactivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        leftArrow.setOnClickListener{
            startActivity(Intent(this, LeftActivity::class.java))
            DataGlobal.isLeft = true
            finish()
        }

        rightArrow.setOnClickListener {
            startActivity(Intent(this, RightActivity::class.java))
            DataGlobal.isRight = true
            finish()
        }
    }
}