package it.engineering.navigationactivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_left.*

class LeftActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_left)

        if(DataGlobal.isLeft == true){
            leftArrowLeftAct.visibility = View.GONE
        }
        rightArrowLeftAct.setOnClickListener {
            DataGlobal.isLeft = false
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }
}