package com.example.tablelayout

import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.marginTop
import androidx.core.view.setPadding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var tableContent: ArrayList<String> = arrayListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createDatasource()
        addStringToTable()

        val addButton = findViewById<Button>(R.id.addNameToArray)
        val editText = findViewById<EditText>(R.id.imeEditText)
        val tableView = findViewById<TableLayout>(R.id.tableView)

        tableView.isStretchAllColumns = true

        addButton.setOnClickListener {
            tableContent.add(editText.text.toString())
            addStringToTable()
        }

    }

    fun addStringToTable(){
        for (ime in tableContent){
            val row = TableRow(this)
            row.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            row.gravity = Gravity.CENTER
            val textView = TextView(this)
            textView.textSize = 15f
            textView.gravity = Gravity.CENTER
            textView.setPadding(25)
            textView.apply {
                textView.text = ime
            }
            textView.setBackgroundResource(R.color.blue)
            textView.setTextColor(Color.parseColor("#000000"))
            textView.setTypeface(null, Typeface.BOLD)
            row.addView(textView)
            val tableView = findViewById<TableLayout>(R.id.tableView)
            tableView.addView(row)

        }
    }

    fun createDatasource(){
        tableContent.add("Hari Poter i relikvije smtri")
        tableContent.add("Hari Poter i vatreni pehar")
        tableContent.add("Hari Poter i kamen mudrosti")
        tableContent.add("Hari Poter i ukleto dete")
        tableContent.add("Hari Poter i dvorana tajni")
        tableContent.add("Hari Poter i red feniksa")
        tableContent.add("Hari Poter i zatvorenik iz Askabana")
        tableContent.add("Fantasticne zveri i gde ih naci")
        tableContent.add("Fantasticne zveri - Grindenvelovi zlocini")
        tableContent.add("Pripovesti Barda Bidla")
        tableContent.add("Upraznjeno mesto")
        tableContent.add("Kratke price sa Hogvortsa")
        tableContent.add("Ikabor")
        tableContent.add("Ime vetra")
        tableContent.add("Kako ubiti pticu rugalicu")
        tableContent.add("Americki bogovi")
        tableContent.add("Dozivljaji Haklberi Fina")
        tableContent.add("Tom Sojer")
        tableContent.add("100 Godina Samoce")
        tableContent.add("Pesma Leda i Vatre")
    }
}