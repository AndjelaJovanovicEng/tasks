package com.example.recyclerview

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.widget.TextView
import androidx.core.view.setPadding
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.recyclerview.adapter.RecyclerViewAdapter
import com.example.recyclerview.model.NewsEntity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val newsArray : ArrayList<NewsEntity> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createDataSource()

        val adapter = RecyclerViewAdapter(newsArray)
        recyclerViewNews.adapter = adapter
        recyclerViewNews.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerViewNews.setBackgroundResource(R.color.blue)
        recyclerViewNews.setPadding(20)

}

    private fun createDataSource(){
        newsArray.add(NewsEntity(
            "Test: Redmi Note 10",
            "To što je Xiaomi uradio sa Redmi Note 10 Pro telefonom, predstavlja nemoguću misiju za kompletnu industriju.",
             R.drawable.redmi
        ))

        newsArray.add(NewsEntity(
            "AMD Radeon RX",
            "Najjači adut u rukama Radeon RDNA2 tehnologije je spreman da pomrsi račune Nvidia GeForce konkurenciji.",
            R.drawable.radeon
        ))

        newsArray.add(NewsEntity(
            "Test: Acer XB253Q GX",
            "Acer XB253Q GX gaming monitor je jedan od modela nove generacije koji po razumnoj ceni nudi dosta toga",
            R.drawable.acer
        ))

        newsArray.add(NewsEntity(
            "AMD koristi PCI drajvere",
            "Alex Ionescu je na svom Twitter nalogu izneo tvrdnju o neuobičajenom ponašanju AMD-ovog PnP PCI drajvera.",
            R.drawable.ryzen
        ))

        newsArray.add(NewsEntity(
            "NVIDIA proširuje Ethereum regulaciju",
            "Kako bi svoje kartice učinili manje korisnim za rudare - i kako bi pokušali da preusmere isporuke natrag kupcima igara",
            R.drawable.nvidia
        ))

        newsArray.add(NewsEntity(
            "Test: Logitech G512 i G513 CARBON",
            "Ovog puta, na testu su nam se našle dve veoma slične tastature iz Logitech G serije",
            R.drawable.logitech
        ))

        newsArray.add(NewsEntity(
                "Motorola menja način na koji koristimo telefon",
                "Tokom prošle godine, naše oslanjanje na mobilne uređaje se dramatično povećalo. ",
                R.drawable.motorola
        ))

        newsArray.add(NewsEntity(
                "Evo šta je sve novo u dizajnu Android 12",
                "Google planira osam izdanja tokom narednih meseci pre nego što će kasnije ove godine lansirati Pixel telefone i druge uređaje.",
                R.drawable.android
        ))

        newsArray.add(NewsEntity(
                "Huawei i Viber partnerstvo jača iz dana u dan",
                "Huawei, vodeća globalna tehnološka kompanija, objavila je da proširuje svoju dugoročnu saradnju sa brendom Viber.",
                R.drawable.viber
        ))

    }
}