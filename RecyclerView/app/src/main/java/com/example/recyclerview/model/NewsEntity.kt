package com.example.recyclerview.model

data class NewsEntity(
    var title: String,
    var text: String,
    var image: Int
) {
}