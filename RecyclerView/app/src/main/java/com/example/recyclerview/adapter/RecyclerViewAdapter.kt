package com.example.recyclerview.adapter

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerview.R
import com.example.recyclerview.model.NewsEntity
import kotlinx.android.synthetic.main.news.view.*

class RecyclerViewAdapter(private val newsData: ArrayList<NewsEntity>) : RecyclerView.Adapter<CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val recycleViewRow = layoutInflater.inflate(R.layout.news, parent, false)
        return CustomViewHolder(recycleViewRow)

    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.title.text = newsData[position].title
        holder.view.body.text = newsData[position].text
        holder.view.imageView.setImageResource(newsData[position].image)


    }

    override fun getItemCount(): Int {

        return newsData.count()
    }


}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}