package it.engineering.textfileobrada

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import java.io.BufferedReader
import java.io.InputStreamReader

data class Student(val ime:String, val prezime:String, val grad:String, val godiste:Int, val visina:Int, val ocena:Double)

class MainActivity : AppCompatActivity() {


    var sviPodaci = ""
    var studenti: ArrayList<Student> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val ispis = findViewById<TextView>(R.id.ispisPodataka)

        ispis.setOnClickListener{
            parseCSV()
            for(s in studenti){
                Log.d("imena", s.ime)
            }
        }
    }

    private fun parseCSV() {
        var linija = ""
        // ili : var linija:String?

        //za parsiranje lokalnih fajlova koristimo klasu input stream
        //posrednik izmejdu toka bajtova i toka karakterka

        val otvoriCSV = InputStreamReader(assets.open("textFile.csv"))

        //citanje linije po linije

        val procitajLiniju = BufferedReader(otvoriCSV)

       // Log.d("CSV", procitajLiniju.toString())

        //citanje buffered reader-a

        while(procitajLiniju.readLine().also {linija = it
                } != null){
            Log.d("CSV", linija)
            val red : List<String> = linija.split(",")
            Log.d("CSV", red[0])

            if(red[0].isEmpty()){
                Log.d("CSV", "Red je prazan")
            } else {
                sviPodaci = sviPodaci + red[0] + " " + red[1] + " " + red[2] + " " + red[4] + " " + red[5] + "/n"

                studenti.add(Student(red[0], red[1], red[2], red[3].toInt(), red[4].toInt(), red[5].toDouble()))
            }


        }

        val ispis = findViewById<TextView>(R.id.ispisPodataka)
        ispis.text = sviPodaci
    }
}